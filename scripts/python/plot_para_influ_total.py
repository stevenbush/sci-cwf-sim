#!/usr/bin/python
# Filename: plot_para_influ.py

import glob, string, sys, traceback, os
import numpy as np
import matplotlib.pyplot as plt
from operator import itemgetter
import ConfigParser
from matplotlib.pyplot import figure

def getFinishedDags(filepath, resultKey):
    finishedDags = {};
    
    for pathvalue in filepath:
        # print pathvalue
        cf = ConfigParser.ConfigParser()
        cf.read(pathvalue)
        budget = string.atof(cf.get("result", "budget"))
        finisheddags = string.atof(cf.get("result", resultKey))
        finishedDags[budget] = finisheddags
        
    finishedDags = sorted(finishedDags.iteritems(), key=itemgetter(0), reverse=False)
    return finishedDags

def plotPara(resultPath, ditribution, resultKey, dag_name, algorithmName,outpath):
    distributionString = ["uniform_sorted", "pareto_sorted"]
    workloadString = ["onoff", "bursting", "stable", "growing"] 
    
    distibution_index = distributionString.index(ditribution)
    for workloadName in workloadString:
        index = workloadString.index(workloadName) 
        try:
            filepath = glob.glob(resultPath + '*result*-' + algorithmName + '-' + ditribution + '-*' + workloadName + '*d10.0-dv0.0-rv0.0-f0.00*')
            original = getFinishedDags(filepath, resultKey)
            original = np.array(original)
            XOriginal = original[0:, 0]
            YOriginal = original[0:, 1] 

            filepath = glob.glob(resultPath + '*result*-' + algorithmName + '-' + ditribution + '-*' + workloadName + '*d10.0-dv0.2-rv0.0-f0.00*')
            delay = getFinishedDags(filepath, resultKey)
            delay = np.array(delay)
            Xdelay = delay[0:, 0]
            Ydelay = delay[0:, 1]

            filepath = glob.glob(resultPath + '*result*-' + algorithmName + '-' + ditribution + '-*' + workloadName + '*d10.0-dv0.0-rv0.2-f0.00*')
            runtime = getFinishedDags(filepath, resultKey)
            runtime = np.array(runtime)
            Xruntime = runtime[0:, 0]
            Yruntime = runtime[0:, 1]

            filepath = glob.glob(resultPath + '*result*-' + algorithmName + '-' + ditribution + '-*' + workloadName + '*d10.0-dv0.0-rv0.0-f0.10*')
            failure = getFinishedDags(filepath, resultKey)
            failure = np.array(failure)
            Xfailure = failure[0:, 0]
            Yfailure = failure[0:, 1]


        except Exception, e:
            print e

        print ditribution+'-'+workloadName+':'
        print 'Original:'
        print original
        print ''
        print 'delay:'
        print delay
        print ''
        print 'runtime:'
        print runtime
        print ''
        print 'failure:'
        print failure        
        print ''

if len(sys.argv) < 4:
    print 'No result path, Dag Name and output path specified, just like:'
    print 'python2.7 plot_para_influ result_path dag_name output_path'
    sys.exit()
    
print 'path: ' + sys.argv[1]
    
resultpath = os.path.abspath(sys.argv[1]) + "/"
dag_name = sys.argv[2]
outpath = os.path.abspath(sys.argv[3]) + "/"
    
# resultpath = '/Users/jiyuanshi/Documents/workspace/sci-cwf-sim/result_CYBERSHAKE/'
# distributionString = ["constant", "uniform_unsorted", "uniform_sorted", "pareto_unsorted", "pareto_sorted"]
distributionString = ["uniform_sorted", "pareto_sorted"]
algorithmSting = ["PPDS"]
for algorithmName in algorithmSting:
    for distributionName in distributionString:
        plotPara(resultpath, distributionName, "ExponentialScore", dag_name, algorithmName,outpath)


