
#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt

N = 5
menMeans = (20, 35, 30, 35, 27)

ind = np.arange(N)  # the x locations for the groups
width = 0.25       # the width of the bars


#plt.subplot(111)
rects1 = plt.bar(ind, menMeans, width, color='y')

womenMeans = (25, 32, 34, 20, 25)
rects2 = plt.bar(ind+width, womenMeans, width, color='black')

childMeans = (32, 28, 45, 14, 42)
rects3 = plt.bar(ind+2*width, childMeans, width, color='b')


# add some
plt.ylim(0, 70)  
plt.ylabel('Scores')
plt.title('Scores by group and gender')
plt.xticks(ind+1.5*width, ('G1', 'G2', 'G3', 'G4', 'G5') )

plt.legend( (rects1[0], rects2[0], rects3[0]), ('Men', 'Women', 'Child') ,prop={'size':11})

def autolabel(rects):
    # attach some text labels
    for rect in rects:
        height = rect.get_height()
        plt.text(rect.get_x()+rect.get_width()/2., 1.02*height, '%d'%int(height),
                ha='center', va='bottom')

autolabel(rects1)
autolabel(rects2)
autolabel(rects3)

plt.show()
