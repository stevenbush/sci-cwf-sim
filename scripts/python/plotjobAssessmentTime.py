#!/usr/bin/python

import os, glob, string, sys, traceback
from pylab import *

if len(sys.argv) < 5:
    print 'No result path, Dag Name, budget and outpath specified.'
    print 'python2.7 plotjobAssessmentTime.py result_path dag_name budget outpath'
    sys.exit()
    
print 'path: ' + sys.argv[1]
    
inputpath = os.path.abspath(sys.argv[1]) + "/"
dagName = sys.argv[2]
budget = sys.argv[3]
outpath = os.path.abspath(sys.argv[4]) + "/"
assessTimeList = []
try:
    filepath = glob.glob(inputpath + '*-' + budget + '.0-d10.0-dv0.0-rv0.0-f0.00-jobAssessmentTime*')
    count = 1
    for pathvalue in filepath:
        print str(count) + "~" + pathvalue
        file = open(pathvalue)
        for line in file:
            assessTime = float(line.split('~')[1].strip()) / 1000000
            assessTimeList.append(assessTime)
        file.close()
        count = count + 1
        
    print len(assessTimeList)
    
    # the histogram of the data
    #n, bins, patches = hist(assessTimeList, 500, normed=1, facecolor='green', alpha=0.75)
    n, bins, patches = hist(assessTimeList, 500, facecolor='green', alpha=0.75)
    
    xlim(0, 40)
    xlabel('Assessment Time(Millisecond)')
    ylabel('number of occurrences')
    title(dagName + " DAG Job Assessment Time")
    grid(True)
    savefig(outpath + dagName + " DAG Job Assessment Time" + budget + ".pdf", dpi=150)
    show()
    
except Exception, e:
    print e
