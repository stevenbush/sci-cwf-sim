#!/usr/bin/python
# Filename: submit_job.py

import glob, string, os, sys, traceback

# inputpath = '/gpfssan3/jishi/experiment/sci-cwf-sim/input/'
#outputpath = '/gpfssan3/jishi/experiment/sci-cwf-sim/output/'
#logpath = '/gpfssan3/jishi/experiment/sci-cwf-sim/log/'

if len(sys.argv) < 3:
    print 'No input path and log path specified.'
    sys.exit()
    
inputpath = os.path.abspath(sys.argv[1])
logpath = os.path.abspath(sys.argv[2])
print "inputpath: %s" %inputpath
print "logpath: %s" %logpath

try:
    filepath = glob.glob(inputpath + '/input-*')
    for pathvalue in filepath:
        jobname = pathvalue.split("/")
        jobname = jobname[len(jobname) - 1]
        #cmd = "bsub -q production \'cd /gpfssan3/jishi/experiment/sci-cwf-sim/ && /gpfssan3/jishi/experiment/sci-cwf-sim/runexperiment.sh %s > %s 2>&1\'" % (pathvalue, logpath + jobname + '.log')
        cmd = "bsub -q 1nd -e %s -o %s \'cd /afs/cern.ch/work/j/jishi/workspace/sci-cwf-sim/ && /afs/cern.ch/work/j/jishi/workspace/sci-cwf-sim/runexperiment.sh %s > %s 2>&1\'" % (logpath + '/' +jobname + '.elog', logpath + '/' +jobname + '.olog',pathvalue, logpath + '/' +jobname + '.log')
        #print cmd
        os.system(cmd)
except Exception, e:
    print e
    print traceback.format_exc()
