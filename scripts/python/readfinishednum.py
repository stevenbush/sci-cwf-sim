#!/usr/bin/python
# Filename: readfinishednum.py

import glob, string
import numpy as np
from pylab import *
from operator import itemgetter

def getFinishedDags(filepath):
    finishedDags = {};
    
    for pathvalue in filepath:
        budget = 0
        finisheddags = 0
        infile = open(pathvalue, 'r')
        for line in infile.readlines():
            key_value = line.split()
            if cmp(key_value[0], 'actualFinishTime') == 0:
                finisheddags = string.atof(key_value[1])
            if cmp(key_value[0], 'budget') == 0:
                budget = string.atof(key_value[1])
        infile.close();
        finishedDags[budget] = finisheddags
        
    finishedDags = sorted(finishedDags.iteritems(), key=itemgetter(0), reverse=False)
    return finishedDags


# filepath = glob.glob('/Users/jiyuanshi/Documents/workspace/sci-cwf-sim/output/*OneType-DPDS*')
# OneTypeDPDSFinishedDags = getFinishedDags(filepath)
#  
# filepath = glob.glob('/Users/jiyuanshi/Documents/workspace/sci-cwf-sim/output/*OneType-WADPDS*')
# OneTypeWADPDSFinishedDags = getFinishedDags(filepath)
#  
# filepath = glob.glob('/Users/jiyuanshi/Documents/workspace/sci-cwf-sim/output/*OneType-SFPDS*')
# OneTypeSFPDSFinishedDags = getFinishedDags(filepath)

resultpath = '/Users/jiyuanshi/Documents/workspace/sci-cwf-sim/output/'

filepath = glob.glob(resultpath + '*MultiType-DPDS*')
MultiTypeDPDSFinishedDags = getFinishedDags(filepath)

filepath = glob.glob(resultpath + '*MultiType-WADPDS*')
MultiTypeWADPDSFinishedDags = getFinishedDags(filepath)

filepath = glob.glob(resultpath + '*MultiType-SFPDS*')
MultiTypeSFPDSFinishedDags = getFinishedDags(filepath)

# OneTypeDPDSFinishedDags = np.array(OneTypeDPDSFinishedDags)
# XBudget = OneTypeDPDSFinishedDags[0:, 0]
# YDags = OneTypeDPDSFinishedDags[0:, 1]
# plot(XBudget, YDags, linewidth=2.5, linestyle="-", label="OneTypeDPDS")
# 
# OneTypeSFPDSFinishedDags = np.array(OneTypeSFPDSFinishedDags)
# XBudget = OneTypeSFPDSFinishedDags[0:, 0]
# YDags = OneTypeSFPDSFinishedDags[0:, 1]
# plot(XBudget, YDags, linewidth=2.5, linestyle="-", label="OneTypeSFPDS")
# 
# OneTypeWADPDSFinishedDags = np.array(OneTypeWADPDSFinishedDags)
# XBudget = OneTypeWADPDSFinishedDags[0:, 0]
# YDags = OneTypeWADPDSFinishedDags[0:, 1]
# plot(XBudget, YDags, linewidth=2.5, linestyle="-", label="OneTypeWADPDS")

MultiTypeWADPDSFinishedDags = np.array(MultiTypeWADPDSFinishedDags)
XBudget = MultiTypeWADPDSFinishedDags[0:, 0]
YDags = MultiTypeWADPDSFinishedDags[0:, 1]
plot(XBudget, YDags, 'o', linewidth=2.5, linestyle="-", label="MultiTypeWADPDS")

MultiTypeDPDSFinishedDags = np.array(MultiTypeDPDSFinishedDags)
XBudget = MultiTypeDPDSFinishedDags[0:, 0]
YDags = MultiTypeDPDSFinishedDags[0:, 1]
plot(XBudget, YDags, 'o', linewidth=2.5, linestyle="-", label="MultiTypeDPDS")

# MultiTypeSFPDSFinishedDags = np.array(MultiTypeSFPDSFinishedDags)
# XBudget = MultiTypeSFPDSFinishedDags[0:, 0]
# YDags = MultiTypeSFPDSFinishedDags[0:, 1]
# plot(XBudget, YDags, 'o', linewidth=2.5, linestyle="-", label="MultiTypeSFPDS")


# ylim(0,6000)
xlabel('budget')
ylabel('dags')
legend(loc='upper left')
show()
