#!/usr/bin/python
# Filename: plot_para_influ.py

import glob, string, sys, traceback, os
import numpy as np
import matplotlib.pyplot as plt
from operator import itemgetter
import ConfigParser
from matplotlib.pyplot import figure

def getFinishedDags(filepath, resultKey):
    finishedDags = {};
    
    for pathvalue in filepath:
        # print pathvalue
        cf = ConfigParser.ConfigParser()
        cf.read(pathvalue)
        budget = string.atof(cf.get("result", "budget"))
        finisheddags = string.atof(cf.get("result", resultKey))
        finishedDags[budget] = finisheddags
        
    finishedDags = sorted(finishedDags.iteritems(), key=itemgetter(0), reverse=False)
    return finishedDags

def plotPara(resultPath, ditribution, resultKey, dag_name, algorithmName,outpath):
    distributionString = ["uniform_sorted", "pareto_sorted"]
    workloadString = ["onoff", "bursting", "stable", "growing"] 

    #AMS
    #total_size = [[r"$6.34 \times 10^{11}$", r"$1.38 \times 10^{12}$", r"$1.32 \times 10^{12}$", r"$3.36 \times 10^{12}$"], [r"$2.27 \times 10^{11}$", r"$4.07 \times 10^{11}$", r"$4.51 \times 10^{11}$", r"$1.16 \times 10^{12}$"]]
    
    #CYBERSHAKE
    #total_size = [[r"$2.94 \times 10^{10}$", r"$5.09 \times 10^{10}$", r"$5.77 \times 10^{10}$", r"$1.37 \times 10^{11}$"], [r"$9.29 \times 10^{9}$", r"$1.83 \times 10^{10}$", r"$2.00 \times 10^{10}$", r"$4.77 \times 10^{10}$"]]

    #GENOME
    #total_size = [[r"$2.54 \times 10^{12}$", r"$5.97 \times 10^{12}$", r"$5.21 \times 10^{12}$", r"$1.32 \times 10^{13}$"], [r"$8.68 \times 10^{11}$", r"$2.08 \times 10^{11}$", r"$1.79 \times 10^{12}$", r"$4.40 \times 10^{12}$"]]
    
    #LIGO
    total_size = [[r"$2.73 \times 10^{11}$", r"$5.50 \times 10^{11}$", r"$5.48 \times 10^{11}$", r"$1.36 \times 10^{12}$"], [r"$1.00 \times 10^{11}$", r"$1.93 \times 10^{11}$", r"$1.87 \times 10^{11}$", r"$4.61 \times 10^{11}$"]]


    distibution_index = distributionString.index(ditribution)
    for workloadName in workloadString:
        index = workloadString.index(workloadName)  
        xmax = 0
        ymax = 0
        plt.subplot(2, 2, index)        
        try:
            filepath = glob.glob(resultPath + '*result*-' + algorithmName + '-' + ditribution + '-*' + workloadName + '*d10.0-dv0.0-rv0.0-f0.00*')
            original = getFinishedDags(filepath, resultKey)
            original = np.array(original)
            X = original[0:, 0]
            YOriginal = original[0:, 1] 
            Y = YOriginal
            if X.max() > xmax:
                    xmax = X.max()
            if Y.max() > ymax:
                    ymax = Y.max()
            plt.plot(X, Y, 'h', linewidth=2.5, linestyle="-", label="accurate parameters")
            plt.tight_layout()     
            
            filepath = glob.glob(resultPath + '*result*-' + algorithmName + '-' + ditribution + '-*' + workloadName + '*d10.0-dv0.2-rv0.0-f0.00*')
            delay = getFinishedDags(filepath, resultKey)
            delay = np.array(delay)
            Y = delay[0:, 1]
            # Y = np.abs(Y - YOriginal) / YOriginal
            if Y.max() > ymax:
                    ymax = Y.max()
            plt.plot(X, Y, 's', linewidth=2.5, linestyle="-", label="inaccurate provision delay")
            plt.tight_layout()
            
            filepath = glob.glob(resultPath + '*result*-' + algorithmName + '-' + ditribution + '-*' + workloadName + '*d10.0-dv0.0-rv0.2-f0.00*')
            runtime = getFinishedDags(filepath, resultKey)
            runtime = np.array(runtime)
            Y = runtime[0:, 1]
            # Y = np.abs(Y - YOriginal) / YOriginal
            if Y.max() > ymax:
                    ymax = Y.max()
            plt.plot(X, Y, 'd', linewidth=2.5, linestyle="-", label="inaccurate run time")
            plt.tight_layout()
            
            filepath = glob.glob(resultPath + '*result*-' + algorithmName + '-' + ditribution + '-*' + workloadName + '*d10.0-dv0.0-rv0.0-f0.10*')
            failure = getFinishedDags(filepath, resultKey)
            failure = np.array(failure)
            Y = failure[0:, 1]
            # Y = np.abs(Y - YOriginal) / YOriginal
            if Y.max() > ymax:
                    ymax = Y.max()
                    
            plt.xlim(0, xmax * 1.1)
            plt.ylim(0, ymax * 1.1)  
            plt.plot(X, Y, 'o', linewidth=2.5, linestyle="-", label="with job execution failure")
            plt.tight_layout()   
            
            plt.title(workloadName + "-" + ditribution.split("_")[0])
            #plt.title(workloadName + "-" + ditribution.split("_")[0] + " " + total_size[distibution_index][index])
            plt.xlabel('budget($/hour)')
            plt.ylabel(resultKey)
            plt.legend(loc='best', prop={'size':8})    
            #plt.legend(loc='lower right', prop={'size':8})  
            
        except Exception, e:
            print e        

    # Save figure using 150 dots per inch
    #plt.suptitle(ditribution.split("_")[0], fontsize=16)
    plt.savefig(outpath + dag_name + "-" + ditribution.split("_")[0] + "-para_affect.pdf", dpi=150)
    plt.savefig(outpath + dag_name + "-" + ditribution.split("_")[0] + "-para_affect.png", dpi=300)
    plt.show()
        

def plotParaDetail(resultPath, ditribution, resultKey, dag_name, algorithmName, outpath):
    workloadString = ["onoff", "bursting", "stable", "growing"] 
       
    for workloadName in workloadString:
        index = workloadString.index(workloadName)  
        xmax = 0
        ymax = 0
        plt.subplot(2, 2, index)        
        try:
            filepath = glob.glob(resultPath + '*result*-' + algorithmName + '-' + ditribution + '-*' + workloadName + '*d10.0-dv0.0-rv0.0-f0.00*')
            original = getFinishedDags(filepath, resultKey)
            original = np.array(original)
            X = original[0:, 0]
            YOriginal = original[0:, 1]      
            
            filepath = glob.glob(resultPath + '*result*-' + algorithmName + '-' + ditribution + '-*' + workloadName + '*d10.0-dv0.2-rv0.0-f0.00*')
            delay = getFinishedDags(filepath, resultKey)
            delay = np.array(delay)
            Y = delay[0:, 1]
            Y = np.abs(Y - YOriginal) / YOriginal
            if Y.max() > ymax:
                    ymax = Y.max()
            plt.plot(X, Y, 'o', linewidth=2.5, linestyle="-", label="inaccurate provision delay")
            plt.tight_layout()
            
            filepath = glob.glob(resultPath + '*result*-' + algorithmName + '-' + ditribution + '-*' + workloadName + '*d10.0-dv0.0-rv0.2-f0.00*')
            runtime = getFinishedDags(filepath, resultKey)
            runtime = np.array(runtime)
            Y = runtime[0:, 1]
            Y = np.abs(Y - YOriginal) / YOriginal
            if Y.max() > ymax:
                    ymax = Y.max()
            plt.plot(X, Y, 'o', linewidth=2.5, linestyle="-", label="inaccurate run time")
            plt.tight_layout()
            
            filepath = glob.glob(resultPath + '*result*-' + algorithmName + '-' + ditribution + '-*' + workloadName + '*d10.0-dv0.0-rv0.0-f0.10*')
            failure = getFinishedDags(filepath, resultKey)
            failure = np.array(failure)
            Y = failure[0:, 1]
            Y = np.abs(Y - YOriginal) / YOriginal
            if Y.max() > ymax:
                    ymax = Y.max()
            plt.ylim(0, ymax * 1.1)  
            plt.plot(X, Y, 'o', linewidth=2.5, linestyle="-", label="with job execution failure")
            plt.tight_layout()   
            
            plt.title(workloadName + "-" + ditribution.split("_")[0])
            plt.xlabel('budget($/hour)')
            plt.ylabel('difference')
            plt.legend(loc='lower right', prop={'size':6})     
            
        except Exception, e:
            print e        

    # Save figure using 150 dots per inch
    plt.suptitle(algorithmName + "-" + ditribution.split("_")[0], fontsize=16)
    plt.savefig(outpath + dag_name + "-" + algorithmName + "-" + ditribution.split("_")[0] + "-para_affect_detail.pdf", dpi=150)
    plt.show()

if len(sys.argv) < 4:
    print 'No result path, Dag Name and output path specified, just like:'
    print 'python2.7 plot_para_influ result_path dag_name output_path'
    sys.exit()
    
print 'path: ' + sys.argv[1]
    
resultpath = os.path.abspath(sys.argv[1]) + "/"
dag_name = sys.argv[2]
outpath = os.path.abspath(sys.argv[3]) + "/"
    
# resultpath = '/Users/jiyuanshi/Documents/workspace/sci-cwf-sim/result_CYBERSHAKE/'
# distributionString = ["constant", "uniform_unsorted", "uniform_sorted", "pareto_unsorted", "pareto_sorted"]
distributionString = ["uniform_sorted", "pareto_sorted"]
algorithmSting = ["PPDS"]
for algorithmName in algorithmSting:
    for distributionName in distributionString:
        plotPara(resultpath, distributionName, "ExponentialScore", dag_name, algorithmName,outpath)
        plotParaDetail(resultpath, distributionName, "ExponentialScore", dag_name, algorithmName,outpath)

