#!/usr/bin/python

from pylab import *

spread = rand(50) * 100
center = ones(25) * 50
flier_high = rand(10) * 100 + 100
flier_low = rand(10) * -100
d1 = concatenate((spread, center, flier_high, flier_low), 0)
d1.shape = (-1, 1)

# fake up some more data
spread = rand(50) * 100
center = ones(25) * 40
flier_high = rand(10) * 100 + 100
flier_low = rand(10) * -100
d2 = concatenate((spread, center, flier_high, flier_low), 0)
d2.shape = (-1, 1)

emptyarray = array([]).shape = (95, 1)
# data = hstack((emptyarray, d2))
data = concatenate((emptyarray, d2), 1)
print data.shape
xflag = [8, 4]
boxplot(data, positions=xflag)
# show()
