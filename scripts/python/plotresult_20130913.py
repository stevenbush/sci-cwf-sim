#!/usr/bin/python
# Filename: plotresult.py

import glob, string, sys, traceback, os
import numpy as np
from pylab import *
from operator import itemgetter
import ConfigParser
from hashlib import algorithms

def getFinishedDags(filepath, resultKey):
    finishedDags = {};
    
    for pathvalue in filepath:
        # print pathvalue
        cf = ConfigParser.ConfigParser()
        cf.read(pathvalue)
        budget = string.atof(cf.get("result", "budget"))
        finisheddags = string.atof(cf.get("result", resultKey))
        finishedDags[budget] = finisheddags
        
    finishedDags = sorted(finishedDags.iteritems(), key=itemgetter(0), reverse=False)
    return finishedDags

def plotResult(resultPath, distribution, resultKey, dag_name, outpath):
    workloadString = ["onoff", "bursting", "stable", "growing"]
    algorithmString = ['DPDS', 'WADPDS', 'SFPDS', 'PPDS', 'WPPDS']
    
    for workloadName in workloadString:
        index = workloadString.index(workloadName)
        xmax = 0
        ymax = 0
        subplot(2, 2, index)
        for algorithmName in algorithmString:
            
            try:
                filepath = glob.glob(resultPath + '*result*-' + algorithmName + '-' + distribution + '-*' + workloadName + '*d10.0-dv0.0-rv0.0-f0.00*')
                FinishedDags = getFinishedDags(filepath, resultKey)
                FinishedDags = np.array(FinishedDags)
                # print algorithmName
                # print FinishedDags
                X = FinishedDags[0:, 0]
                Y = FinishedDags[0:, 1]
                if X.max() > xmax:
                    xmax = X.max()
                if Y.max() > ymax:
                    ymax = Y.max()
                # subplot(2, 2, index)
                xlim(0, xmax * 1.1)
                ylim(0, ymax * 1.1)  
                plot(X, Y, 'o', linewidth=2.5, linestyle="-", label=algorithmName)
                tight_layout()
        
                title(workloadName + "-" + distribution.split("_")[0])
                xlabel('budget($/hour)')
                ylabel(resultKey)
                legend(loc='upper left', prop={'size':6})
            except Exception, e:
                print e     
        
    # Save figure using 150 dots per inch
    suptitle(distribution.split("_")[0], fontsize=16)
    savefig(outpath + dag_name + "-" + distribution.split("_")[0] + "-" + resultKey + ".pdf", dpi=150)
    show()

if len(sys.argv) < 4:
    print 'No result path, Dag Name and output path specified, just like:'
    print 'python2.7 plotresult result_path dag_name output_path'
    sys.exit()
    
print 'path: ' + sys.argv[1]
    
resultpath = os.path.abspath(sys.argv[1]) + "/"
dag_name = sys.argv[2]
outpath = os.path.abspath(sys.argv[3]) + "/"

#resultpath = '/Users/jiyuanshi/Documents/workspace/sci-cwf-sim/result_CYBERSHAKE/'
# resultpath = '/Users/jiyuanshi/Documents/workspace/sci-cwf-sim/result_MONTAGE/'
# resultpath = '/Users/jiyuanshi/Documents/workspace/sci-cwf-sim/result_GENOME/'
# resultpath = '/Users/jiyuanshi/Documents/workspace/sci-cwf-sim/result_LIGO/'
# distributionString = ["constant", "uniform_unsorted", "uniform_sorted", "pareto_unsorted", "pareto_sorted"]
distributionString = ["uniform_sorted", "pareto_sorted"]
for distributionName in distributionString:
    plotResult(resultpath, distributionName, "ExponentialScore", dag_name, outpath)

# for distributionName in distributionString:
#     plotResult(resultpath, distributionName, "finished")
    
# for distributionName in distributionString:
#     plotResult(resultpath, distributionName, "cost")
