#!/usr/bin/python

import os, glob, string, sys, traceback
from pylab import *

if len(sys.argv) < 4:
    print 'No input path and Dag Name specified.'
    print 'python2.7 plotdisDeadlineTime result_path dag_name output_path'
    sys.exit()
    
print 'path: ' + sys.argv[1]
    
inputpath = os.path.abspath(sys.argv[1]) + "/"
dagName = sys.argv[2]
outpath = os.path.abspath(sys.argv[3]) + "/"
timeRecord = {}

try:
    filepath = glob.glob(inputpath + '*-2000.0-d10.0-dv0.0-rv0.0-f0.00-disDeadlineTime*')
    count = 1
    for pathvalue in filepath:
        print str(count) + "~" + pathvalue
        file = open(pathvalue)
        for line in file:
            dagsize = float(line.split('~')[0].strip())
            timespan = (float(line.split('~')[1].strip())) / 1000000
            # timespan = (float(line.split('~')[1].strip()))
            if timeRecord.has_key(dagsize):
                tmplist = list(timeRecord[dagsize])
                tmplist.append(timespan)
                timeRecord[dagsize] = tmplist
            else:
                newlist = [timespan]
                timeRecord[dagsize] = newlist
        file.close()
        count = count + 1
    
    for dag_size, timearray in timeRecord.items():
        print dag_size
        print list(timearray)[0]
    
    datalist = []
    for element in timeRecord.values():
        datalist.append(array(element))
    
    data = array(datalist)
    xflag = timeRecord.keys()
    boxplot(data, 0, '', positions=xflag, widths=30)
    xlim(0, 1050)
    ylim(0, 5)
    title(dagName + " Sub-Deadline Distribution Time")
    xlabel('Dag Size')
    ylabel('Sub-Deadline Distribution Time(Millisecond)') 
    grid(True) 
    savefig(outpath + dagName + " Sub-Deadline Distribution Time" + ".pdf", dpi=150)
    show()
        
except Exception, e:
    print e
