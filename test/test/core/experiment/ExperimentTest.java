package test.core.experiment;

import static org.junit.Assert.*;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.math3.distribution.UniformIntegerDistribution;
import org.junit.Test;

import scws.core.DAGDescription;
import scws.core.VMDescription;
import scws.core.dag.DAG;
import scws.core.dag.DAGParser;
import scws.core.dag.DAGStats;
import scws.core.experiment.ExperimentDescription;
import scws.core.experiment.GenerateConfiguration;

/**
 * Tests generating a series of workload of the same workflow.
 * 
 * @author jiyuanshi
 * @since 2013-4-15
 * @version sci-cwf-sim 1.0
 */
public class ExperimentTest {

	String runDirectory = "output";
	String group = "Constant";
	String dagPath = "dags";
	String dagName;
	double budget;
	HashMap<String, Double[]> vmDescription;
	String workload;
	Double timespan;
	double max_scaling = 2.0;
	double alpha = 0.7;
	double taskDilatation = 1000.0;
	double runtimeVariation = 0.0;
	double delayVariation = 0.2;
	double deadlineVariation = 0.5;
	double failureRate = 0.1;
	double informtimeratio = 0.05;
	double delay = 0.0;
	int averagejobnum = 120;
	int job_deviation = 20;
	int increments = 10;
	int perjob_upper = 200;
	int perjob_lower = 40;
	int min_jobnum = 20;

	// @Test
	public void testGenerateConfigure() {
		dagName = "MONTAGE.n.1000.19.dag";
		budget = 80.0;
		HashMap<String, VMDescription> vmDescription = new HashMap<String, VMDescription>();
		/** Micro vm property */
		VMDescription microProperty = new VMDescription("Micro", 1000.0, 0.02);
		vmDescription.put(microProperty.vmname, microProperty);
		/** Standard vm property */
		VMDescription standardProperty = new VMDescription("Standard", 2000.0, 0.06);
		vmDescription.put(standardProperty.vmname, standardProperty);
		/** Medium vm property */
		VMDescription mediumProperty = new VMDescription("Medium", 5000.0, 0.145);
		vmDescription.put(mediumProperty.vmname, mediumProperty);
		/** High-CPU vm property */
		VMDescription highcpuProperty = new VMDescription("High-CPU", 20000.0, 0.85);
		vmDescription.put(highcpuProperty.vmname, highcpuProperty);
		/** Extra-Large vm property */
		VMDescription extralargeProperty = new VMDescription("Extra-Large", 33500.0, 1.3);
		vmDescription.put(extralargeProperty.vmname, extralargeProperty);
		workload = "5number";
		timespan = 48 * 3600.0;

		double[] subtimes = { 10, 120, 230, 340, 450 };

		UniformIntegerDistribution uniformIntegerDistribution = new UniformIntegerDistribution(1, 6);
		Integer priority;
		System.out.println(dagPath + File.separator + dagName);
		DAG dag = DAGParser.parseDAG(new File(dagPath + File.separator + dagName), dagName);
		DAGStats dagStats = new DAGStats(dag, taskDilatation, vmDescription);
		Double deadline = dagStats.getMaxCriticalPath() * (1 + deadlineVariation);
		ArrayList<DAGDescription> dags = new ArrayList<DAGDescription>(); // dags'
																			// description

		for (Double subtime : subtimes) {
			priority = uniformIntegerDistribution.sample();
			DAGDescription dagDescription = new DAGDescription(dagName, dagPath, subtime, subtime + deadline,
					priority);
			dags.add(dagDescription);
		}

		GenerateConfiguration.generateSeries(runDirectory, group, dagPath, dags, budget, vmDescription,
				workload, timespan, max_scaling, taskDilatation, runtimeVariation, delayVariation,
				deadlineVariation, failureRate, informtimeratio, delay, "constant", 0, averagejobnum,
				job_deviation, increments, perjob_upper, perjob_lower, min_jobnum);

	}

	// @Test
	public void testGenerateConfigurationMontage() {

		dagName = "MONTAGE.n.1000.19.dag";
		budget = 80.0;
		HashMap<String, VMDescription> vmDescription = new HashMap<String, VMDescription>();
		/** Micro vm property */
		VMDescription microProperty = new VMDescription("Micro", 1000.0, 0.02);
		vmDescription.put(microProperty.vmname, microProperty);
		/** Standard vm property */
		VMDescription standardProperty = new VMDescription("Standard", 2000.0, 0.06);
		vmDescription.put(standardProperty.vmname, standardProperty);
		/** Medium vm property */
		VMDescription mediumProperty = new VMDescription("Medium", 5000.0, 0.145);
		vmDescription.put(mediumProperty.vmname, mediumProperty);
		/** High-CPU vm property */
		VMDescription highcpuProperty = new VMDescription("High-CPU", 20000.0, 0.85);
		vmDescription.put(highcpuProperty.vmname, highcpuProperty);
		/** Extra-Large vm property */
		VMDescription extralargeProperty = new VMDescription("Extra-Large", 33500.0, 1.3);
		vmDescription.put(extralargeProperty.vmname, extralargeProperty);
		workload = "bursting";
		timespan = 48 * 3600.0;

		GenerateConfiguration.generateConstantSeries(runDirectory, group, dagPath, dagName, budget,
				vmDescription, workload, timespan, max_scaling, taskDilatation, runtimeVariation,
				delayVariation, deadlineVariation, failureRate, informtimeratio, delay, averagejobnum,
				job_deviation, increments, perjob_upper, perjob_lower, min_jobnum);
	}

	//@Test
	public void testReadConfigurationMontage() {
		String configfile = "output/input-CYBERSHAKE-WPPDS-uniform_unsorted-5175-bursting-200.00.properties";
		ExperimentDescription param = new ExperimentDescription(configfile);
		System.out.println(param.getDelayVariation());

		// for (VMDescription iterable_element :
		// param.getVmDescription().values()) {
		// System.out.println(iterable_element.vmname + " " +
		// iterable_element.mips + " "
		// + iterable_element.price);
		// }
		//
		// for (DAGDescription iterable_element : param.getDags()) {
		// System.out.println(iterable_element.filename + " " +
		// iterable_element.subtime + " "
		// + iterable_element.deadline + " " + iterable_element.priority);
		// }

	}

}
