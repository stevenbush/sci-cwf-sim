package test.core.experiment;

import static org.junit.Assert.*;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import org.apache.commons.math3.distribution.UniformIntegerDistribution;
import org.junit.Test;

import scws.core.experiment.DAGListGenerator;

import scws.core.DAGDescription;
import scws.core.VMDescription;
import scws.core.experiment.ExperimentDescription;
import scws.core.experiment.GenerateConfiguration;

public class TestPropertyCenerator {

	String runDirectory = "output";
	String generateDagPath = "dags/";
	String dagPath = "dags/";
	String dagName;
	String workload;
	String distribution;
	double budget;
	double price = 1.0;
	int numDAGs = 40;
	double max_scaling = 2.0;
	String group = "";
	double alpha = 0.7;
	double taskDilatation = 1000.0;
	double runtimeVariation = 0.0;
	double delayVariation = 0.2;
	double delay = 10;
	double timespan = 48 * 3600.0;
	HashMap<String, Double[]> vmDescription;
	double deadlineVariation = 0.5;
	double failureRate = 0.01;
	double informtimeratio = 0.05;
	int averagejobnum = 100;
	int job_deviation = 20;
	int increments = 10;
	int perjob_upper = 180;
	int perjob_lower = 20;
	int min_jobnum = 10;

	/******************************
	 * 
	 * Tests with max scaling = 0.0
	 * 
	 ******************************/

	/**
	 * bursting workload, random priority between 1~6, random submit time,
	 * random dag size random dag size with multiple budget
	 */
	@Test
	public void testTotalRandomMultiBudget() {
		// generateDagPath = "/Users/jiyuanshi/Downloads/dags/";
		generateDagPath = "/Users/jiyuanshi/Documents/workspace/sci-cwf-sim/ams_workflow/dag/";
		//dagPath = "/gpfssan3/jishi/experiment/dags/";
		dagPath = "/Users/jiyuanshi/Documents/workspace/sci-cwf-sim/ams_workflow/dag/";
		group = "MultiType";
		// dagName = "CYBERSHAKE";
		// dagName = "GENOME";
		// dagName = "LIGO";
		// dagName = "MONTAGE";
		// dagName = "SIPHT";
		dagName = "ams";
		budget = 100;
		runtimeVariation = 0.2;
		failureRate = 0.1;
		delay = 10;
		delayVariation = 0.2;
		HashMap<String, VMDescription> vmDescription = new HashMap<String, VMDescription>();
		/** Micro vm property */
		VMDescription microProperty = new VMDescription("Micro", 1000.0, 0.02);
		vmDescription.put(microProperty.vmname, microProperty);
		/** Standard vm property */
		VMDescription standardProperty = new VMDescription("Standard", 2000.0, 0.06);
		vmDescription.put(standardProperty.vmname, standardProperty);
		/** Medium vm property */
		VMDescription mediumProperty = new VMDescription("Medium", 5000.0, 0.145);
		vmDescription.put(mediumProperty.vmname, mediumProperty);
		/** High-CPU vm property */
		VMDescription highcpuProperty = new VMDescription("High-CPU", 20000.0, 0.85);
		vmDescription.put(highcpuProperty.vmname, highcpuProperty);
		/** Extra-Large vm property */
		VMDescription extralargeProperty = new VMDescription("Extra-Large", 33500.0, 1.3);
		vmDescription.put(extralargeProperty.vmname, extralargeProperty);
		timespan = 48 * 3600.0;

		String distribution[] = { "uniform_unsorted", "uniform_sorted", "pareto_unsorted", "pareto_sorted",
				"constant" };
		String workload[] = { "bursting", "stable", "growing", "onoff" };

		for (int i = 0; i < workload.length; i++) {
			for (int j = 0; j < distribution.length; j++) {
				GenerateConfiguration.generateVaryBudgetSeries(runDirectory, group, generateDagPath, dagPath,
						dagName, budget, vmDescription, workload[i], distribution[j], timespan, max_scaling,
						taskDilatation, runtimeVariation, delayVariation, deadlineVariation, failureRate,
						informtimeratio, delay, averagejobnum, job_deviation, increments, perjob_upper,
						perjob_lower, min_jobnum, 1);
				System.out.println(workload[i]+"+"+distribution[j]+" genration completed");
			}
		}

	}

	/******************************
	 * 
	 * Tests with max scaling = 0.0
	 * 
	 ******************************/

	/**
	 * bursting workload, random priority between 1~6, random submit time,
	 * random dag size random dag size
	 */
	// @Test
	public void testTotalRandom() {
		generateDagPath = "/Users/jiyuanshi/Downloads/dags/";
		dagPath = "/gpfssan3/jishi/experiment/dags/";
		group = "CYBERSHAKE";
		dagName = "CYBERSHAKE";
		budget = 500.0;
		runtimeVariation = 0.2;
		failureRate = 0.1;
		delay = 10;
		delayVariation = 0.2;
		HashMap<String, VMDescription> vmDescription = new HashMap<String, VMDescription>();
		/** Micro vm property */
		VMDescription microProperty = new VMDescription("Micro", 1000.0, 0.02);
		vmDescription.put(microProperty.vmname, microProperty);
		/** Standard vm property */
		VMDescription standardProperty = new VMDescription("Standard", 2000.0, 0.06);
		vmDescription.put(standardProperty.vmname, standardProperty);
		/** Medium vm property */
		VMDescription mediumProperty = new VMDescription("Medium", 5000.0, 0.145);
		vmDescription.put(mediumProperty.vmname, mediumProperty);
		/** High-CPU vm property */
		VMDescription highcpuProperty = new VMDescription("High-CPU", 20000.0, 0.85);
		vmDescription.put(highcpuProperty.vmname, highcpuProperty);
		/** Extra-Large vm property */
		VMDescription extralargeProperty = new VMDescription("Extra-Large", 33500.0, 1.3);
		vmDescription.put(extralargeProperty.vmname, extralargeProperty);
		workload = "bursting";
		distribution = "constant";
		timespan = 48 * 3600.0;

		GenerateConfiguration.generateVarySeries(runDirectory, group, generateDagPath, dagPath, dagName,
				budget, vmDescription, workload, distribution, timespan, max_scaling, taskDilatation,
				runtimeVariation, delayVariation, deadlineVariation, failureRate, informtimeratio, delay,
				averagejobnum, job_deviation, increments, perjob_upper, perjob_lower, min_jobnum, 1);

	}

	/**
	 * bursting workload, random priority between 1~6, random submit time,
	 * random dag size random dag size for just on MV type
	 */
	// @Test
	public void testTotalRandomOneVMtype() {
		generateDagPath = "/Users/jiyuanshi/Downloads/dags/";
		dagPath = "/gpfssan3/jishi/experiment/dags/";
		group = "OneType";
		dagName = "CYBERSHAKE";
		budget = 200;
		HashMap<String, VMDescription> vmDescription = new HashMap<String, VMDescription>();
		/** Micro vm property */
		VMDescription microProperty = new VMDescription("Micro", 1000.0, 0.02);
		vmDescription.put(microProperty.vmname, microProperty);
		workload = "bursting";
		distribution = "constant";
		timespan = 48 * 3600.0;

		GenerateConfiguration.generateVaryBudgetSeries(runDirectory, group, generateDagPath, dagPath,
				dagName, budget, vmDescription, workload, distribution, timespan, max_scaling,
				taskDilatation, runtimeVariation, delayVariation, deadlineVariation, failureRate,
				informtimeratio, delay, averagejobnum, job_deviation, increments, perjob_upper, perjob_lower,
				min_jobnum, 1);

	}

	/**
	 * bursting workload, random priority between 1~6, random submit time.
	 */
	// @Test
	public void testCyberShake_100() {
		group = "CyberShake_1000.dag";
		dagName = "CyberShake_1000.dag";
		budget = 100.0;
		HashMap<String, VMDescription> vmDescription = new HashMap<String, VMDescription>();
		/** Micro vm property */
		VMDescription microProperty = new VMDescription("Micro", 1000.0, 0.02);
		vmDescription.put(microProperty.vmname, microProperty);
		/** Standard vm property */
		VMDescription standardProperty = new VMDescription("Standard", 2000.0, 0.06);
		vmDescription.put(standardProperty.vmname, standardProperty);
		/** Medium vm property */
		VMDescription mediumProperty = new VMDescription("Medium", 5000.0, 0.145);
		vmDescription.put(mediumProperty.vmname, mediumProperty);
		/** High-CPU vm property */
		VMDescription highcpuProperty = new VMDescription("High-CPU", 20000.0, 0.85);
		vmDescription.put(highcpuProperty.vmname, highcpuProperty);
		/** Extra-Large vm property */
		VMDescription extralargeProperty = new VMDescription("Extra-Large", 33500.0, 1.3);
		vmDescription.put(extralargeProperty.vmname, extralargeProperty);
		workload = "bursting";
		timespan = 48 * 3600.0;

		GenerateConfiguration.generateConstantSeries(runDirectory, group, dagPath, dagName, budget,
				vmDescription, workload, timespan, max_scaling, taskDilatation, runtimeVariation,
				delayVariation, deadlineVariation, failureRate, informtimeratio, delay, averagejobnum,
				job_deviation, increments, perjob_upper, perjob_lower, min_jobnum);

	}

	// @Test
	public void testRunOneCyberShake_100() {
		dagName = "CyberShake_100.dag";
		Double budget = 1000.0;
		max_scaling = 0;
		group = "testRunOneCyberShake_100";
		ArrayList<DAGDescription> dags = new ArrayList<DAGDescription>();
		for (int i = 0; i < 1000; i++) {
			DAGDescription dag = new DAGDescription(dagName, dagPath, 0.0, timespan, i);
			dags.add(dag);
		}

		HashMap<String, VMDescription> vms = new HashMap<String, VMDescription>();
		VMDescription vmDescription = new VMDescription("Micro", 1000.0, 0.02);
		vms.put(vmDescription.vmname, vmDescription);

		ExperimentDescription param;
		String fileName;

		String algorithms[] = { "DPDS", "WADPDS", "SFPDS", "PPDS" };

		for (String alg : algorithms) {
			param = new ExperimentDescription(group, alg, runDirectory, dagPath, dags, "Stable", timespan,
					budget, vms, max_scaling, 0, taskDilatation, runtimeVariation, delayVariation,
					deadlineVariation, failureRate, informtimeratio, delay, "Constant", averagejobnum,
					job_deviation, increments, perjob_upper, perjob_lower, min_jobnum);
			fileName = "input-" + param.getFileName() + ".properties";
			param.storeProperties(runDirectory + File.separator + fileName);
		}

	}

	/**
	 * specific dag jobs with random submit time
	 */
	// @Test
	public void testRunOneCyberShake_randomsubtime_100() {
		// generateDagPath = "/Users/jiyuanshi/Downloads/dags/";
		generateDagPath = "/Users/jiyuanshi/Documents/workspace/sci-cwf-sim/ams_workflow/dag/";
		dagPath = "/gpfssan3/jishi/experiment/dags/";
		group = "MultiType";
		dagName = "ams.n.900.3.dag";
		Double budget = 1000.0;
		max_scaling = 0;
		group = "testRunOneCyberShake_100";
		ArrayList<DAGDescription> dags = new ArrayList<DAGDescription>();
		UniformIntegerDistribution distribution = new UniformIntegerDistribution(0, 30 * 3600);
		Double subtimeDouble = 0.0;
		for (int i = 0; i < 100; i++) {
			subtimeDouble = distribution.sample() * 1.0;
			DAGDescription dag = new DAGDescription(dagName, dagPath, subtimeDouble, timespan, i);
			dags.add(dag);
		}

		HashMap<String, VMDescription> vms = new HashMap<String, VMDescription>();
		VMDescription vmDescription = new VMDescription("test", 1000.0, 0.02);
		vms.put(vmDescription.vmname, vmDescription);

		ExperimentDescription param;
		String fileName;

		String algorithms[] = { "DPDS", "WADPDS", "SFPDS", "PPDS" };

		for (String alg : algorithms) {
			param = new ExperimentDescription(group, alg, runDirectory, dagPath, dags, "Stable", timespan,
					budget, vms, max_scaling, 0, taskDilatation, runtimeVariation, delayVariation,
					deadlineVariation, failureRate, informtimeratio, delay, "Constant", averagejobnum,
					job_deviation, increments, perjob_upper, perjob_lower, min_jobnum);
			fileName = "input-" + param.getFileName() + ".properties";
			param.storeProperties(runDirectory + File.separator + fileName);
		}

	}

}
