package test.core.dag;

import java.io.File;

import scws.core.dag.DAG;
import scws.core.dag.DAGParser;

public class DAGParser_test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DAG dag = DAGParser.parseDAG(new File("ams_workflow/AMS_dag/ams.n.500.15.dag"), "BlackDiamonDAX.dag");
		System.out.println("getDagname: " + dag.getDagname());
		System.out.println("getFiles: " + dag.getFiles().length);
		System.out.println("getTasks: " + dag.getTasks().length);
		System.out.println("getStartTasks: " + dag.getStartTasks());
		System.out.println("getEndTasks: " + dag.getEndTasks());
		System.out.println("getVirtualStartTask: " + dag.getVirtualStartTask());
		System.out.println("getVirtualEndTask: " + dag.getVirtualEndTask());
		
	}

}
