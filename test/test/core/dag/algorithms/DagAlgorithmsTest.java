package test.core.dag.algorithms;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;

import org.junit.Test;

import scws.core.dag.DAGPartition;
import scws.core.dag.Task;
import scws.core.dag.TaskPartition;

import scws.core.dag.DAG;
import scws.core.dag.algorithms.DeadlineDistribution;

public class DagAlgorithmsTest {

	public DAG CreateDag1() {
		DAG testdag = new DAG("testdag");
		Task t1 = new Task("t1", "test::t1", 50);
		Task t2 = new Task("t2", "test::t2", 50);
		Task t3 = new Task("t3", "test::t3", 40);
		Task t4 = new Task("t4", "test::t4", 30);
		Task t5 = new Task("t5", "test::t5", 150);
		Task t6 = new Task("t6", "test::t6", 110);
		Task t7 = new Task("t7", "test::t7", 20);
		Task t8 = new Task("t8", "test::t8", 20);
		Task t9 = new Task("t9", "test::t9", 10);
		Task t10 = new Task("t10", "test::t10", 15);
		Task t11 = new Task("t11", "test::t11", 40);
		Task t12 = new Task("t12", "test::t12", 30);
		Task t13 = new Task("t13", "test::t13", 20);
		Task t14 = new Task("t14", "test::t14", 70);
		testdag.addTask(t1);
		testdag.addTask(t2);
		testdag.addTask(t3);
		testdag.addTask(t4);
		testdag.addTask(t5);
		testdag.addTask(t6);
		testdag.addTask(t7);
		testdag.addTask(t8);
		testdag.addTask(t9);
		testdag.addTask(t10);
		testdag.addTask(t11);
		testdag.addTask(t12);
		testdag.addTask(t13);
		testdag.addTask(t14);
		testdag.addEdge("t1", "t2");
		testdag.addEdge("t2", "t3");
		testdag.addEdge("t3", "t4");
		testdag.addEdge("t4", "t14");
		testdag.addEdge("t1", "t5");
		testdag.addEdge("t5", "t6");
		testdag.addEdge("t6", "t14");
		testdag.addEdge("t1", "t7");
		testdag.addEdge("t7", "t10");
		testdag.addEdge("t10", "t11");
		testdag.addEdge("t11", "t14");
		testdag.addEdge("t1", "t8");
		testdag.addEdge("t8", "t9");
		testdag.addEdge("t9", "t10");
		testdag.addEdge("t10", "t12");
		testdag.addEdge("t12", "t13");
		testdag.addEdge("t13", "t14");
		return testdag;
	}
	
	public DAG CreateDag2() {
		DAG testdag = new DAG("testdag");
		Task t0 = new Task("t0", "test::t0", 30);
		Task t1 = new Task("t1", "test::t1", 90);
		Task t2 = new Task("t2", "test::t2", 50);
		Task t3 = new Task("t3", "test::t3", 40);
		Task t4 = new Task("t4", "test::t4", 30);
		Task t5 = new Task("t5", "test::t5", 150);
		Task t6 = new Task("t6", "test::t6", 110);
		Task t7 = new Task("t7", "test::t7", 20);
		Task t8 = new Task("t8", "test::t8", 20);
		Task t9 = new Task("t9", "test::t9", 30);
		Task t10 = new Task("t10", "test::t10", 20);
		Task t11 = new Task("t11", "test::t11", 40);
		Task t12 = new Task("t12", "test::t12", 30);
		Task t13 = new Task("t13", "test::t13", 40);
		testdag.addTask(t0);
		testdag.addTask(t1);
		testdag.addTask(t2);
		testdag.addTask(t3);
		testdag.addTask(t4);
		testdag.addTask(t5);
		testdag.addTask(t6);
		testdag.addTask(t7);
		testdag.addTask(t8);
		testdag.addTask(t9);
		testdag.addTask(t10);
		testdag.addTask(t11);
		testdag.addTask(t12);
		testdag.addTask(t13);
		testdag.addEdge("t1", "t2");
		testdag.addEdge("t1", "t5");
		testdag.addEdge("t2", "t3");
		testdag.addEdge("t3", "t4");
		testdag.addEdge("t5", "t4");
		testdag.addEdge("t5", "t6");
		testdag.addEdge("t10", "t11");
		testdag.addEdge("t10", "t12");
		testdag.addEdge("t12", "t13");
		testdag.addEdge("t10", "t5");
		testdag.addEdge("t7", "t10");
		testdag.addEdge("t9", "t10");
		testdag.addEdge("t0", "t7");
		testdag.addEdge("t8", "t9");
		return testdag;
	}
	
	public DAG CreateDag3() {
		DAG testdag = new DAG("testdag");
		Task t0 = new Task("t0", "test::t0", 20);
		Task t1 = new Task("t1", "test::t1", 40);
		Task t2 = new Task("t2", "test::t2", 30);
		Task t3 = new Task("t3", "test::t3", 50);
		Task t4 = new Task("t4", "test::t4", 50);
		Task t5 = new Task("t5", "test::t5", 20);
		Task t6 = new Task("t6", "test::t6", 20);
		Task t7 = new Task("t7", "test::t7", 30);
		Task t8 = new Task("t8", "test::t8", 20);
		Task t9 = new Task("t9", "test::t9", 150);
		Task t10 = new Task("t10", "test::t10", 40);
		Task t11 = new Task("t11", "test::t11", 30);
		Task t12 = new Task("t12", "test::t12", 30);
		Task t13 = new Task("t13", "test::t13", 110);
		Task t14 = new Task("t14", "test::t14", 40);
		Task t15 = new Task("t15", "test::t15", 20);
		Task t16 = new Task("t16", "test::t16", 70);
		Task t17 = new Task("t17", "test::t17", 30);
		Task t18 = new Task("t18", "test::t18", 20);
		Task t19 = new Task("t19", "test::t19", 50);
		testdag.addTask(t0);
		testdag.addTask(t1);
		testdag.addTask(t2);
		testdag.addTask(t3);
		testdag.addTask(t4);
		testdag.addTask(t5);
		testdag.addTask(t6);
		testdag.addTask(t7);
		testdag.addTask(t8);
		testdag.addTask(t9);
		testdag.addTask(t10);
		testdag.addTask(t11);
		testdag.addTask(t12);
		testdag.addTask(t13);
		testdag.addTask(t14);
		testdag.addTask(t15);
		testdag.addTask(t16);
		testdag.addTask(t17);
		testdag.addTask(t18);
		testdag.addTask(t19);
		testdag.addEdge("t0", "t3");
		testdag.addEdge("t1", "t3");
		testdag.addEdge("t2", "t3");
		testdag.addEdge("t3", "t4");
		testdag.addEdge("t3", "t9");
		testdag.addEdge("t3", "t5");
		testdag.addEdge("t3", "t6");
		testdag.addEdge("t6", "t7");
		testdag.addEdge("t7", "t8");
		testdag.addEdge("t5", "t8");
		testdag.addEdge("t4", "t10");
		testdag.addEdge("t10", "t12");
		testdag.addEdge("t9", "t13");
		testdag.addEdge("t8", "t14");
		testdag.addEdge("t8", "t11");
		testdag.addEdge("t11", "t15");
		testdag.addEdge("t14", "t16");
		testdag.addEdge("t15", "t16");
		testdag.addEdge("t13", "t16");
		testdag.addEdge("t12", "t16");
		testdag.addEdge("t16", "t17");
		testdag.addEdge("t16", "t18");
		testdag.addEdge("t16", "t19");
		return testdag;
	}

	//@Test
	public void testDagGetStartAndEnd() {
		DAG dag = CreateDag3();

		System.out.println("StartTasks: " + dag.getVirtualStartTask().children);
		System.out.println("EndTasks: " + dag.getVirtualEndTask().parents);
	}

	@Test
	public void testDeadlineDistribute() {
		DAG dag = CreateDag3();
		Double subtime = 0.0;
		Double deadline = 600.0;
		DeadlineDistribution.DistributeDeadline(dag, subtime, deadline);
		DeadlineDistribution.DistributeDeadline(dag, subtime, deadline);
		DAGPartition dagPartition = DeadlineDistribution.DagPartition();
		for (String id : dagPartition.getTasks()) {
			System.out.println("id: " + id);
			System.out.println("parents: " + dagPartition.getTask(id).parents);
			System.out.println("childrens: " + dagPartition.getTask(id).children);
			System.out.println("tasks: " + dagPartition.getTask(id).tasks);
			System.out.println("size: " + dagPartition.getTask(id).getSize());
		}
		System.out.println("~~~~~~~~~~");
		for (Integer lever : DeadlineDistribution.PartitionLever.keySet()) {
			System.out.println("lever " + lever + ": " + DeadlineDistribution.PartitionLever.get(lever));
		}
		System.out.println("Postorder~~~~~~~~~~");
		System.out.println(DeadlineDistribution.Postorder);
		System.out.println("Topologicorder~~~~~~~~~~");
		for (int i = DeadlineDistribution.Postorder.size() - 1; i >= 0; i--) {
			System.out.print(DeadlineDistribution.Postorder.get(i) + ", ");
		}
		System.out.println();
		System.out.println("Preorder~~~~~~~~~~");
		System.out.println(DeadlineDistribution.Preorder);
		System.out.println("CriticalPath:"+DeadlineDistribution.Critacalpath);
		System.out.println("CriticalPath~~~~~~~~~~size: " + DeadlineDistribution.CriticalPathSize);
		for (TaskPartition taskPartition : DeadlineDistribution.Critacalpath) {
			System.out.println(taskPartition + "~subdeadline:" + taskPartition.subdealine);
		}
		System.out.println("task partition subdeadline~~~~~~~~~~:");
		for (ArrayList<TaskPartition> iterable_element : DeadlineDistribution.PartitionLever.values()) {
			for (TaskPartition taskPartition : iterable_element) {
				System.out.println("taskpartition:" + taskPartition+"~tasks:" +taskPartition.tasks+ "~lever:" + taskPartition.lever
						+ "~subdeadline:" + taskPartition.subdealine);
			}
		}
		System.out.println("task's subdeadline~~~~~~~~~~:");
		for (String taskname : dag.getTasks()) {
			System.err.println("task:"+taskname+"~subdeadline:"+dag.getTask(taskname).subdealine);
		}
	}

}
