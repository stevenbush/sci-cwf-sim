package test.core.dag;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

import scws.core.VMDescription;
import scws.core.dag.DAG;
import scws.core.dag.DAGParser;
import scws.core.dag.DAGStats;

public class DAG_test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DAG dag = DAGParser.parseDAG(new File("dags/BlackDiamonDAX.dag"),"BlackDiamonDAX.dag");
		Double taskDilatation = 1000.0;
		HashMap<String, VMDescription> vmDescription = new HashMap<String, VMDescription>();
		/** Micro vm property */
		VMDescription microProperty = new VMDescription("Micro", 1000.0, 0.02);
		vmDescription.put(microProperty.vmname, microProperty);
		/** Standard vm property */
		VMDescription standardProperty = new VMDescription("Standard", 2000.0, 0.06);
		vmDescription.put(standardProperty.vmname, standardProperty);
		/** Medium vm property */
		VMDescription mediumProperty = new VMDescription("Medium", 5000.0, 0.145);
		vmDescription.put(mediumProperty.vmname, mediumProperty);
		/** High-CPU vm property */
		VMDescription highcpuProperty = new VMDescription("High-CPU", 20000.0, 0.85);
		vmDescription.put(highcpuProperty.vmname, highcpuProperty);
		/** Extra-Large vm property */
		VMDescription extralargeProperty = new VMDescription("Extra-Large", 33500.0, 1.3);
		vmDescription.put(extralargeProperty.vmname, extralargeProperty);

		DAGStats dagStats = new DAGStats(dag, taskDilatation, vmDescription);

		System.out.println("MaxTotalRuntime: " + dagStats.getMaxTotalRuntime());
		System.out.println("MinTotalRuntime: " + dagStats.getMinTotalRuntime());
		System.out.println("MaxCriticalPath: " + dagStats.getMaxCriticalPath());
		System.out.println("MinCriticalPath: " + dagStats.getMinCriticalPath());

		try {
			BufferedWriter file_runtime_distri = new BufferedWriter(new FileWriter("runtime_distribution"));

			for (Double tasksize : dagStats.gettasksize()) {
				file_runtime_distri.write(String.valueOf(tasksize));
				file_runtime_distri.newLine();
			}

			file_runtime_distri.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
