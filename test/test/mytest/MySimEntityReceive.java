package test.mytest;

import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.core.SimEntity;
import org.cloudbus.cloudsim.core.SimEvent;

public class MySimEntityReceive extends SimEntity {

	public MySimEntityReceive(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void startEntity() {
		// TODO Auto-generated method stub
		Log.printLine("Name: " + getName());
		Log.printLine("Nano time: "+ System.currentTimeMillis());
		Log.printLine("Now CloudSim time: " + CloudSim.clock());

	}

	@Override
	public void processEvent(SimEvent ev) {
		// TODO Auto-generated method stub
		Log.printLine("Nano time: "+ System.currentTimeMillis());
		Log.printLine("Now CloudSim time: " + CloudSim.clock());
		Log.printLine("receive a message: "+String.valueOf(ev.getData()));

	}

	@Override
	public void shutdownEntity() {
		// TODO Auto-generated method stub

	}

}
