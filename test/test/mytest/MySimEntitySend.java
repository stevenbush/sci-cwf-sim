package test.mytest;

import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.core.SimEntity;
import org.cloudbus.cloudsim.core.SimEvent;

public class MySimEntitySend extends SimEntity {

	public MySimEntitySend(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void startEntity() {
		// TODO Auto-generated method stub
		Log.printLine("Name: " + getName());
		Log.printLine("Nano time: "+ System.currentTimeMillis());
		Log.printLine("Now CloudSIm time: " + CloudSim.clock());
		send("receiver", 6.1, 1, "10 seconds later");
		send("receiver", 6, 1, "5 seconds later");
		Log.printLine(CloudSim.clock()+" :Message sended");

	}

	@Override
	public void processEvent(SimEvent ev) {
		// TODO Auto-generated method stub

	}

	@Override
	public void shutdownEntity() {
		// TODO Auto-generated method stub

	}

}
