package test.mytest;

import java.math.BigDecimal;
import java.util.ArrayList;

import org.apache.commons.math3.distribution.UniformIntegerDistribution;

import scws.core.DAGJob;
import scws.core.WorkflowEngine;

class element {
	public element(Integer priority, Integer cost) {
		this.priority = priority;
		this.cost = cost;
		this.flag = false;
	}

	public Integer priority = 0;
	public Integer cost = 0;
	public Boolean flag = false;

	public String toString() {
		return cost.toString() + "~" + priority.toString();
	}

}

public class DynamicProgrammingTest {

	private static Boolean DAGJobReplacement(Integer budgetGap, element originalElement,
			ArrayList<element> curreset) {
		Double[][] dpMatrix = new Double[curreset.size() + 1][budgetGap + 1];
		// System.out.println("ilen:" + dpMatrix.length + "~ylen" +
		// dpMatrix[0].length);
		for (int i = 0; i <= budgetGap; i++) {
			dpMatrix[0][i] = Double.MAX_VALUE;
		}
		for (int i = 0; i <= curreset.size(); i++) {
			dpMatrix[i][0] = 0.0;
		}

		for (int i = 1; i <= curreset.size(); i++) {
			for (int j = 1; j <= budgetGap; j++) {
				Double sumCost = 0.0;
				for (int k = 1; k <= i; k++) {
					sumCost = sumCost + curreset.get(i - 1).cost;
				}

				if (sumCost < j) {
					dpMatrix[i][j] = Double.MAX_VALUE;
				} else {
					Integer tmpj = j;
					if (j < curreset.get(i - 1).cost) {
						tmpj = curreset.get(i - 1).cost;
					}
					// System.out.println("i:" + i + "~j:" + j);
					Double tmpValue = getExponentialScore(curreset.get(i - 1).priority)
							+ dpMatrix[i - 1][(tmpj - curreset.get(i - 1).cost)];
					dpMatrix[i][j] = Math.min(tmpValue, dpMatrix[i - 1][j]);
				}
			}
		}

		if (dpMatrix[curreset.size()][budgetGap] < getExponentialScore(originalElement.priority)) {
			Integer currentGap = budgetGap;
			for (int i = curreset.size(); i > 0; i--) {
				// System.out.println("i:" + i + "~currentGap:" + currentGap);
				if (currentGap < 0) {
					break;
				}
				if (dpMatrix[i][currentGap] < dpMatrix[i - 1][currentGap]) {
					curreset.get(i - 1).flag = true;
					currentGap = currentGap - curreset.get(i - 1).cost;
				}
			}
			return true;
		} else {
			return false;
		}

	}

	public static double getExponentialScore(Integer priority) {
		BigDecimal one = BigDecimal.ONE;
		BigDecimal two = new BigDecimal(2.0);

		BigDecimal divisor = two.pow(priority);
		BigDecimal score = one.divide(divisor);

		return score.doubleValue();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		UniformIntegerDistribution distributionPriority = new UniformIntegerDistribution(0, 6);
		UniformIntegerDistribution distributionCost = new UniformIntegerDistribution(10, 40);
		element original = new element(3, 50);

		ArrayList<element> currenSet = new ArrayList<element>();
		for (int i = 0; i < 10; i++) {
			currenSet.add(new element(distributionPriority.sample(), distributionCost.sample()));
		}
		for (element element : currenSet) {
			System.out.println(element.toString());
		}
		System.out.println("~~~~~~~~~~~~~");

		Integer budgetgap = 30;
		System.out.println("original:" + original.cost + "~" + original.priority);
		System.out.println("~~~~~~~~~~");
		System.out.println("gap:" + budgetgap);
		System.out.println("~~~~~~~~~~");
		if (DAGJobReplacement(budgetgap, original, currenSet)) {
			
			for (element element : currenSet) {
				if (element.flag) {
					System.out.println(element.toString());
				}
			}

		} else {
			System.out.println("no element can be replacement");
		}

	}

}
