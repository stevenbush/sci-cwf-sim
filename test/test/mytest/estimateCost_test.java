package test.mytest;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import org.cloudbus.cloudsim.core.CloudSim;

import scws.core.Cloud;
import scws.core.DAGJob;
import scws.core.SimpleJobFactory;
import scws.core.VMDescription;
import scws.core.VMManger;
import scws.core.WorkflowEngine;
import scws.core.dag.DAG;
import scws.core.dag.DAGParser;
import scws.core.dag.algorithms.DeadlineDistribution;
import scws.core.experiment.VMFactory;
import scws.core.provisioner.PreProcessingProvisioner;
import scws.core.scheduler.WorkflowAwarePreProcessingScheduler;

public class estimateCost_test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		HashMap<String, VMDescription> vmDescription = new HashMap<String, VMDescription>();
		/** Micro vm property */
		VMDescription microProperty = new VMDescription("Micro", 1000.0, 0.02);
		vmDescription.put(microProperty.vmname, microProperty);
		/** Standard vm property */
		VMDescription standardProperty = new VMDescription("Standard", 2000.0, 0.06);
		vmDescription.put(standardProperty.vmname, standardProperty);
		/** Medium vm property */
		VMDescription mediumProperty = new VMDescription("Medium", 5000.0, 0.145);
		vmDescription.put(mediumProperty.vmname, mediumProperty);
		/** High-CPU vm property */
		VMDescription highcpuProperty = new VMDescription("High-CPU", 20000.0, 0.85);
		vmDescription.put(highcpuProperty.vmname, highcpuProperty);
		/** Extra-Large vm property */
		VMDescription extralargeProperty = new VMDescription("Extra-Large", 33500.0, 1.3);
		vmDescription.put(extralargeProperty.vmname, extralargeProperty);

		String dagPath = "/Users/jiyuanshi/Downloads/dags/";

		String dagName = "CYBERSHAKE.n.50.6.dag";

		CloudSim.init(1, null, false);

		Cloud cloud = new Cloud();
		WorkflowAwarePreProcessingScheduler scheduler = new WorkflowAwarePreProcessingScheduler("");

		WorkflowEngine engine = new WorkflowEngine(new SimpleJobFactory(1000.0),
				new PreProcessingProvisioner(), scheduler, 172800.0, 1500.0);
		ArrayList<VMManger> vmMangers = new ArrayList<VMManger>();
		for (VMDescription vm : vmDescription.values()) {
			VMManger vmManger = new VMManger(vm.vmname, vm.price, vm.mips, 1, 1.0,
					new PreProcessingProvisioner(), scheduler, engine, cloud);
			vmMangers.add(vmManger);
		}

		engine.setVmMangers(vmMangers);
		DAG dag = DAGParser.parseDAG(new File(dagPath + "/" + dagName), dagName);
		DAGJob dagJob = new DAGJob(dag, 2);
		dagJob.setSubtime(172775.1587110615);
		dagJob.setDeadline(172800.0);
		DeadlineDistribution.DistributeDeadline(dagJob.getDAG(), dagJob.getSubtime(), dagJob.getDeadline());
		System.out.println(scheduler.estimateCost(dagJob, engine));

	}
}
