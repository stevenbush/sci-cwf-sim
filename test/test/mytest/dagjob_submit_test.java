package test.mytest;

import java.util.ArrayList;

import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.core.CloudSim;

import scws.core.Cloud;
import scws.core.DAGDescription;
import scws.core.SimpleJobFactory;
import scws.core.VMDescription;
import scws.core.VMManger;
import scws.core.WorkflowEngine;
import scws.core.WorkloadManager;
import scws.core.experiment.ExperimentDescription;
import scws.core.provisioner.SimpleUtilizationBasedProvisioner;
import scws.core.scheduler.DAGDynamicScheduler;

public class dagjob_submit_test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String configfile = "output/input-Constant-DPDS-constant-5t172800.0w5numberb80.0m2.0t1000.0v0.0-0.5l0.0r0.properties";
		ExperimentDescription param = new ExperimentDescription(configfile);
		ArrayList<DAGDescription> dags = param.getDags();
		System.out.println(dags.size());

		CloudSim.init(1, null, false);

		DAGDynamicScheduler scheduler = new DAGDynamicScheduler();
		SimpleUtilizationBasedProvisioner provisioner = new SimpleUtilizationBasedProvisioner(
				param.getMaxScaling());
		WorkflowEngine engine = new WorkflowEngine(new SimpleJobFactory(1000), provisioner, scheduler,
				48 * 3600.0, Double.MAX_VALUE);
		scheduler.setWorkflowEngine(engine);

		ArrayList<VMManger> vmMangers = new ArrayList<VMManger>();
		for (VMDescription vmDescription : param.getVmDescription().values()) {
			VMManger vmManger = new VMManger(vmDescription.vmname, vmDescription.price, vmDescription.mips,
					1, 1.0, provisioner, scheduler, engine, new Cloud());
			vmMangers.add(vmManger);
		}
		engine.setVmMangers(vmMangers);

		WorkloadManager wm = new WorkloadManager(dags, engine);

		for (DAGDescription dag : dags) {
			Log.printLine(dag.dagpath + "-" + dag.filename + "-" + dag.subtime + "-" + dag.deadline + "-"
					+ dag.priority);
		}

		CloudSim.startSimulation();
	}
}
