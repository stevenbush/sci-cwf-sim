package scws.core;

/**
 * This is a interface that can be implemented for the get actual runtime method.
 * @author jiyuanshi <shijiyuan.seu@gmail.com>
 * @since 2013-4-16
 * @version sci-cwf-sim 1.0
 */
public interface RuntimeDistribution {
	public double getActualRuntime(double runtime);
}
