package scws.core.scheduler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;

import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.core.CloudSim;

import scws.core.DAGJob;
import scws.core.Job;
import scws.core.Scheduler;
import scws.core.VMInstance;
import scws.core.VMManger;
import scws.core.WorkflowEngine;
import scws.core.WorkflowEvent;

public class ScalingFirstScheduler extends DAGDynamicScheduler {

	@Override
	public void scheduleJobs(WorkflowEngine engine) {

		// check the deadline constraints (provisioner takes care about budget)
		double timespan = engine.getTimespan();
		double time = CloudSim.clock();

		// stop scheduling any new jobs if we are over timespan
		if (time >= timespan) {
			engine.getQueuedJobs().clear();
			engine.getDags().clear();
			return;
		}

		// get the queued (released) jobs from the workflow engine
		Queue<Job> jobs = engine.getQueuedJobs();

		ArrayList<VMManger> vmMangers = engine.getVmMangers();
		// the comparator for comparing two VMMangers
		Comparator<VMManger> comparator = new Comparator<VMManger>() {
			public int compare(VMManger v1, VMManger v2) {
				if (v1.getMips() < v2.getMips()) {
					return 1;
				} else if (v1.getMips() > v2.getMips()) {
					return -1;
				} else {
					return 0;
				}
			}
		};

		// sort all the vmmanger according the mips from large to small
		Collections.sort(vmMangers, comparator);

		for (VMManger vmManger : vmMangers) {
			int i = 0;
			while (i < vmManger.getFreeVMs().size() && !jobs.isEmpty()) {
				Job job = jobs.poll();
				if (time > job.getDAGJob().getDeadline()) {
					engine.getDags().remove(job.getDAGJob());
					job.setResult(Job.Result.FAILURE);
					job.setState(Job.State.TERMINATED);
				} else {
					job.setVmManger(vmManger);
					// Log.printLine(CloudSim.clock() + " Submitting job " +
					// job.getID() + " to VMManger "
					// + job.getVmManger().getName() +
					// job.getVmManger().getId());
					job.setSubmitTime(CloudSim.clock());
					job.setState(Job.State.IDLE);
					CloudSim.send(engine.getId(), vmManger.getId(), 0.0, JOB_SUBMIT, job);
					i++;
				}

			}

		}

	}

}
