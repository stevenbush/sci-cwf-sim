/**
 * 
 */
package scws.core.scheduler;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Queue;

import org.cloudbus.cloudsim.core.CloudSim;

import scws.core.DAGJob;
import scws.core.Job;
import scws.core.VMManger;
import scws.core.WorkflowEngine;
import scws.core.dag.algorithms.DeadlineDistribution;

/**
 * This class implement my PSS algorithm that first divide the overall deadline
 * into sub-deadline and assign them to every task, then according every task's
 * deadline to schedule it to the suitable VM type that can complete it before
 * the deadline with cheapest cost
 * 
 * @author jiyuanshi <shijiyuan.seu@gmail.com>
 * @since 2013-06-05
 * @version sci-cwf-sim 1.0
 * 
 */
public class PreProcessingScalingScheduler extends DAGDynamicScheduler {

	private String logFilePath;

	public PreProcessingScalingScheduler(String logFilePath) {
		this.logFilePath = logFilePath;
	}

	/**
	 * doing the preprocessing for the submitted dag job: divide the overall
	 * deadline into sub-sdealine and distribute them to every tasks
	 */
	@Override
	public void preprocessDagJob(DAGJob dagJob) {
		long begin = System.nanoTime();

		DeadlineDistribution.DistributeDeadline(dagJob.getDAG(), dagJob.getSubtime(), dagJob.getDeadline());

		long duration = System.nanoTime() - begin;
		try {
			String filenpath = logFilePath + "-disDeadlineTime";
			BufferedWriter filewriter = new BufferedWriter(new FileWriter(filenpath, true));

			filewriter.write(dagJob.getDAG().getTasks().length + "~" + duration);
			filewriter.newLine();
			filewriter.flush();
			filewriter.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * according to the sub-deadline of every jobs to schedule them to suitable
	 * VM type, the selection principle is to chose the cheapest VM type that
	 * can complete this job before sub-deadline, if a job is already exceeded
	 * it's deadline, we will schedule it to the fastest VM type. if the current
	 * time exceeded the total timespan or the dag's total deadline we will end
	 * this scheduling.
	 */
	@Override
	public void scheduleJobs(WorkflowEngine engine) {

		// check the time constraints (provisioner takes care about budget)
		double timespan = engine.getTimespan();
		double time = CloudSim.clock();

		// stop scheduling any new jobs if we are over timespan
		if (time >= timespan) {
			engine.getQueuedJobs().clear();
			engine.getDags().clear();
			return;
		}

		// get the queued (released) jobs from the workflow engine
		Queue<Job> jobs = engine.getQueuedJobs();
		// get all the VM Mangers
		ArrayList<VMManger> vmMangers = engine.getVmMangers();

		// the comparator for comparing two VMMangers
		Comparator<VMManger> comparator = new Comparator<VMManger>() {
			public int compare(VMManger v1, VMManger v2) {
				if (v1.getMips() < v2.getMips()) {
					return 1;
				} else if (v1.getMips() > v2.getMips()) {
					return -1;
				} else {
					return 0;
				}
			}
		};

		// sort all the vmmanger according the mips from large to small
		Collections.sort(vmMangers, comparator);
		VMManger mostPowerfullVMManger = vmMangers.get(0);

		// according the sub-deadline to scheudle the job to the suitable VM
		// type, if the deadline of this job's dagjob is exceeded, we just
		// ignore this job and do not schedule
		while (!jobs.isEmpty()) {
			Job job = jobs.poll();
			// if the current time exceed the dag's overall deadline we just
			// ignore this Dag job.
			if (time > job.getDAGJob().getDeadline()) {
				engine.getDags().remove(job.getDAGJob());
				job.setResult(Job.Result.FAILURE);
				job.setState(Job.State.TERMINATED);
			} else {
				if (time > job.getSubdeadline()) {
					job.setVmManger(mostPowerfullVMManger);
					// Log.printLine(CloudSim.clock() +
					// " Submitting exceed sud-deadline job " + job.getID()
					// + " to most powerfull VMManger " +
					// job.getVmManger().getName()
					// + job.getVmManger().getId());
					job.setSubmitTime(CloudSim.clock());
					job.setState(Job.State.IDLE);
					CloudSim.send(engine.getId(), mostPowerfullVMManger.getId(), 0.0, JOB_SUBMIT, job);
				} else {
					Double minmunCost = Double.MAX_VALUE;
					VMManger minCostVMManger = mostPowerfullVMManger;
					// find the cheapest VM type that can complete the job
					// before the job's sub-deadline
					for (VMManger vmManger : vmMangers) {
						Double timecost = job.getSize() / vmManger.getMips();
						Double vmcost = Math.ceil(timecost / 3600) * vmManger.getPrice();
						if (vmcost < minmunCost && time + timecost < job.getSubdeadline()) {
							minmunCost = vmcost;
							minCostVMManger = vmManger;
						}
					}
					job.setVmManger(minCostVMManger);
					// Log.printLine(CloudSim.clock() + " Submitting job " +
					// job.getID() + " to VMManger "
					// + job.getVmManger().getName() +
					// job.getVmManger().getId());
					job.setSubmitTime(CloudSim.clock());
					job.setState(Job.State.IDLE);
					CloudSim.send(engine.getId(), minCostVMManger.getId(), 0.0, JOB_SUBMIT, job);
				}
			}
		}

	}
}
