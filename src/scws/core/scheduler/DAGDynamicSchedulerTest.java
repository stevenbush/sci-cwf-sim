package scws.core.scheduler;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;

import org.cloudbus.cloudsim.core.CloudSim;
import org.junit.Test;

import scws.core.log.WorkflowLog;
import scws.core.Cloud;
import scws.core.DAGDescription;
import scws.core.SimpleJobFactory;
import scws.core.VMDescription;
import scws.core.VMInstance;
import scws.core.VMManger;
import scws.core.WorkflowEngine;
import scws.core.WorkloadManager;
import scws.core.provisioner.SimpleUtilizationBasedProvisioner;
import scws.core.scheduler.DAGDynamicScheduler;
import scws.core.Provisioner;
import scws.core.WorkflowEvent;

public class DAGDynamicSchedulerTest implements WorkflowEvent {

	// @Test
	public void testScheduleVMS() {
		VMDescription vmDescription = new VMDescription("testvm", 1000.0, 0.018);
		CloudSim.init(1, null, false);
		Cloud cloud = new Cloud();
		Provisioner provisioner = null;
		DAGDynamicScheduler scheduler = new DAGDynamicScheduler();
		WorkflowEngine engine = new WorkflowEngine(new SimpleJobFactory(1000), provisioner, scheduler,
				48 * 3600.0, Double.MAX_VALUE);
		VMManger vmManger = new VMManger(vmDescription.vmname, vmDescription.price, vmDescription.mips, 1,
				1.0, provisioner, scheduler, engine, cloud);

		HashSet<VMInstance> vms = new HashSet<VMInstance>();

		for (int i = 0; i < 10; i++) {
			VMInstance vmInstance = new VMInstance(vmManger, vmManger.getMips(), vmManger.getCores(),
					vmManger.getBandwidth(), vmManger.getPrice());
			vmInstance.setProvisioningDelay(0.0);
			vmInstance.setDeprovisioningDelay(0.0);
			vms.add(vmInstance);
			CloudSim.send(vmManger.getId(), cloud.getId(), 0.1, VM_LAUNCH, vmInstance);
		}

		CloudSim.startSimulation();

		System.out.println("AvailableVMs: " + vmManger.getAvailableVMs().size());
		System.out.println("FreeVMs: " + vmManger.getFreeVMs().size());
		System.out.println("BusyVMs " + vmManger.getBusyVMs().size());
		System.out.println(vmManger.getFreeVMs().iterator().next().getCores());
		System.out.println(vmManger.getFreeVMs().iterator().next().getMips());
		System.out.println(vmManger.getFreeVMs().iterator().next().getPrice());
		System.out.println(vmManger.getFreeVMs().iterator().next().getManager().getName());
		System.out.println(vmManger.getFreeVMs().iterator().next().getProvisioningDelay());
		System.out.println(vmManger.getFreeVMs().iterator().next().getDeprovisioningDelay());
		System.out.println(vmManger.getFreeVMs().iterator().next().isRunning());
		assertEquals(vms.size(), vmManger.getAvailableVMs().size());
	}

	@Test
	public void testScheduleDag100() {

		VMDescription vmDescription = new VMDescription("testvm", 1000.0, 1.0);
		CloudSim.init(1, null, false);
		Cloud cloud = new Cloud();
		Provisioner provisioner = new SimpleUtilizationBasedProvisioner(0.0);
		DAGDynamicScheduler scheduler = new DAGDynamicScheduler();
		WorkflowEngine engine = new WorkflowEngine(new SimpleJobFactory(1000), provisioner, scheduler,
				48 * 3600.0, Double.MAX_VALUE);
		VMManger vmManger = new VMManger(vmDescription.vmname, vmDescription.price, vmDescription.mips, 1,
				1.0, provisioner, scheduler, engine, cloud);

		ArrayList<VMManger> vmMangers = new ArrayList<VMManger>();
		vmMangers.add(vmManger);
		WorkflowLog jobLog = new WorkflowLog();
		engine.addJobListener(jobLog);
		engine.setVmMangers(vmMangers);
		provisioner.setCloud(cloud);

		HashSet<VMInstance> vms = new HashSet<VMInstance>();

		for (int i = 0; i < 10; i++) {
			VMInstance vmInstance = new VMInstance(vmManger, vmManger.getMips(), vmManger.getCores(),
					vmManger.getBandwidth(), vmManger.getPrice());
			;
			vms.add(vmInstance);
			CloudSim.send(vmManger.getId(), cloud.getId(), 0.1, VM_LAUNCH, vmInstance);
		}

		DAGDescription dag = new DAGDescription("CyberShake_100.dag", "dags", 0.0, 48 * 3600.0, 0);
		ArrayList<DAGDescription> dags = new ArrayList<DAGDescription>();
		dags.add(dag);
		new WorkloadManager(dags, engine);

		CloudSim.startSimulation();

		System.out.println(vmManger.getFreeVMs().size());
		System.out.println(vmManger.getBusyVMs().size());
		// assertEquals(vms.size(), vmManger.getAvailableVMs().size());
		assertEquals(0, engine.getQueuedJobs().size());

		jobLog.printJobs("testDynamicSchedulerDag_CyberShake_100");

	}

}
