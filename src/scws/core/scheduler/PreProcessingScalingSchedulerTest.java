package scws.core.scheduler;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;

import org.cloudbus.cloudsim.core.CloudSim;
import org.junit.Test;

import scws.core.Cloud;
import scws.core.DAGDescription;
import scws.core.Provisioner;
import scws.core.SimpleJobFactory;
import scws.core.VMDescription;
import scws.core.VMInstance;
import scws.core.VMManger;
import scws.core.WorkflowEngine;
import scws.core.WorkflowEvent;
import scws.core.WorkloadManager;
import scws.core.log.WorkflowLog;
import scws.core.provisioner.PreProcessingProvisioner;

public class PreProcessingScalingSchedulerTest implements WorkflowEvent {

	@Test
	public void testScheduleDag100() {
		Double budget = 1000.0;
		VMDescription vmDescription = new VMDescription("testvm", 1000.0, 1.0);
		CloudSim.init(1, null, false);
		Cloud cloud = new Cloud();
		Provisioner provisioner = new PreProcessingProvisioner();
		PreProcessingScalingScheduler scheduler = new PreProcessingScalingScheduler(
				"PreProcessingdisDeadline.log");
		WorkflowEngine engine = new WorkflowEngine(new SimpleJobFactory(1000), provisioner, scheduler,
				48 * 3600.0, budget);
		VMManger vmManger = new VMManger(vmDescription.vmname, vmDescription.price, vmDescription.mips, 1,
				1.0, provisioner, scheduler, engine, cloud);
		vmManger.setInformtimeratio(0.05);
		ArrayList<VMManger> vmMangers = new ArrayList<VMManger>();
		vmMangers.add(vmManger);
		WorkflowLog jobLog = new WorkflowLog();
		engine.addJobListener(jobLog);
		engine.setVmMangers(vmMangers);
		engine.setBudget(budget);
		provisioner.setCloud(cloud);

		HashSet<VMInstance> vms = new HashSet<VMInstance>();

		for (int i = 0; i < 10; i++) {
			VMInstance vmInstance = new VMInstance(vmManger, vmManger.getMips(), vmManger.getCores(),
					vmManger.getBandwidth(), vmManger.getPrice());
			;
			vms.add(vmInstance);
			CloudSim.send(vmManger.getId(), cloud.getId(), 0.1, VM_LAUNCH, vmInstance);
		}

		//DAGDescription dag = new DAGDescription("CyberShake_100.dag", "dags", 0.0, 48 * 3600.0, 0);
		DAGDescription dag = new DAGDescription("BlackDiamonDAX.dag", "dags", 0.0, 48 * 3600.0, 0);
		ArrayList<DAGDescription> dags = new ArrayList<DAGDescription>();
		dags.add(dag);
		new WorkloadManager(dags, engine);

		CloudSim.startSimulation();

		System.out.println(vmManger.getFreeVMs().size());
		System.out.println(vmManger.getBusyVMs().size());
		// assertEquals(vms.size(), vmManger.getAvailableVMs().size());
		assertEquals(0, engine.getQueuedJobs().size());

		jobLog.printJobs("testPreProcessingScalingSchedulerDag_CyberShake_100");
	}

}
