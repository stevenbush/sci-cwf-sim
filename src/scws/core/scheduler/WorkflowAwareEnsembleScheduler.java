package scws.core.scheduler;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Queue;
import java.util.Random;

import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.core.CloudSim;

import scws.core.dag.Task;

import scws.core.dag.DAG;
import scws.core.Job;
import scws.core.VMInstance;
import scws.core.VMManger;

import scws.core.WorkflowEngine;

import scws.core.DAGJob;

/**
 * This scheduler submits workflow ensemble to VMs on FCFS basis. Job is
 * submitted to VM only if VM is idle (no queueing in VMs) ,When we face a new
 * coming DAG job we should assess it, if it is admitted then we try to schedule
 * it
 * 
 * @author jiyuanshi
 * 
 */
public class WorkflowAwareEnsembleScheduler extends EnsembleDynamicScheduler {

	// private HashSet<DAGJob> admittedDAGs = new HashSet<DAGJob>();
	// private HashSet<DAGJob> rejectedDAGs = new HashSet<DAGJob>();

	@Override
	public void scheduleJobs(WorkflowEngine engine) {

		// check the deadline constraints (provisioner takes care about budget)
		double timespan = engine.getTimespan();
		double time = CloudSim.clock();

		// stop scheduling any new jobs if we are over timespan
		if (time >= timespan) {
			engine.getQueuedJobs().clear();
			engine.getDags().clear();
			return;
		}

		Queue<Job> jobs = engine.getQueuedJobs();
		ArrayList<VMManger> vmMangers = engine.getVmMangers();
		Random distribution = new Random();

		// random chose a VM type for execute a job, if the deadline of this
		// job's dagjob is exceeded, we just ignore this job and do not schedule
		// it
		while (!jobs.isEmpty()) {
			Job job = jobs.poll();
			if (time > job.getDAGJob().getDeadline()) {
				engine.getDags().remove(job.getDAGJob());
				job.setResult(Job.Result.FAILURE);
				job.setState(Job.State.TERMINATED);
			} else {
				DAGJob dj = job.getDAGJob();
				if (dj.getAdmitted() == 2) {
					// ignore
					continue;
				} else if (dj.getAdmitted() == 1) {
					// schedule the job
				} else if (admitDAG(dj, engine)) {
					// if the DAG is admitted we add it to the queue
					dj.setAdmitted(1);
				} else {
					dj.setAdmitted(2);
					// skip this job
					continue;
				}

				VMManger vmManger = vmMangers.get(distribution.nextInt(vmMangers.size()));
				job.setVmManger(vmManger);
				// Log.printLine(CloudSim.clock() + " Submitting job " +
				// job.getID() + " to VMManger "
				// + job.getVmManger().getId());
				job.setSubmitTime(CloudSim.clock());
				job.setState(Job.State.IDLE);
				CloudSim.send(engine.getId(), vmManger.getId(), 0.0, JOB_SUBMIT, job);

			}

		}

	}

	/**
	 * decide what to do with the job from a new dag
	 * 
	 * @param dj
	 * @param engine
	 * @return
	 */
	private boolean admitDAG(DAGJob dj, WorkflowEngine engine) {
		double costEstimate = estimateCost(dj, engine);
		double budgetRemaining = estimateBudgetRemaining(engine);
		Log.printLine(CloudSim.clock() + " Cost estimate: " + costEstimate + " Budget remaining: "
				+ budgetRemaining);
		if (!(costEstimate < budgetRemaining)) {
			engine.getDags().remove(dj);
		}
		return costEstimate < budgetRemaining;
	}

	/**
	 * Estimate possible minimum cost of this DAGJob
	 * 
	 * @param dj
	 * @param engine
	 * @return
	 */
	private double estimateCost(DAGJob dj, WorkflowEngine engine) {
		Double sumcost = 0.0;
		DAG dag = dj.getDAG();
		for (String taskName : dag.getTasks()) {
			Task task = dag.getTask(taskName);
			sumcost = sumcost + taskCost(task, engine);
		}

		return sumcost / 3600;

	}

	/**
	 * Estimate budget remaining, including unused $ and running VMs TODO:
	 * compute budget consumed/remaining by already admitted workflows
	 * 
	 * @param dj
	 * @param engine
	 * @return
	 */
	private double estimateBudgetRemaining(WorkflowEngine engine) {
		// remaining budget for starting new vms
		double rn = engine.getBudget() - engine.getCost();
		if (rn < 0)
			rn = 0;

		// compute remaining (not consumed) budget of currently running VMs
		double rc = 0.0;

		HashSet<VMInstance> vms = new HashSet<VMInstance>();
		for (VMManger vmManger : engine.getVmMangers()) {
			vms.addAll(vmManger.getBusyVMs());
			vms.addAll(vmManger.getFreeVMs());
		}

		for (VMInstance vm : vms) {
			rc += vm.getCost() - vm.getRuntime() * vm.getPrice() / 3600.0;
		}

		// compute remaining runtime of admitted workflows
		double ra = 0.0;

		for (DAGJob admittedDJ : engine.getDags()) {
			if (!admittedDJ.isFinished()) {
				ra += computeRemainingCost(admittedDJ, engine);
			}
		}

		// we add this for safety in order not to underestimate our budget
		double safetyMargin = 0.1;

		Log.printLine(CloudSim.clock() + " Budget for new VMs: " + rn + " Budget on running VMs: " + rc
				+ " Remaining budget of admitted workflows: " + ra);

		return rn + rc - ra - safetyMargin;
	}

	/**
	 * Estimate remaining cost = total remaining time of incomplete tasks *
	 * price
	 * 
	 * @param admittedDJ
	 * @param engine
	 * @return
	 */
	private double computeRemainingCost(DAGJob admittedDJ, WorkflowEngine engine) {
		double cost = 0.0;
		DAG dag = admittedDJ.getDAG();

		for (String taskName : dag.getTasks()) {
			Task task = dag.getTask(taskName);
			if (!admittedDJ.isComplete(task))
				cost += taskCost(task, engine);

		}
		return cost / 3600.0;
	}

	/**
	 * @return get the possible minimum cost of execute this task.
	 */
	private double taskCost(Task task, WorkflowEngine engine) {
		double mincost = Double.MAX_VALUE;
		for (VMManger vmManger : engine.getVmMangers()) {
			double jobsize = engine.getJobFactory().getJobSize(task);
			if (vmManger.getPrice() * (jobsize / vmManger.getMips()) < mincost) {
				mincost = vmManger.getPrice() * (jobsize / vmManger.getMips());
			}
		}
		return mincost;
	}

}
