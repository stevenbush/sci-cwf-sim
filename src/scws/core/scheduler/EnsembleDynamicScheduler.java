package scws.core.scheduler;

import java.util.ArrayList;
import java.util.Queue;
import java.util.Random;

import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.core.CloudSim;

import scws.core.Job;
import scws.core.VMManger;

import scws.core.WorkflowEngine;

/**
 * This scheduler submits workflow ensemble to VMs on FCFS basis. Job is
 * submitted to VM only if VM is idle (no queueing in VMs) and if there are no
 * higher priority jobs in the queue.
 * 
 * @author jiyuanshi <shijiyuan.seu@gmail.com>
 * @since 2013-4-21
 * @version sci-cwf-sim 1.0
 */
public class EnsembleDynamicScheduler extends DAGDynamicScheduler {

	/**
	 * Schedule all jobs from the queue to available random chose free VMs.
	 * Successfully scheduled jobs are removed from the queue.
	 */
	@Override
	public void scheduleJobs(WorkflowEngine engine) {

		// check the time constraints (provisioner takes care about budget)
		double timespan = engine.getTimespan();
		double time = CloudSim.clock();

		// stop scheduling any new jobs if we are over timespan and clear the
		// related queue in the workflow engine.
		if (time >= timespan) {
			engine.getQueuedJobs().clear();
			engine.getDags().clear();
			return;
		}

		Queue<Job> jobs = engine.getQueuedJobs();
		ArrayList<VMManger> vmMangers = engine.getVmMangers();
		Random distribution = new Random();

		// random chose a VM type for execute a job, if the deadline of this
		// job's dagjob is exceeded, we just ignore this job and do not schedule
		// it
		while (!jobs.isEmpty()) {
			Job job = jobs.poll();
			if (time > job.getDAGJob().getDeadline()) {
				engine.getDags().remove(job.getDAGJob());
				job.setResult(Job.Result.FAILURE);
				job.setState(Job.State.TERMINATED);
			} else {
				VMManger vmManger = vmMangers.get(distribution.nextInt(vmMangers.size()));
				job.setVmManger(vmManger);
				// Log.printLine(CloudSim.clock() + " Submitting job " +
				// job.getID() + " to VMManger "
				// + job.getVmManger().getId());
				job.setSubmitTime(CloudSim.clock());
				job.setState(Job.State.IDLE);
				CloudSim.send(engine.getId(), vmManger.getId(), 0.0, JOB_SUBMIT, job);
			}

		}
	}

}
