package scws.core.scheduler;

import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Random;
import java.util.Set;

import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.core.CloudSim;

import scws.core.DAGJob;
import scws.core.Job;
import scws.core.Scheduler;
import scws.core.VMInstance;
import scws.core.VMManger;
import scws.core.WorkflowEngine;
import scws.core.WorkflowEvent;

/**
 * This scheduler submits jobs to the correspond type of VM Manger on FCFS
 * basis. Job is submitted to VM only if VM is idle (no queueing in VMs).
 * 
 * @author jiyuanshi
 * @since 2013-4-17
 * @version sci-cwf-sim 1.0
 * 
 */
public class DAGDynamicScheduler implements Scheduler, WorkflowEvent {

	@Override
	public void preprocessDagJob(DAGJob dagJob) {
		// Do nothing
	}

	@Override
	public void jobFailerProcess(Job job) {
		// Do nothing
	}

	/**
	 * Schedule all jobs from the queue to available free VMs. Successfully
	 * scheduled jobs are removed from the queue.
	 */
	@Override
	public void scheduleJobs(WorkflowEngine engine) {
		// use the queued (released) jobs from the workflow engine
		Queue<Job> jobs = engine.getQueuedJobs();

		ArrayList<VMManger> vmMangers = engine.getVmMangers();
		Random distribution = new Random();

		// random chose a VM type for execute a job
		while (!jobs.isEmpty()) {
			Job job = jobs.poll();
			VMManger vmManger = vmMangers.get(distribution.nextInt(vmMangers.size()));
			job.setVmManger(vmManger);
			// Log.printLine(CloudSim.clock() + " Submitting job " + job.getID()
			// + " to VMManger " + job.getVmManger().getName()
			// + job.getVmManger().getId());
			job.setSubmitTime(CloudSim.clock());
			job.setState(Job.State.IDLE);
			CloudSim.send(engine.getId(), vmManger.getId(), 0.0, JOB_SUBMIT, job);
		}
	}

	@Override
	public void scheduleJobToVMInstance(VMManger vmManger) {
		PriorityQueue<Job> jobs = vmManger.getJobPriorityQueue();
		Set<VMInstance> freeVMs = vmManger.getFreeVMs();
		Set<VMInstance> busyVMs = vmManger.getBusyVMs();

		// check the time constraints (provisioner takes care about budget)
		double timespan = vmManger.getWorkflowengine().getTimespan();
		double time = CloudSim.clock();

		// stop scheduling any new jobs if we are over timespan and clear the
		// related queue in the workflow engine.
		if (time >= timespan) {
			jobs.clear();
			return;
		}
		
		Job[] tmpjobs = new Job[jobs.size()];
		jobs.toArray(tmpjobs);
		
		for (Job job : tmpjobs) {
			if (time > job.getDAGJob().getDeadline()) {
				vmManger.getWorkflowengine().getDags().remove(job.getDAGJob());
				job.setResult(Job.Result.FAILURE);
				job.setState(Job.State.TERMINATED);
				jobs.remove(job);
			}
		}
		
		while (!freeVMs.isEmpty() && !jobs.isEmpty()) {
			Job job = jobs.poll();
			if (!job.getDAGJob().getStarted()) {
				job.getDAGJob().setStarted(true);
			}
			VMInstance vm = freeVMs.iterator().next();
			job.setVminstance(vm);
			freeVMs.remove(vm); // remove VM from free set
			busyVMs.add(vm); // add vm to busy set
			// Log.printLine(CloudSim.clock() + " Submitting job "
			// + job.getID() + "from VMManger: " + vmManger.getName()
			// + " to VM Instance " + job.getVminstance().getVmID());
			CloudSim.send(vmManger.getId(), vm.getOwner(), 0.0, JOB_SUBMIT_TO_INSTANCE, job);

		}

	}

	@Override
	public void setWorkflowEngine(WorkflowEngine engine) {
		// TODO Auto-generated method stub

	}

}
