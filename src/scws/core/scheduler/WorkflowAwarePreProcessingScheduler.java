package scws.core.scheduler;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Queue;

import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.core.predicates.Predicate;

import scws.core.DAGJob;
import scws.core.Job;
import scws.core.PredicateCancleDag;
import scws.core.VMInstance;
import scws.core.VMManger;
import scws.core.WorkflowEngine;
import scws.core.dag.DAG;
import scws.core.dag.Task;
import scws.core.dag.algorithms.DeadlineDistribution;

/**
 * This class implement my PSS algorithm that first assess this incoming DAG job
 * to decide if accept it for scheduling second divide the overall deadline into
 * sub-deadline and assign them to every task, then according every task's
 * deadline to schedule it to the suitable VM type that can complete it before
 * the deadline with cheapest cost
 * 
 * @author jiyuanshi
 * @since 2013-05-10
 * @version sci-cwf-sim 1.0
 * 
 */
public class WorkflowAwarePreProcessingScheduler extends DAGDynamicScheduler {

	private String logFilePath;

	public WorkflowAwarePreProcessingScheduler(String logFilePath) {
		this.logFilePath = logFilePath;
	}

	// private HashSet<DAGJob> admittedDAGs = new HashSet<DAGJob>();
	// private HashSet<DAGJob> rejectedDAGs = new HashSet<DAGJob>();

	/**
	 * doing the preprocessing for the submitted dag job: divide the overall
	 * deadline into sub-sdealine and distribute them to every tasks
	 */
	@Override
	public void preprocessDagJob(DAGJob dagJob) {
		long begin = System.nanoTime();

		DeadlineDistribution.DistributeDeadline(dagJob.getDAG(), dagJob.getSubtime(), dagJob.getDeadline());

		long duration = System.nanoTime() - begin;

		try {
			String filenpath = logFilePath + "-disDeadlineTime";
			BufferedWriter filewriter = new BufferedWriter(new FileWriter(filenpath, true));
			filewriter.write(dagJob.getDAG().getTasks().length + "~" + duration);
			filewriter.newLine();
			filewriter.flush();
			filewriter.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * First assess this incoming DAG Job to decide if accept it for scheduling
	 * then according to the sub-deadline of every jobs to schedule them to
	 * suitable VM type, the selection principle is to chose the cheapest VM
	 * type that can complete this job before sub-deadline, if a job is already
	 * exceeded it's deadline, we will schedule it to the fastest VM type. if
	 * the current time exceeded the total timespan or the dag's total deadline
	 * we will end this scheduling.
	 */
	@Override
	public void scheduleJobs(WorkflowEngine engine) {

		// check the deadline constraints (provisioner takes care about budget)
		double timespan = engine.getTimespan();
		double time = CloudSim.clock();

		// stop scheduling any new jobs if we are over timespan
		if (time >= timespan) {
			engine.getQueuedJobs().clear();
			engine.getDags().clear();
			return;
		}

		// get the queued (released) jobs from the workflow engine
		Queue<Job> jobs = engine.getQueuedJobs();
		// get all the VM Mangers
		ArrayList<VMManger> vmMangers = engine.getVmMangers();

		// the comparator for comparing two VMMangers
		Comparator<VMManger> comparator = new Comparator<VMManger>() {
			public int compare(VMManger v1, VMManger v2) {
				if (v1.getMips() < v2.getMips()) {
					return 1;
				} else if (v1.getMips() > v2.getMips()) {
					return -1;
				} else {
					return 0;
				}
			}
		};

		// sort all the vmmanger according the mips from large to small
		Collections.sort(vmMangers, comparator);
		VMManger mostPowerfullVMManger = vmMangers.get(0);

		// according the sub-deadline to scheudle the job to the suitable VM
		// type, if the deadline of this job's dagjob is exceeded, we just
		// ignore this job and do not schedule
		while (!jobs.isEmpty()) {
			Job job = jobs.poll();
			// if the current time exceed the dag's overall deadline we just
			// ignore this Dag job.
			if (time > job.getDAGJob().getDeadline()) {
				engine.getDags().remove(job.getDAGJob());
				job.setResult(Job.Result.FAILURE);
				job.setState(Job.State.TERMINATED);
			} else {
				DAGJob dj = job.getDAGJob();
				if (dj.getAdmitted() == 2) {
					// ignore
					continue;
				} else if (dj.getAdmitted() == 1) {
					// schedule the job
				} else if (admitDAG(dj, engine)) {
					// if the DAG is admitted we add it to the queue
					dj.setAdmitted(1);
				} else {
					dj.setAdmitted(2);
					// skip this job
					continue;
				}

				if (time > job.getSubdeadline()) {
					job.setVmManger(mostPowerfullVMManger);
					// Log.printLine(CloudSim.clock() +
					// " Submitting exceed sud-deadline job " + job.getID()
					// + " to most powerfull VMManger " +
					// job.getVmManger().getName()
					// + job.getVmManger().getId());
					job.setSubmitTime(CloudSim.clock());
					job.setState(Job.State.IDLE);
					CloudSim.send(engine.getId(), mostPowerfullVMManger.getId(), 0.0, JOB_SUBMIT, job);
				} else {
					Double minmunCost = Double.MAX_VALUE;
					VMManger minCostVMManger = mostPowerfullVMManger;
					// find the cheapest VM type that can complete the job
					// before the job's sub-deadline
					for (VMManger vmManger : vmMangers) {
						Double timecost = job.getSize() / vmManger.getMips();
						Double vmcost = Math.ceil(timecost / 3600) * vmManger.getPrice();
						if (vmcost < minmunCost && time + timecost < job.getSubdeadline()) {
							minmunCost = vmcost;
							minCostVMManger = vmManger;
						}
					}
					job.setVmManger(minCostVMManger);
					// Log.printLine(CloudSim.clock() + " Submitting job " +
					// job.getID() + " to VMManger "
					// + job.getVmManger().getName() +
					// job.getVmManger().getId());
					job.setSubmitTime(CloudSim.clock());
					job.setState(Job.State.IDLE);
					CloudSim.send(engine.getId(), minCostVMManger.getId(), 0.0, JOB_SUBMIT, job);
				}
			}
		}

	}

	/**
	 * decide what to do with the job from a new dag
	 * 
	 * @param dj
	 * @param engine
	 * @return
	 */
	private boolean admitDAG(DAGJob dj, WorkflowEngine engine) {
		Boolean returnValue = true;

		long begin = System.nanoTime();

		double costEstimate = estimateCost(dj, engine);
		double budgetRemaining = estimateBudgetRemaining(engine);
		Log.printLine(CloudSim.clock() + " Cost estimate: " + costEstimate + " Budget remaining: "
				+ budgetRemaining);

		if (budgetRemaining < 0) {
			returnValue = false;
		} else {
			if (costEstimate < budgetRemaining) {
				returnValue = true;
			} else {
				returnValue = DAGJobReplacement((int) Math.ceil(costEstimate - budgetRemaining), dj, engine);
			}

			long duration = System.nanoTime() - begin;

			try {
				String filenpath = logFilePath + "-jobAssessmentTime";
				BufferedWriter filewriter = new BufferedWriter(new FileWriter(filenpath, true));
				filewriter.write(engine.getDags().size() + "~" + duration);
				filewriter.newLine();
				filewriter.flush();
				filewriter.close();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (!returnValue) {
			engine.getDags().remove(dj);
		}

		return returnValue;
	}

	/**
	 * Using the Dynamic programming method try to find a sub-set of already
	 * accepted DAG jobs that can be replaced by the current received DAG job,
	 * this replacement must meet the requirement that the sum of all the
	 * replaced jobs' priority is less then the original job's priority and the
	 * remaining budget can not be exceeded
	 * 
	 * @param budgetGap
	 * @param dj
	 * @param engine
	 * @return is the replacement successful?
	 */
	private Boolean DAGJobReplacement(Integer budgetGap, DAGJob dj, WorkflowEngine engine) {
		ArrayList<DAGJob> unStartedDagJobs = new ArrayList<DAGJob>();
		for (DAGJob dagJob : engine.getDags()) {
			if (!dagJob.getStarted()) {
				unStartedDagJobs.add(dagJob);
			}
		}

		if (!unStartedDagJobs.isEmpty()) {
			// Log.printLine("unStartedDagJobs.size():" +
			// unStartedDagJobs.size() + "~budgetGap:" + budgetGap);
			Double[][] dpMatrix = new Double[unStartedDagJobs.size() + 1][budgetGap + 1];

			for (int i = 0; i <= budgetGap; i++) {
				dpMatrix[0][i] = Double.MAX_VALUE;
			}
			for (int i = 0; i <= unStartedDagJobs.size(); i++) {
				dpMatrix[i][0] = 0.0;
			}

			for (int i = 1; i <= unStartedDagJobs.size(); i++) {
				for (int j = 1; j <= budgetGap; j++) {
					Double sumCost = 0.0;
					for (int k = 1; k <= i; k++) {
						sumCost = sumCost + estimateCost(unStartedDagJobs.get(i - 1), engine);
					}

					if (sumCost < j) {
						dpMatrix[i][j] = Double.MAX_VALUE;
					} else {
						Integer tmpj = j;
						Integer formerCost = (int) Math
								.ceil(estimateCost(unStartedDagJobs.get(i - 1), engine));
						if (j < formerCost) {
							tmpj = formerCost;
						}

						Double tmpValue = getExponentialScore((int) unStartedDagJobs.get(i - 1).getPriority())
								+ dpMatrix[i - 1][(tmpj - formerCost)];
						dpMatrix[i][j] = Math.min(tmpValue, dpMatrix[i - 1][j]);
					}
				}
			}

			if (dpMatrix[unStartedDagJobs.size()][budgetGap] < getExponentialScore((int) dj.getPriority())) {
				Integer currentGap = budgetGap;
				for (int i = unStartedDagJobs.size(); i > 0; i--) {
					if (currentGap < 0) {
						break;
					}
					if (dpMatrix[i][currentGap] < dpMatrix[i - 1][currentGap]) {
						// Cancel the dag's job in relate entity
						cancleJobs(unStartedDagJobs.get(i - 1), engine);
						currentGap = currentGap
								- (int) Math.ceil(estimateCost(unStartedDagJobs.get(i - 1), engine));
					}
				}
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 * cancle the jobs belong to the DAG job in related entity
	 * 
	 * @param dagJob
	 */
	public void cancleJobs(DAGJob dagJob, WorkflowEngine workflowEngine) {

		// Predicate used for cancle events
		Predicate p = new PredicateCancleDag(dagJob);

		// cancle job in every VMManger's queue
		for (VMManger vmManger : workflowEngine.getVmMangers()) {

			Job[] tmpjobs = new Job[vmManger.getJobPriorityQueue().size()];
			vmManger.getJobPriorityQueue().toArray(tmpjobs);

			for (Job job : tmpjobs) {
				if (job.getDAGJob() == dagJob) {
					vmManger.getJobPriorityQueue().remove(job);
				}
			}

			// Cancle future events send from all the VMMangers
			CloudSim.cancelAll(vmManger.getId(), p);
		}

		// cancle job in the engine's queue
		Job[] tmpjobs = new Job[workflowEngine.getQueuedJobs().size()];
		workflowEngine.getQueuedJobs().toArray(tmpjobs);
		for (Job job : tmpjobs) {
			if (job.getDAGJob() == dagJob) {
				workflowEngine.getQueuedJobs().remove(job);
			}
		}

		// cancel future events send from WorkflowEngine
		CloudSim.cancelAll(workflowEngine.getId(), p);

		// cancle the DAG job
		workflowEngine.getDags().remove(dagJob);
	}

	/**
	 * Estimate possible minimum cost of this DAGJob
	 * 
	 * @param dj
	 * @param engine
	 * @return
	 */
	public double estimateCost(DAGJob dj, WorkflowEngine engine) {
		Double sumcost = 0.0;
		DAG dag = dj.getDAG();
		for (String taskName : dag.getTasks()) {
			Task task = dag.getTask(taskName);
			sumcost = sumcost + taskCost(task, engine);
		}

		return sumcost / 3600;
	}

	/**
	 * Estimate budget remaining, including unused $ and running VMs TODO:
	 * compute budget consumed/remaining by already admitted workflows
	 * 
	 * @param dj
	 * @param engine
	 * @return
	 */
	private double estimateBudgetRemaining(WorkflowEngine engine) {
		// remaining budget for starting new vms
		double rn = engine.getBudget() - engine.getCost();
		if (rn < 0)
			rn = 0;

		// compute remaining (not consumed) budget of currently running VMs
		double rc = 0.0;

		HashSet<VMInstance> vms = new HashSet<VMInstance>();
		for (VMManger vmManger : engine.getVmMangers()) {
			vms.addAll(vmManger.getBusyVMs());
			vms.addAll(vmManger.getFreeVMs());
		}

		for (VMInstance vm : vms) {
			rc += vm.getCost() - vm.getRuntime() * vm.getPrice() / 3600.0;
		}

		// compute remaining runtime of admitted workflows
		double ra = 0.0;

		for (DAGJob admittedDJ : engine.getDags()) {
			if (!admittedDJ.isFinished()) {
				ra += computeRemainingCost(admittedDJ, engine);
			}
		}

		// we add this for safety in order not to underestimate our budget
		double safetyMargin = 0.1;

		Log.printLine(CloudSim.clock() + " Budget for new VMs: " + rn + " Budget on running VMs: " + rc
				+ " Remaining budget of admitted workflows: " + ra);

		return rn + rc - ra - safetyMargin;

	}

	/**
	 * Estimate remaining cost = total remaining time of incomplete tasks *
	 * price
	 * 
	 * @param admittedDJ
	 * @param engine
	 * @return
	 */
	private double computeRemainingCost(DAGJob admittedDJ, WorkflowEngine engine) {
		double cost = 0.0;
		DAG dag = admittedDJ.getDAG();

		for (String taskName : dag.getTasks()) {
			Task task = dag.getTask(taskName);
			if (!admittedDJ.isComplete(task))
				cost += taskCost(task, engine);

		}
		return cost / 3600.0;
	}

	/**
	 * @return get the possible minimum cost of execute this task.
	 */
	private double taskCost(Task task, WorkflowEngine engine) {
		double earliestStartTime = Double.MIN_VALUE;
		for (Task parentTask : task.parents) {
			earliestStartTime = Math.max(earliestStartTime, parentTask.subdealine);
		}
		double task_timespan = task.subdealine - earliestStartTime;
		double mincost = Double.MAX_VALUE;
		for (VMManger vmManger : engine.getVmMangers()) {
			double jobsize = engine.getJobFactory().getJobSize(task);
			if (vmManger.getPrice() * (jobsize / vmManger.getMips()) < mincost
					&& (jobsize / vmManger.getMips()) <= task_timespan) {
				mincost = vmManger.getPrice() * (jobsize / vmManger.getMips());
			}
		}
		return mincost;
	}

	/**
	 * calculate the ExponentialScore of this priority
	 * 
	 * @param priority
	 * @return
	 */
	public static double getExponentialScore(Integer priority) {
		BigDecimal one = BigDecimal.ONE;
		BigDecimal two = new BigDecimal(2.0);

		BigDecimal divisor = two.pow(priority);
		BigDecimal score = one.divide(divisor);

		return score.doubleValue();
	}
}
