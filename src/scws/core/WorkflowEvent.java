package scws.core;

/**
 * a class that contains all the event type will used in this simulation, based
 * on the version writed by Gideon Juve <juve@usc.edu>
 * 
 * @author jiyuanshi
 * @since 2013-4-15
 * @version sci-cwf-sim 1.0
 */
public interface WorkflowEvent {
	// ///////////////////////////////////////////////////////
	// TRANSFER EVENTS
	// ///////////////////////////////////////////////////////

	/** Client entity submits a new transfer */
	public static final int NEW_TRANSFER = 0;

	/** Update the progress of existing transfers */
	public static final int UPDATE_TRANSFER_PROGRESS = 1;

	/** Transfer's initial handshake is complete */
	public static final int HANDSHAKE_COMPLETE = 2;

	/** Transfer's final ACK received */
	public static final int FINAL_ACK_RECEIVED = 3;

	/** Transfer is complete */
	public static final int TRANSFER_COMPLETE = 4;

	// ///////////////////////////////////////////////////////
	// VM EVENTS
	// ///////////////////////////////////////////////////////

	/** Client entity submits a new VM creation request */
	public static final int NEW_VM = 5;

	/** VM creation is complete */
	public static final int VM_CREATION_COMPLETE = 6;

	/** Client entity submits a new cloudlet to a VM */
	public static final int CLOUDLET_SUBMIT = 7;

	/** Cloudlet execution is started */
	public static final int CLOUDLET_STARTED = 8;

	/** Cloudlet execution is comlplete */
	public static final int CLOUDLET_COMPLETE = 9;

	/** Client entity submits a VM termination request */
	public static final int TERMINATE_VM = 10;

	/** VM termination is complete */
	public static final int VM_TERMINATION_COMPLETE = 11;

	/** Start a new VM */
	public static final int VM_LAUNCH = 12;

	/** VM started up */
	public static final int VM_LAUNCHED = 13;

	/** Terminate an existing VM */
	public static final int VM_TERMINATE = 14;

	/** VM was terminated */
	public static final int VM_TERMINATED = 15;

	/** VM was approaching the billing cycle */
	public static final int VM_APPROACHING = 16;

	/** Set the approaching billing cycle check event */
	public static final int VM_CHECK_APPROACHING = 17;

	// ///////////////////////////////////////////////////////
	// JOB EVENTS
	// ///////////////////////////////////////////////////////

	/** Submit a new task */
	public static final int JOB_SUBMIT = 18;

	/** Submit a new task to VM instance */
	public static final int JOB_SUBMIT_TO_INSTANCE = 19;

	/** Job begins execution on remote resource */
	public static final int JOB_STARTED = 20;

	/** Job finished execution on remote host */
	public static final int JOB_FINISHED = 21;

	/** Job approaching the latest start time */
	public static final int JOB_APPROACH_LST = 22;

	// ///////////////////////////////////////////////////////
	// DAG EVENTS
	// ///////////////////////////////////////////////////////

	public static final int DAG_SUBMIT = 23;

	public static final int DAG_STARTED = 24;

	public static final int DAG_FINISHED = 25;
	
	public static final int DAG_EXPIRED = 26;

	// ///////////////////////////////////////////////////////
	// PROVISIONING EVENTS
	// ///////////////////////////////////////////////////////

	/** Submit next provisioning request */
	public static final int PROVISIONING_REQUEST = 27;

	/** reach the timespan terminate this simulation */
	public static final int TERMINATE_SIMULATION = 28;
}
