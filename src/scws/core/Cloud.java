package scws.core;

import java.util.HashSet;

import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.core.SimEntity;
import org.cloudbus.cloudsim.core.SimEvent;

public class Cloud extends SimEntity implements WorkflowEvent {

	/** The set of currently active VMs */
	private HashSet<VMInstance> vms = new HashSet<VMInstance>();

	private HashSet<VMInstanceListener> vmListeners = new HashSet<VMInstanceListener>();

	public Cloud() {
		super("Cloud");
		CloudSim.addEntity(this);
	}

	public void addVMListener(VMInstanceListener l) {
		vmListeners.add(l);
	}

	public void removeVMListener(VMInstanceListener l) {
		vmListeners.remove(l);
	}

	@Override
	public void startEntity() {
		// DO NOTHING
	}

	@Override
	public void processEvent(SimEvent ev) {
		switch (ev.getTag()) {
		case TERMINATE_SIMULATION:
			vmTerminated((VMInstance) ev.getData());
			break;
		case VM_LAUNCH:
			launchVM(ev.getSource(), (VMInstance) ev.getData());
			break;
		case VM_TERMINATE:
			terminateVM((VMInstance) ev.getData());
			break;
		case VM_LAUNCHED:
			vmLaunched((VMInstance) ev.getData());
			break;
		case VM_TERMINATED:
			vmTerminated((VMInstance) ev.getData());
			break;
		default:
			throw new RuntimeException("Unknown event: " + ev);
		}
	}

	@Override
	public void shutdownEntity() {
		// DO NOTHING
	}

	private void launchVM(int owner, VMInstance vm) {
		vm.setOwner(owner);
		vm.setCloud(getId());
		vm.setLaunchTime(CloudSim.clock());
		vms.add(vm);

		// We launch the VM now...
		sendNow(vm.getOwner(), VM_LAUNCH, vm);

		// But it isn't ready until after the delay
		send(getId(), vm.getProvisioningDelay(), VM_LAUNCHED, vm);
	}

	private void vmLaunched(VMInstance vm) {
		// Sanity check
		if (vms.contains(vm)) {
			// Listeners are informed
			for (VMInstanceListener l : vmListeners) {
				l.vminstanceLaunched(vm);
			}

			// The owner learns about the launch
			sendNow(vm.getOwner(), VM_LAUNCHED, vm);
		}
	}

	private void terminateVM(VMInstance vm) {
		// Sanity check
		if (vms.contains(vm)) {
			// We terminate the VM now...
			sendNow(vm.getOwner(), VM_TERMINATE, vm);
			Log.printLine(CloudSim.clock() + "  We Terminate the VM: " + vm.getVmID());
			// sendNow(vm.getId(), VM_TERMINATE);

			// But it isn't gone until after the delay
			send(getId(), vm.getDeprovisioningDelay(), VM_TERMINATED, vm);
		}
	}

	private void vmTerminated(VMInstance vm) {
		// Sanity check
		if (vms.contains(vm)) {
			vm.setTerminateTime(CloudSim.clock());
			vms.remove(vm);

			// Listeners find out
			for (VMInstanceListener l : vmListeners) {
				l.vminstanceTerminated(vm);
			}

			// The owner finds out
			sendNow(vm.getOwner(), VM_TERMINATED, vm);
			Log.printLine(CloudSim.clock() + "VM: " + vm.getVmID() + " Terminated...");
		}

	}

	public HashSet<VMInstance> getVms() {
		return vms;
	}

	public void setVms(HashSet<VMInstance> vms) {
		this.vms = vms;
	}

}
