package scws.core.experiment;

import org.cloudbus.cloudsim.distributions.ContinuousDistribution;

import scws.core.FailureModel;
import scws.core.IdentityRuntimeDistribution;
import scws.core.RuntimeDistribution;
import scws.core.VMInstance;
import scws.core.VMManger;

/**
 * This is a tool class that used to generate specify type of VM instance
 * 
 * @author jiyuanshi <shijiyuan.seu@gmail.com>
 * @since 2013-4-20
 * @version sci-cwf-sim 1.0
 * 
 */
public class VMFactory {

	public static class ZeroDistribution implements ContinuousDistribution {
		@Override
		public double sample() {
			return 0.0d;
		}
	}

	private static double provisioningDelay = 0.0;
	private static double deprovisioningDelay = 0.0;
	private static ContinuousDistribution provisioningDelayDistribution = new ZeroDistribution();
	private static ContinuousDistribution deprovisioningDelayDistribution = new ZeroDistribution();
	private static RuntimeDistribution runtimeDistribution = new IdentityRuntimeDistribution();
	private static FailureModel failureModel = new FailureModel(0, 0.0);

	public static ContinuousDistribution getProvisioningDelayDistribution() {
		return VMFactory.provisioningDelayDistribution;
	}

	public static void setProvisioningDelayDistribution(ContinuousDistribution distribution) {
		VMFactory.provisioningDelayDistribution = distribution;
	}

	public static ContinuousDistribution getDeprovisioningDelayDistribution() {
		return VMFactory.deprovisioningDelayDistribution;
	}

	public static void setDeprovisioningDelayDistribution(
			ContinuousDistribution deprovisioningDelayDistribution) {
		VMFactory.deprovisioningDelayDistribution = deprovisioningDelayDistribution;
	}

	public static void setRuntimeDistribution(RuntimeDistribution runtimeDistribution) {
		VMFactory.runtimeDistribution = runtimeDistribution;
	}

	public static RuntimeDistribution getRuntimeDistribution() {
		return runtimeDistribution;
	}

	public static FailureModel getFailureModel() {
		return failureModel;
	}

	public static void setFailureModel(FailureModel failureModel) {
		VMFactory.failureModel = failureModel;
	}

	public static VMInstance createVM(VMManger manage, Double mips, int cores, double bandwidth, double price) {
		VMInstance vm = new VMInstance(manage, mips, cores, bandwidth, price);
		vm.setProvisioningDelay(provisioningDelayDistribution.sample());
		vm.setDeprovisioningDelay(deprovisioningDelayDistribution.sample());
		vm.setRuntimeDistribution(runtimeDistribution);
		vm.setFailureModel(failureModel);
		return vm;
	}

	public static double getProvisioningDelay() {
		return provisioningDelay;
	}

	public static void setProvisioningDelay(double provisioningDelay) {
		VMFactory.provisioningDelay = provisioningDelay;
	}

	public static double getDeprovisioningDelay() {
		return deprovisioningDelay;
	}

	public static void setDeprovisioningDelay(double deprovisioningDelay) {
		VMFactory.deprovisioningDelay = deprovisioningDelay;
	}

}
