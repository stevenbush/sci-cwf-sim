package scws.core.experiment;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import scws.core.experiment.Experiment;
import scws.core.log.WorkflowLog;

import scws.core.algorithms.Algorithm;
import scws.core.experiment.AlgorithmFactory;
import scws.core.experiment.ExperimentDescription;
import scws.core.experiment.ExperimentResult;
import scws.core.dag.DAG;

public class Experiment {

	/**
	 * Runs experiment given its description
	 * 
	 * @param param
	 *            experiment description
	 * @return results
	 */

	public ExperimentResult runExperiment(ExperimentDescription param) {
		long startTime = System.nanoTime();

		ExperimentResult result = new ExperimentResult();
		Algorithm algorithm = AlgorithmFactory.createAlgorithm(param);

		//decide if write the log
		algorithm.setGenerateLog(false);

		String fileName = param.getRunDirectory() + File.separator + "output-" + param.getFileName();

		long simulationStartTime = System.nanoTime();
		algorithm.simulate(fileName);

		result.setInitWallTime(simulationStartTime - startTime);
		result.setPlanningWallTime(algorithm.getPlanningnWallTime());
		result.setSimulationWallTime(algorithm.getSimulationWallTime());

		result.setBudget(param.getBudget());
		result.setTimespan(param.getTimespan());
		result.setCost(algorithm.getActualCost());
		result.setAlgorithm(param.getAlgorithmName());
		result.setNumTotalDAGs(param.getDags().size());

		List<Integer> priorities = new LinkedList<Integer>();
		List<Double> sizes = new LinkedList<Double>();

		int finished = algorithm.numCompletedDAGs();
		// for (DAG dag : algorithm.getCompletedDAGs()) {
		// sizes.add(sumRuntime(dag));
		// }

		priorities = algorithm.completedDAGPriorities();
		result.setNumFinishedDAGs(finished);
		result.setPriorities(priorities);
		//result.setSizes(sizes);

		result.setActualFinishTime(algorithm.getActualDagFinishTime());
		result.setActualJobFinishTime(algorithm.getActualJobFinishTime());
		//result.setScoreBitString(algorithm.getScoreBitString());
		result.setExponentialScore(algorithm.getExponentialScore());
		result.setLinearScore(algorithm.getLinearScore());

		return result;
	}

	/**
	 * @return The total runtime of all tasks in the workflow
	 */
	public double sumRuntime(DAG dag) {
		double sum = 0.0;
		for (String taskName : dag.getTasks()) {
			sum += dag.getTask(taskName).size;
		}
		return sum;
	}

	/**
	 * Runs a single experiment given its description in property file. Result
	 * is saved in corresponding result file in the same directory.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Experiment experiment = new Experiment();
		ExperimentDescription param = new ExperimentDescription(args[0]);
		ExperimentResult result = experiment.runExperiment(param);
		//System.out.println(result.formatResult());

		String fileName = "result-" + param.getFileName() + "-result.txt";

		result.WriteResult(param.getRunDirectory() + File.separator + fileName);
		//WorkflowLog.stringToFile(result.formatResult(), param.getRunDirectory() + File.separator + fileName);

	}

}
