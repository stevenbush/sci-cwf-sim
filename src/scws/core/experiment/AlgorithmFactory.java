package scws.core.experiment;

import java.io.File;
import java.util.ArrayList;

import org.apache.commons.math3.distribution.UniformRealDistribution;
import org.cloudbus.cloudsim.distributions.ContinuousDistribution;

import scws.core.DAGDescription;
import scws.core.FailureModel;
import scws.core.UniformRuntimeDistribution;
import scws.core.VMDescription;
import scws.core.experiment.VMFactory;

import scws.core.algorithms.Algorithm;
import scws.core.algorithms.DPDS;
import scws.core.algorithms.PPDS;
import scws.core.algorithms.SFPDS;
import scws.core.algorithms.WADPDS;
import scws.core.algorithms.WPPDS;
import scws.core.experiment.ExperimentDescription;

public class AlgorithmFactory {

	public static Algorithm createAlgorithm(ExperimentDescription e) {
		String name = e.getAlgorithmName();
		ArrayList<DAGDescription> dags = e.getDags();
		ArrayList<VMDescription> vms = new ArrayList<VMDescription>();
		for (VMDescription vmDescription : e.getVmDescription().values()) {
			vms.add(vmDescription);
		}
		int runId = e.getRunID();

		// Set the Runtime Variation
		double runtimeVariation = e.getRuntimeVariation();
		if (runtimeVariation > 0.0) {
			VMFactory.setRuntimeDistribution(new UniformRuntimeDistribution(runtimeVariation));
		}

		// Set Provisioning Delay Distributuion
		double delayVariation = e.getDelayVariation();
		double delay = e.getDelay();
		if (delay > 0.0) {
			VMFactory.setDeprovisioningDelay(delay);
			VMFactory.setProvisioningDelay(delay);
			VMFactory.setProvisioningDelayDistribution(new DelayDistribution(delay, delayVariation));
			VMFactory.setDeprovisioningDelayDistribution(new DelayDistribution(delay, 0));
		}

		// Set the FailureModel
		double failureRate = e.getFailureRate();
		if (failureRate > 0.0) {
			VMFactory.setFailureModel(new FailureModel(0, failureRate));
		}

		double budget = e.getBudget();
		double timespan = e.getTimespan();
		double maxScaling = e.getMaxScaling();
		double taskDilatation = e.getTaskDilatation();
		double informtimeratio = e.getInformtimeratio();

		if (name.equals("DPDS")) {
			return new DPDS(budget, timespan, taskDilatation, informtimeratio, dags, vms, maxScaling);
		} else if (name.equals("WADPDS")) {
			return new WADPDS(budget, timespan, taskDilatation, informtimeratio, dags, vms, maxScaling);
		} else if (name.equals("SFPDS")) {
			return new SFPDS(budget, timespan, taskDilatation, informtimeratio, dags, vms);
		} else if (name.equals("PPDS")) {
			return new PPDS(budget, timespan, taskDilatation, informtimeratio, dags, vms, e.getRunDirectory()
					+ File.separator + e.getFileName());
		} else if (name.equals("WPPDS")) {
			return new WPPDS(budget, timespan, taskDilatation, informtimeratio, dags, vms,
					e.getRunDirectory() + File.separator + e.getFileName());
		} else {
			return null;
		}

	}

	/**
	 * 
	 * FIXME create a separate class
	 * 
	 */
	static class DelayDistribution implements ContinuousDistribution {
		private double delay;
		private double delayVariation;

		public DelayDistribution(double delay, double delayVariation) {
			this.delay = delay;
			this.delayVariation = delayVariation;
		}

		@Override
		public double sample() {
			UniformRealDistribution distribution = new UniformRealDistribution(-1, 1);
			double plusorminus = distribution.sample();
			return delay + (plusorminus * delayVariation * delay);
		}
	}
}
