package scws.core.experiment;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.math3.distribution.UniformIntegerDistribution;

import scws.core.DAGDescription;
import scws.core.VMDescription;

public class PropertyGenerator {

	static String runDirectory = "output";
	static String generateDagPath = "dags/";
	static String dagPath = "dags/";
	static String dagName;
	static String workload;
	static String distribution;
	static double budget;
	static double price = 1.0;
	static int numDAGs = 40;
	static double max_scaling = 2.0;
	static String group = "";
	static double alpha = 0.7;
	static double taskDilatation = 1000.0;
	static double runtimeVariation = 0.0;
	static double delayVariation = 0.2;
	static double delay = 10;
	static double timespan = 48 * 3600.0;
	static HashMap<String, Double[]> vmDescription;
	static double deadlineVariation = 0.5;
	static double failureRate = 0.01;
	static double informtimeratio = 0.05;
	static int averagejobnum = 100;
	static int job_deviation = 20;
	static int increments = 10;
	static int perjob_upper = 180;
	static int perjob_lower = 20;
	static int min_jobnum = 10;

	/******************************
	 * 
	 * Tests with max scaling = 0.0
	 * 
	 ******************************/

	/**
	 * bursting workload, random priority between 1~6, random submit time,
	 * random dag size random dag size with multiple budget
	 */

	public static void testTotalRandomMultiBudget(String runLocation, String dagLocation, String nameofdag) {

		runDirectory = runLocation;
		generateDagPath = dagLocation;
		// generateDagPath =
		// "/Users/jiyuanshi/Documents/workspace/sci-cwf-sim/ams_workflow/dag/";
		// dagPath = "/gpfssan3/jishi/experiment/dags/";
		dagPath = dagLocation;
		group = "MultiType";
		// dagName = "CYBERSHAKE";
		// dagName = "GENOME";
		// dagName = "LIGO";
		// dagName = "MONTAGE";
		// dagName = "SIPHT";
		dagName = nameofdag;
		budget = 100;
		runtimeVariation = 0.2;
		failureRate = 0.1;
		delay = 10;
		delayVariation = 0.2;
		HashMap<String, VMDescription> vmDescription = new HashMap<String, VMDescription>();
		/** Micro vm property */
		VMDescription microProperty = new VMDescription("Micro", 1000.0, 0.02);
		vmDescription.put(microProperty.vmname, microProperty);
		/** Standard vm property */
		VMDescription standardProperty = new VMDescription("Standard", 2000.0, 0.06);
		vmDescription.put(standardProperty.vmname, standardProperty);
		/** Medium vm property */
		VMDescription mediumProperty = new VMDescription("Medium", 5000.0, 0.145);
		vmDescription.put(mediumProperty.vmname, mediumProperty);
		/** High-CPU vm property */
		VMDescription highcpuProperty = new VMDescription("High-CPU", 20000.0, 0.85);
		vmDescription.put(highcpuProperty.vmname, highcpuProperty);
		/** Extra-Large vm property */
		VMDescription extralargeProperty = new VMDescription("Extra-Large", 33500.0, 1.3);
		vmDescription.put(extralargeProperty.vmname, extralargeProperty);
		timespan = 48 * 3600.0;

		String distribution[] = { "uniform_unsorted", "uniform_sorted", "pareto_unsorted", "pareto_sorted",
				"constant" };
		String workload[] = { "bursting", "stable", "growing", "onoff" };

		for (int i = 0; i < workload.length; i++) {
			for (int j = 0; j < distribution.length; j++) {
				GenerateConfiguration.generateVaryBudgetSeries(runDirectory, group, generateDagPath, dagPath,
						dagName, budget, vmDescription, workload[i], distribution[j], timespan, max_scaling,
						taskDilatation, runtimeVariation, delayVariation, deadlineVariation, failureRate,
						informtimeratio, delay, averagejobnum, job_deviation, increments, perjob_upper,
						perjob_lower, min_jobnum, 1);
				System.out.println(workload[i] + "+" + distribution[j] + " genration completed");
			}
		}

	}

	/******************************
	 * 
	 * Tests with max scaling = 0.0
	 * 
	 ******************************/

	/**
	 * bursting workload, random priority between 1~6, random submit time,
	 * random dag size random dag size
	 */

	public void testTotalRandom() {
		generateDagPath = "/Users/jiyuanshi/Downloads/dags/";
		dagPath = "/gpfssan3/jishi/experiment/dags/";
		group = "CYBERSHAKE";
		dagName = "CYBERSHAKE";
		budget = 500.0;
		runtimeVariation = 0.2;
		failureRate = 0.1;
		delay = 10;
		delayVariation = 0.2;
		HashMap<String, VMDescription> vmDescription = new HashMap<String, VMDescription>();
		/** Micro vm property */
		VMDescription microProperty = new VMDescription("Micro", 1000.0, 0.02);
		vmDescription.put(microProperty.vmname, microProperty);
		/** Standard vm property */
		VMDescription standardProperty = new VMDescription("Standard", 2000.0, 0.06);
		vmDescription.put(standardProperty.vmname, standardProperty);
		/** Medium vm property */
		VMDescription mediumProperty = new VMDescription("Medium", 5000.0, 0.145);
		vmDescription.put(mediumProperty.vmname, mediumProperty);
		/** High-CPU vm property */
		VMDescription highcpuProperty = new VMDescription("High-CPU", 20000.0, 0.85);
		vmDescription.put(highcpuProperty.vmname, highcpuProperty);
		/** Extra-Large vm property */
		VMDescription extralargeProperty = new VMDescription("Extra-Large", 33500.0, 1.3);
		vmDescription.put(extralargeProperty.vmname, extralargeProperty);
		workload = "bursting";
		distribution = "constant";
		timespan = 48 * 3600.0;

		GenerateConfiguration.generateVarySeries(runDirectory, group, generateDagPath, dagPath, dagName,
				budget, vmDescription, workload, distribution, timespan, max_scaling, taskDilatation,
				runtimeVariation, delayVariation, deadlineVariation, failureRate, informtimeratio, delay,
				averagejobnum, job_deviation, increments, perjob_upper, perjob_lower, min_jobnum, 1);

	}

	/**
	 * bursting workload, random priority between 1~6, random submit time,
	 * random dag size random dag size for just on MV type
	 */
	public void testTotalRandomOneVMtype() {
		generateDagPath = "/Users/jiyuanshi/Downloads/dags/";
		dagPath = "/gpfssan3/jishi/experiment/dags/";
		group = "OneType";
		dagName = "CYBERSHAKE";
		budget = 200;
		HashMap<String, VMDescription> vmDescription = new HashMap<String, VMDescription>();
		/** Micro vm property */
		VMDescription microProperty = new VMDescription("Micro", 1000.0, 0.02);
		vmDescription.put(microProperty.vmname, microProperty);
		workload = "bursting";
		distribution = "constant";
		timespan = 48 * 3600.0;

		GenerateConfiguration.generateVaryBudgetSeries(runDirectory, group, generateDagPath, dagPath,
				dagName, budget, vmDescription, workload, distribution, timespan, max_scaling,
				taskDilatation, runtimeVariation, delayVariation, deadlineVariation, failureRate,
				informtimeratio, delay, averagejobnum, job_deviation, increments, perjob_upper, perjob_lower,
				min_jobnum, 1);

	}

	/**
	 * bursting workload, random priority between 1~6, random submit time.
	 */
	public void testCyberShake_100() {
		group = "CyberShake_1000.dag";
		dagName = "CyberShake_1000.dag";
		budget = 100.0;
		HashMap<String, VMDescription> vmDescription = new HashMap<String, VMDescription>();
		/** Micro vm property */
		VMDescription microProperty = new VMDescription("Micro", 1000.0, 0.02);
		vmDescription.put(microProperty.vmname, microProperty);
		/** Standard vm property */
		VMDescription standardProperty = new VMDescription("Standard", 2000.0, 0.06);
		vmDescription.put(standardProperty.vmname, standardProperty);
		/** Medium vm property */
		VMDescription mediumProperty = new VMDescription("Medium", 5000.0, 0.145);
		vmDescription.put(mediumProperty.vmname, mediumProperty);
		/** High-CPU vm property */
		VMDescription highcpuProperty = new VMDescription("High-CPU", 20000.0, 0.85);
		vmDescription.put(highcpuProperty.vmname, highcpuProperty);
		/** Extra-Large vm property */
		VMDescription extralargeProperty = new VMDescription("Extra-Large", 33500.0, 1.3);
		vmDescription.put(extralargeProperty.vmname, extralargeProperty);
		workload = "bursting";
		timespan = 48 * 3600.0;

		GenerateConfiguration.generateConstantSeries(runDirectory, group, dagPath, dagName, budget,
				vmDescription, workload, timespan, max_scaling, taskDilatation, runtimeVariation,
				delayVariation, deadlineVariation, failureRate, informtimeratio, delay, averagejobnum,
				job_deviation, increments, perjob_upper, perjob_lower, min_jobnum);

	}

	public void testRunOneCyberShake_100() {
		dagName = "CyberShake_100.dag";
		Double budget = 1000.0;
		max_scaling = 0;
		group = "testRunOneCyberShake_100";
		ArrayList<DAGDescription> dags = new ArrayList<DAGDescription>();
		for (int i = 0; i < 1000; i++) {
			DAGDescription dag = new DAGDescription(dagName, dagPath, 0.0, timespan, i);
			dags.add(dag);
		}

		HashMap<String, VMDescription> vms = new HashMap<String, VMDescription>();
		VMDescription vmDescription = new VMDescription("Micro", 1000.0, 0.02);
		vms.put(vmDescription.vmname, vmDescription);

		ExperimentDescription param;
		String fileName;

		String algorithms[] = { "DPDS", "WADPDS", "SFPDS", "PPDS" };

		for (String alg : algorithms) {
			param = new ExperimentDescription(group, alg, runDirectory, dagPath, dags, "Stable", timespan,
					budget, vms, max_scaling, 0, taskDilatation, runtimeVariation, delayVariation,
					deadlineVariation, failureRate, informtimeratio, delay, "Constant", averagejobnum,
					job_deviation, increments, perjob_upper, perjob_lower, min_jobnum);
			fileName = "input-" + param.getFileName() + ".properties";
			param.storeProperties(runDirectory + File.separator + fileName);
		}

	}

	/**
	 * specific dag jobs with random submit time
	 */
	public void testRunOneCyberShake_randomsubtime_100() {
		// generateDagPath = "/Users/jiyuanshi/Downloads/dags/";
		generateDagPath = "/Users/jiyuanshi/Documents/workspace/sci-cwf-sim/ams_workflow/dag/";
		dagPath = "/gpfssan3/jishi/experiment/dags/";
		group = "MultiType";
		dagName = "ams.n.900.3.dag";
		Double budget = 1000.0;
		max_scaling = 0;
		group = "testRunOneCyberShake_100";
		ArrayList<DAGDescription> dags = new ArrayList<DAGDescription>();
		UniformIntegerDistribution distribution = new UniformIntegerDistribution(0, 30 * 3600);
		Double subtimeDouble = 0.0;
		for (int i = 0; i < 100; i++) {
			subtimeDouble = distribution.sample() * 1.0;
			DAGDescription dag = new DAGDescription(dagName, dagPath, subtimeDouble, timespan, i);
			dags.add(dag);
		}

		HashMap<String, VMDescription> vms = new HashMap<String, VMDescription>();
		VMDescription vmDescription = new VMDescription("test", 1000.0, 0.02);
		vms.put(vmDescription.vmname, vmDescription);

		ExperimentDescription param;
		String fileName;

		String algorithms[] = { "DPDS", "WADPDS", "SFPDS", "PPDS" };

		for (String alg : algorithms) {
			param = new ExperimentDescription(group, alg, runDirectory, dagPath, dags, "Stable", timespan,
					budget, vms, max_scaling, 0, taskDilatation, runtimeVariation, delayVariation,
					deadlineVariation, failureRate, informtimeratio, delay, "Constant", averagejobnum,
					job_deviation, increments, perjob_upper, perjob_lower, min_jobnum);
			fileName = "input-" + param.getFileName() + ".properties";
			param.storeProperties(runDirectory + File.separator + fileName);
		}

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		if (args.length < 2) {
			System.out.println("Please enter the run_path, dag_path and dag_name just like:");
			System.out.println("PropertyGenerator \"run_path\" \"dag_path\" \"dag_name\"");
		} else { 
			String run_path = (new File(args[0])).getAbsolutePath()+"/";
			String dag_path = (new File(args[1])).getAbsolutePath()+"/";
			String dag_name = args[2];
			System.out.println("runpath=" + run_path);
			System.out.println("dag_path=" + dag_path);
			System.out.println("dag_name=" + dag_name);
			testTotalRandomMultiBudget(run_path, dag_path, dag_name);
		}

	}

}
