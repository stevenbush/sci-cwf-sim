package scws.core.experiment;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;

import org.apache.commons.io.filefilter.FileFilterUtils;
import org.junit.Test;

import scws.core.DAGDescription;

public class GeneratePerfectResult {

	public double getExponentialScore(ArrayList<Integer> Priorities) {
		BigDecimal one = BigDecimal.ONE;
		BigDecimal two = new BigDecimal(2.0);

		BigDecimal score = new BigDecimal(0.0);
		for (Integer priority : Priorities) {
			BigDecimal divisor = two.pow(priority);
			BigDecimal increment = one.divide(divisor);
			score = score.add(increment);
		}
		return score.doubleValue();
	}

	@Test
	public void GenerateFiles() {
		String inputPath = "/Users/jiyuanshi/Documents/workspace/sci-cwf-sim/output";
		// String workload[] = { "bursting", "stable", "growing", "onoff" };
		File folder = new File(inputPath);
		String[] files = folder.list();
		int n = 0;
		for (int i = 0; i < files.length; i++) {
			String[] splitstring = files[i].split("-");
			if (splitstring.length > 2 && splitstring[2].equals("PPDS")) {
				String inputFileName =inputPath+File.separator+ files[i];
				System.out.println(inputFileName);
				ExperimentDescription description = new ExperimentDescription(inputFileName);
				ExperimentResult result = new ExperimentResult();
				ArrayList<Integer> Priorities = new ArrayList<Integer>();
				for (DAGDescription dagDescription : description.getDags()) {
					Priorities.add(dagDescription.priority);
				}
				result.setPriorities(Priorities);
				result.setSizes(new ArrayList<Double>());
				result.setBudget(description.getBudget());
				result.setExponentialScore(getExponentialScore(Priorities));
				result.setAlgorithm("PERFECT");
				result.setNumFinishedDAGs(Priorities.size());
				String fileName = "result-" + description.getFileName().replaceFirst("PPDS", "PERFECT")
						+ "-result.txt";
				result.WriteResult(description.getRunDirectory() + File.separator + fileName);
				n++;
			}
		}

		System.out.println(n);

	}
}
