package scws.core.experiment;

import static org.junit.Assert.*;

import java.util.Random;

import org.junit.Test;


import scws.core.experiment.DAGListGenerator;

public class DAGListGeneratorTest {

	//@Test
	public void testGenerateDAGList() {
        String[] dags = DAGListGenerator.generateDAGList("SIPHT", new int[] { 900, 1000 }, 20);
        assertEquals("SIPHT.n.900.0.dag", dags[0]);
        assertEquals("SIPHT.n.1000.0.dag", dags[20]);
        assertEquals("SIPHT.n.1000.19.dag", dags[39]);
        for (String string : dags) {
			System.out.println(string);
		}
    }
	
	//@Test
    public void testConstant40() {
        String[] dags = DAGListGenerator.generateDAGListConstant("SIPHT", 1000, 40);
        assertEquals("SIPHT.n.1000.0.dag", dags[0]);
        assertEquals("SIPHT.n.1000.19.dag", dags[39]);
        assertEquals("SIPHT.n.1000.7.dag", dags[7]);
        for (String string : dags) {
			System.out.println(string);
		}
    }
    
    //@Test
    public void testConstantRandom() {
        String[] dags = DAGListGenerator.generateDAGListConstant(new Random(0), "SIPHT", 40);
        //assertEquals("SIPHT.n.1000.0.dag", dags[0]);
        //assertEquals("SIPHT.n.1000.19.dag", dags[39]);
        //assertEquals("SIPHT.n.1000.7.dag", dags[7]);
        for (String string : dags) {
			System.out.println(string);
		}
    }
    
    //@Test
    public void testUniformSizesArray() {
    	int[] sizes = DAGListGenerator.generateUniformSizesArray(new Random(0), 11);
    	for (int i : sizes) {
			System.out.println(i);
		}
    }
    
    //@Test
    public void testDAGListUniform(){
    	String[] dags = DAGListGenerator.generateDAGListUniform(new Random(0), "SIPHT", 10);
    	for (String string : dags) {
			System.out.println(string);
		}
    	System.out.println();
    }
    
    //@Test
    public void testDAGListUniformUnsorted(){
    	String[] dags = DAGListGenerator.generateDAGListUniformUnsorted(new Random(0), "SIPHT", 10);
    	for (String string : dags) {
			System.out.println(string);
		}
    	System.out.println();
    }
    
    @Test
    public void testDAGListParetoUnsorted() {
        String[] dags = DAGListGenerator.generateDAGListParetoUnsorted(new Random(0), "SIPHT", 20);
        //assertEquals("SIPHT.n.1000.0.dag", dags[0]);
        //assertEquals("SIPHT.n.50.0.dag", dags[19]);
        //assertEquals("SIPHT.n.300.1.dag", dags[2]);
        for (String string : dags) {
			System.out.println(string);
		}
        System.out.println();
    }
    
    @Test
    public void testPareto() {
        String[] dags = DAGListGenerator.generateDAGListPareto(new Random(0), "SIPHT", 20);
        assertEquals("SIPHT.n.1000.0.dag", dags[0]);
        assertEquals("SIPHT.n.50.0.dag", dags[19]);
        assertEquals("SIPHT.n.300.1.dag", dags[2]);
        for (String string : dags) {
			System.out.println(string);
		}
        System.out.println();
    }

    @Test
    public void testPareto40() {
        String[] dags = DAGListGenerator.generateDAGListPareto(new Random(0), "SIPHT", 40);
        assertEquals("SIPHT.n.1000.1.dag", dags[0]);
        assertEquals("SIPHT.n.50.0.dag", dags[39]);
        assertEquals("SIPHT.n.300.1.dag", dags[7]);
        for (String string : dags) {
			System.out.println(string);
		}
        System.out.println();
    }

}
