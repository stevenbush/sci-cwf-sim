package scws.core.experiment;

import java.io.File;
import java.util.ArrayList;

import scws.core.DAGDescription;
import scws.core.dag.DAG;
import scws.core.dag.DAGParser;
import scws.core.dag.DAGStats;

public class get_workload_size {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		if (args.length < 1) {
			System.out.println("please enther the config file path:");
		} else {
			ExperimentDescription param = new ExperimentDescription(args[0]);
			ArrayList<DAGDescription> dags = param.getDags();
			Double workload_size = 0.0;
			for (DAGDescription dagDescription : dags) {
				DAG dag = DAGParser.parseDAG(
						new File(dagDescription.dagpath + "/" + dagDescription.filename),
						"BlackDiamonDAX.dag");
				DAGStats dagStats = new DAGStats(dag, param.getTaskDilatation());
				workload_size = workload_size + dagStats.getTotal_size();
			}
			System.out.println("Workload Name is: " + args[0]);
			System.out.println("Total Workload size is: " + workload_size);
		}
	}
}
