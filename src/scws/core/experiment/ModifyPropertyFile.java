package scws.core.experiment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.io.IOUtils;

public class ModifyPropertyFile {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		String algorithmName = "SIPHT";
		String runDirectory = "/afs/cern.ch/work/j/jishi/workspace/sci-cwf-sim/output/" + algorithmName;
		String dagPath = "/afs/cern.ch/work/j/jishi/workspace/sci-cwf-sim/dags/";
		String fileName = "";

		if (args.length < 2) {
			System.out.println("No input and output path specified.");
		} else {
			String inputPathString = args[0];
			String outputPathString = args[1];
			File inputPath = new File(inputPathString);
			System.out.println("input path: " + inputPath.getAbsolutePath());
			for (File file : inputPath.listFiles()) {
				System.out.println("processing file " + file.getAbsolutePath());
				Properties p = new Properties();
				FileInputStream in = null;
				try {
					in = new FileInputStream(file.getAbsolutePath());
					p.load(in);
				} catch (IOException e) {
					throw new RuntimeException(e);
				} finally {
					IOUtils.closeQuietly(in);
				}

				fileName = algorithmName + "-" + p.getProperty("fileName");
				p.setProperty("runDirectory", runDirectory);
				p.setProperty("dagPath", dagPath);
				p.setProperty("fileName", fileName);

				FileOutputStream out = null;
				try {
					out = new FileOutputStream(outputPathString + File.separator + "input-" + fileName
							+ ".properties");
					p.store(out, "");
				} catch (FileNotFoundException e) {
					throw new RuntimeException(e);
				} catch (IOException e) {
					throw new RuntimeException(e);
				} finally {
					IOUtils.closeQuietly(out);
				}

			}
		}

	}

}
