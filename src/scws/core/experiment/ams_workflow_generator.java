/**
 * This class is used to generate the ams workflow jobs
 * 
 * @author jiyuanshi
 * @since 2013-9-4
 * @version sci-cwf-sim 1.0
 */

package scws.core.experiment;

import java.util.ArrayList;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.distribution.UniformIntegerDistribution;

import edu.isi.pegasus.planner.dax.ADAG;
import edu.isi.pegasus.planner.dax.Job;

public class ams_workflow_generator {

	static String[] datasets = { "antipr-pl1", "b11-pl1", "b11-dpmjet-pl1", "c-pl1", "c-dpmjet", "el",
			"el-ecalpl1", "el-pl1", "el-pl1-ecal", "el-dpmjet-pl1-ecal", "he-pl1", "li-pl1", "n-pl1",
			"n-dpmjet-pl1", "o-pl1", "ph-pl1", "pr-pl1-ecal", "pr-pl1-flux", "pr-pl1-sdisalign", "pos-pl1" };

	static Integer[] energysets = { 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000 };

	static String path = "./ams_workflow/dax_short/";

	/**
	 * This function is used to generate the workflow dax object
	 * 
	 * @param name
	 *            , workflow name
	 * @param node_num
	 *            , nodes number in this workflow
	 * @return a ADAG object
	 */
	public ADAG generate(String name, int node_num) {
		Long runtime;
		ADAG dax = new ADAG(name);

		// Add one Detector Simulation job
		// runtime distribution for Detector Simulation job
		NormalDistribution ds_distribution = new NormalDistribution(300, 20);
		runtime = Math.round(ds_distribution.sample());
		Job ds_job = new Job("ds", "ams", "ds", "4.0", runtime.toString());
		dax.addJob(ds_job);

		// Add the 20 Simulation data generation jobs for different dataset
		ArrayList<Job> sdg_jobs = new ArrayList<Job>();
		// runtime distribution for Simulation data generation job
		NormalDistribution sdg_distribution = new NormalDistribution(240, 20);
		for (Integer i = 0; i < 20; i++) {
			runtime = Math.round(sdg_distribution.sample());
			Job sdg_job = new Job("sdg" + i.toString(), "ams", datasets[i] + "-sdg", "4.0",
					runtime.toString());
			sdg_jobs.add(sdg_job);
		}
		dax.addJobs(sdg_jobs);

		// According the node numbers to add the corresponding Data Production
		// job for every dataset
		ArrayList<ArrayList<Job>> dp_job_groups = new ArrayList<ArrayList<Job>>();
		for (Integer i = 0; i < 20; i++) {
			ArrayList<Job> dp_jobs = new ArrayList<Job>();
			for (Integer j = 0; j < node_num / 20; j++) {
				UniformIntegerDistribution energy_distribution = new UniformIntegerDistribution(0, 9);
				Integer energy = energy_distribution.sample();
				// runtime distribution for Data Production jobs
				NormalDistribution dp_distribution = new NormalDistribution(energysets[energy],
						energysets[energy] / 10);
				runtime = Math.round(dp_distribution.sample());
				Job dp_job = new Job("dp-" + datasets[i] + "-" + j.toString(), "ams", datasets[i] + "-"
						+ energysets[energy].toString(), "4.0", runtime.toString());
				dp_jobs.add(dp_job);
			}
			dax.addJobs(dp_jobs);
			dp_job_groups.add(dp_jobs);
		}

		// Add the 20 Data Aggregation jobs for different dataset
		ArrayList<Job> da_jobs = new ArrayList<Job>();
		// runtime distribution for Data Aggregation job
		NormalDistribution da_distribution = new NormalDistribution(60, 10);
		for (Integer i = 0; i < 20; i++) {
			runtime = Math.round(da_distribution.sample());
			Job da_job = new Job("da" + i.toString(), "ams", datasets[i] + "-da", "4.0", runtime.toString());
			da_jobs.add(da_job);
		}
		dax.addJobs(da_jobs);

		// Add the 20 Individual Data Analysis jobs for different dataset
		ArrayList<Job> ida_jobs = new ArrayList<Job>();
		// runtime distribution for Individual Data Analysis job
		NormalDistribution ida_distribution = new NormalDistribution(60, 10);
		for (Integer i = 0; i < 20; i++) {
			runtime = Math.round(ida_distribution.sample());
			Job ida_job = new Job("ida" + i.toString(), "ams", datasets[i] + "-ida", "4.0",
					runtime.toString());
			ida_jobs.add(ida_job);
		}
		dax.addJobs(ida_jobs);

		// Add one Combined Data Analysis job
		// runtime distribution for Combined Data Analysis job
		NormalDistribution cda_distribution = new NormalDistribution(60, 10);
		runtime = Math.round(cda_distribution.sample());
		Job cda_job = new Job("cda", "ams", "cda", "4.0", runtime.toString());
		dax.addJob(cda_job);

		// Add the jobs' dependency
		// Add the dependency between the ds_job and sdg_jobs
		for (Job job : sdg_jobs) {
			dax.addDependency(ds_job, job);
		}

		// Add the dependency between the sdg_jobs and dp_jobs
		for (int i = 0; i < dp_job_groups.size(); i++) {
			Job parent_job = sdg_jobs.get(i);
			for (int j = 0; j < dp_job_groups.get(i).size(); j++) {
				dax.addDependency(parent_job, dp_job_groups.get(i).get(j));
			}
		}

		// Add the dependency between the dp_jobs and da_jobs
		for (int i = 0; i < dp_job_groups.size(); i++) {
			Job child_job = da_jobs.get(i);
			for (int j = 0; j < dp_job_groups.get(i).size(); j++) {
				dax.addDependency(dp_job_groups.get(i).get(j), child_job);
			}
		}

		// Add the dependency between the da_jobs and ina_jobs
		for (int i = 0; i < da_jobs.size(); i++) {
			dax.addDependency(da_jobs.get(i), ida_jobs.get(i));
		}

		// Add the dependency between the da_jobs and the cda_job
		for (Job job : da_jobs) {
			dax.addDependency(job, cda_job);
		}

		return dax;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		try {
			Integer nodes_num = 1000;
			Integer index = 20;
			ams_workflow_generator generator = new ams_workflow_generator();
			
			for (int j = 0; j < index; j++) {
				ADAG dag = generator.generate("ams", 50);
				dag.writeToFile(path + "ams.n." + 50 + "." + j + ".dax");
				System.out.println("generating: " + "ams.n." + 50 + "." + j + ".dax");
			}

			for (int i = 100; i <= nodes_num; i = i + 100) {
				for (int j = 0; j < index; j++) {
					ADAG dag = generator.generate("ams", i);
					dag.writeToFile(path + "ams.n." + i + "." + j + ".dax");
					System.out.println("generating: " + "ams.n." + i + "." + j + ".dax");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
