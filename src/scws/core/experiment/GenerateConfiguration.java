package scws.core.experiment;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import org.apache.commons.math3.distribution.UniformIntegerDistribution;

import scws.core.experiment.DAGListGenerator;
import scws.core.DAGDescription;
import scws.core.VMDescription;
import scws.core.dag.DAG;
import scws.core.dag.DAGParser;
import scws.core.dag.DAGStats;

/**
 * This class contains different tools to generate different kind of experiment
 * configuration files.
 * 
 * @author jiyuanshi
 * @since 2013-4-14
 * @version sci-cwf-sim 1.0
 */
public class GenerateConfiguration {

	/**
	 * Generates a series of experiments configuration file for different
	 * parameter
	 * 
	 * @param runDirectory
	 *            : the root directory of the input and output file
	 * @param group
	 *            : the group description of this experiment
	 * @param dagPath
	 *            : the dag file path
	 * @param dags
	 *            : all the jobs' dag information stored in the HashMap
	 * @param budget
	 *            : the budhet in $ for total timespan
	 * @param vmDescription
	 *            : the vm description include vm name, vm price and vm power in
	 *            bips
	 * @param workload
	 *            : the workload type of this experiment: growing, bursting,
	 *            stable, onoff
	 * @param timespan
	 *            : the total time span of experiment, 48 hours default.
	 * @param max_scaling
	 *            : max autoscaling factor for provisioner
	 * @param taskDilatation
	 *            : task runtimes from the dag are multiplied by this factor;
	 *            this parameter is useful to control the task granularity
	 * @param runtimeVariation
	 *            : defines the maximum relative difference between estimated
	 *            and actual task runtime, e.g. 0.50 means that task can run 50%
	 *            longer than a given estimate.
	 * @param deadlineVariation
	 *            : defines the maximum relative difference between the deadline
	 *            and the longest job runtime, eg 0.5 means that deadline can be
	 *            50% longer than the longest estimated job runtime.
	 * @param informtimeratio
	 *            : the proportion of in advance inform time
	 * @param delay
	 *            : provisioning delay in seconds
	 * @param distribution
	 *            : the distribution type of this experiment.
	 * @param runID
	 *            : runID of this experiment.
	 */
	public static void generateSeries(String runDirectory, String group, String dagPath,
			ArrayList<DAGDescription> dags, double budget, HashMap<String, VMDescription> vmDescription,
			String workload, Double timespan, double max_scaling, double taskDilatation,
			double runtimeVariation, double delayVariation, double deadlineVariation, double failureRate,
			double informtimeratio, double delay, String distribution, int runID, int averagejobnum,
			int job_deviation, int increments, int perjob_upper, int perjob_lower, int min_jobnum) {

		new File(runDirectory).mkdir();

		// String algorithms[] = {"SPSS", "DPDS", "WADPDS", "MaxMin", "Wide",
		// "Backtrack"};
		String algorithms[] = { "DPDS", "WADPDS", "SFPDS", "PPDS", "WPPDS" };

		for (String alg : algorithms) {
			ExperimentDescription param = new ExperimentDescription(group, alg, runDirectory, dagPath, dags,
					workload, timespan, budget, vmDescription, max_scaling, runID, taskDilatation,
					runtimeVariation, delayVariation, deadlineVariation, failureRate, informtimeratio, delay,
					distribution, averagejobnum, job_deviation, increments, perjob_upper, perjob_lower,
					min_jobnum);

			String fileName = "input-" + param.getFileName() + ".properties";
			param.storeProperties(runDirectory + File.separator + fileName);
		}

	}

	public static void generatePPDSSeries(String runDirectory, String group, String dagPath,
			ArrayList<DAGDescription> dags, double budget, HashMap<String, VMDescription> vmDescription,
			String workload, Double timespan, double max_scaling, double taskDilatation,
			double runtimeVariation, double delayVariation, double deadlineVariation, double failureRate,
			double informtimeratio, double delay, String distribution, int runID, int averagejobnum,
			int job_deviation, int increments, int perjob_upper, int perjob_lower, int min_jobnum) {

		new File(runDirectory).mkdir();

		// String algorithms[] = {"SPSS", "DPDS", "WADPDS", "MaxMin", "Wide",
		// "Backtrack"};
		String algorithms[] = { "PPDS", "WPPDS" };

		for (String alg : algorithms) {
			ExperimentDescription param = new ExperimentDescription(group, alg, runDirectory, dagPath, dags,
					workload, timespan, budget, vmDescription, max_scaling, runID, taskDilatation,
					runtimeVariation, delayVariation, deadlineVariation, failureRate, informtimeratio, delay,
					distribution, averagejobnum, job_deviation, increments, perjob_upper, perjob_lower,
					min_jobnum);

			String fileName = "input-" + param.getFileName() + ".properties";
			param.storeProperties(runDirectory + File.separator + fileName);
		}

	}

	/**
	 * Generates a series constructing dags array by repeating the same DAG file
	 * numDAGs times. Sets runID to 0.
	 * 
	 * @param runDirectory
	 *            : the root directory of the input and output file
	 * @param group
	 *            : the group description of this experiment
	 * @param dagPath
	 *            : the dag file path
	 * @param dagName
	 *            : the name of the dag
	 * @param budget
	 *            : the budhet in $ for total timespan
	 * @param vmDescription
	 *            : the vm description include vm name, vm price and vm power in
	 *            bips
	 * @param workload
	 *            : the workload type of this experiment: growing, bursting,
	 *            stable, onoff
	 * @param timespan
	 *            : the total time span of experiment, 48 hours default.
	 * @param max_scaling
	 *            : max autoscaling factor for provisioner
	 * @param taskDilatation
	 *            : task runtimes from the dag are multiplied by this factor;
	 *            this parameter is useful to control the task granularity
	 * @param runtimeVariation
	 *            : defines the maximum relative difference between estimated
	 *            and actual task runtime, e.g. 0.50 means that task can run 50%
	 *            longer than a given estimate
	 * @param deadlineVariation
	 *            : defines the maximum relative difference between the deadline
	 *            and the longest job runtime, eg 0.5 means that deadline can be
	 *            50% longer than the longest estimated job runtime.
	 * @param delay
	 *            : provisioning delay in seconds
	 */
	public static void generateConstantSeries(String runDirectory, String group, String dagPath,
			String dagName, double budget, HashMap<String, VMDescription> vmDescription, String workload,
			Double timespan, double max_scaling, double taskDilatation, double runtimeVariation,
			double delayVariation, double deadlineVariation, double failureRate, double informtimeratio,
			double delay, int averagejobnum, int job_deviation, int increments, int perjob_upper,
			int perjob_lower, int min_jobnum) {

		WorkloadGenerator workloadGenerator = new WorkloadGenerator(timespan, averagejobnum, job_deviation,
				increments, perjob_upper, perjob_lower, min_jobnum);

		ArrayList<Double> subtimes = new ArrayList<Double>();

		// according the workload type to generate different job submitted time
		// distribution.
		if (workload.equals("stable")) {
			subtimes = workloadGenerator.Distribute_stable();
		} else if (workload.equals("growing")) {
			subtimes = workloadGenerator.Distribute_growing();
		} else if (workload.equals("onoff")) {
			subtimes = workloadGenerator.Distribute_onoff();
		} else if (workload.equals("bursting")) {
			subtimes = workloadGenerator.Distribute_bursting();
		} else {
			subtimes = workloadGenerator.Distribute_stable();
		}

		UniformIntegerDistribution uniformIntegerDistribution = new UniformIntegerDistribution(0, 10);
		Integer priority;
		//System.out.println(dagPath + File.separator + dagName);
		DAG dag = DAGParser.parseDAG(new File(dagPath + File.separator + dagName), dagName);
		DAGStats dagStats = new DAGStats(dag, taskDilatation, vmDescription);
		Double deadline = dagStats.getMaxCriticalPath() * (1 + deadlineVariation);
		// System.out.println(dagStats.getMaxCriticalPath());
		ArrayList<DAGDescription> dags = new ArrayList<DAGDescription>(); // dags'
																			// description

		for (Double subtime : subtimes) {
			priority = uniformIntegerDistribution.sample();
			DAGDescription dagDescription = new DAGDescription(dagName, dagPath, subtime, subtime + deadline,
					priority);
			dags.add(dagDescription);
			// System.out.println("dagname: "+dagName+"~"+subtime+"~"+deadline);
		}

		generateSeries(runDirectory, group, dagPath, dags, budget, vmDescription, workload, timespan,
				max_scaling, taskDilatation, runtimeVariation, delayVariation, deadlineVariation,
				failureRate, informtimeratio, delay, "constant", 0, averagejobnum, job_deviation, increments,
				perjob_upper, perjob_lower, min_jobnum);

	}

	/**
	 * Generates a vary series constructing dags array by random chose the dag
	 * size numDAGs times. Sets runID to 0.
	 * 
	 * @param runDirectory
	 *            : the root directory of the input and output file
	 * @param group
	 *            : the group description of this experiment
	 * @param dagPath
	 *            : the dag file path
	 * @param dagName
	 *            : the name of the dag
	 * @param budget
	 *            : the budhet in $ for total timespan
	 * @param vmDescription
	 *            : the vm description include vm name, vm price and vm power in
	 *            bips
	 * @param workload
	 *            : the workload type of this experiment: growing, bursting,
	 *            stable, onoff
	 * @param timespan
	 *            : the total time span of experiment, 48 hours default.
	 * @param max_scaling
	 *            : max autoscaling factor for provisioner
	 * @param taskDilatation
	 *            : task runtimes from the dag are multiplied by this factor;
	 *            this parameter is useful to control the task granularity
	 * @param runtimeVariation
	 *            : defines the maximum relative difference between estimated
	 *            and actual task runtime, e.g. 0.50 means that task can run 50%
	 *            longer than a given estimate
	 * @param deadlineVariation
	 *            : defines the maximum relative difference between the deadline
	 *            and the longest job runtime, eg 0.5 means that deadline can be
	 *            50% longer than the longest estimated job runtime.
	 * @param delay
	 *            : provisioning delay in seconds
	 */
	public static void generateVarySeries(String runDirectory, String group, String generateDagPath,
			String dagPath, String dagName, double budget, HashMap<String, VMDescription> vmDescription,
			String workload, String distribution, Double timespan, double max_scaling, double taskDilatation,
			double runtimeVariation, double delayVariation, double deadlineVariation, double failureRate,
			double informtimeratio, double delay, int averagejobnum, int job_deviation, int increments,
			int perjob_upper, int perjob_lower, int min_jobnum, int runID) {

		WorkloadGenerator workloadGenerator = new WorkloadGenerator(timespan, averagejobnum, job_deviation,
				increments, perjob_upper, perjob_lower, min_jobnum);
		String[] dagNames = null;
		ArrayList<Double> subtimes = new ArrayList<Double>();

		// according the workload type to generate different job submitted time
		// distribution.
		if (workload.equals("stable")) {
			subtimes = workloadGenerator.Distribute_stable();
		} else if (workload.equals("growing")) {
			subtimes = workloadGenerator.Distribute_growing();
		} else if (workload.equals("onoff")) {
			subtimes = workloadGenerator.Distribute_onoff();
		} else if (workload.equals("bursting")) {
			subtimes = workloadGenerator.Distribute_bursting();
		} else {
			subtimes = workloadGenerator.Distribute_stable();
		}

		if ("uniform_unsorted".equals(distribution) || "uniform_sorted".equals(distribution)) {
			dagNames = DAGListGenerator.generateDAGListUniformUnsorted(new Random(runID), dagName,
					subtimes.size());
		} else if ("pareto_unsorted".equals(distribution) || "pareto_sorted".equals(distribution)) {
			dagNames = DAGListGenerator.generateDAGListParetoUnsorted(new Random(runID), dagName,
					subtimes.size());
		} else if ("constant".equals(distribution)) {
			dagNames = DAGListGenerator.generateDAGListConstant(new Random(runID), dagName, subtimes.size());
		} else if (distribution.startsWith("fixed")) {
			int size = Integer.parseInt(distribution.substring(5));
			dagNames = DAGListGenerator.generateDAGListConstant(dagName, size, subtimes.size());
		} else {
			System.err.println("Unrecognized distribution: " + distribution);
			System.exit(1);
		}

		UniformIntegerDistribution uniformIntegerDistribution = new UniformIntegerDistribution(0, 10);
		Integer priority;
		// System.out.println(dagStats.getMaxCriticalPath());
		// dag's decription
		ArrayList<DAGDescription> dags = new ArrayList<DAGDescription>();

		for (Double subtime : subtimes) {
			//System.out.println(generateDagPath + File.separator + dagNames[subtimes.indexOf(subtime)]);
			DAG dag = DAGParser.parseDAG(
					new File(generateDagPath + File.separator + dagNames[subtimes.indexOf(subtime)]),
					dagNames[subtimes.indexOf(subtime)]);
			DAGStats dagStats = new DAGStats(dag, taskDilatation, vmDescription);
			Double deadline = dagStats.getMaxCriticalPath() * (1 + deadlineVariation);

			if ("uniform_sorted".equals(distribution) || "pareto_sorted".equals(distribution)) {
				priority = 10 - Math.round(dag.getTasks().length / 100);
			} else {
				priority = uniformIntegerDistribution.sample();
			}

			DAGDescription dagDescription = new DAGDescription(dagNames[subtimes.indexOf(subtime)], dagPath,
					subtime, subtime + deadline, priority);
			dags.add(dagDescription);
			// System.out.println("dagname: "+dagName+"~"+subtime+"~"+deadline);
		}

		generateSeries(runDirectory, group, dagPath, dags, budget, vmDescription, workload, timespan,
				max_scaling, taskDilatation, 0.0, 0.0, deadlineVariation, 0.0, informtimeratio, delay,
				distribution, 0, averagejobnum, job_deviation, increments, perjob_upper, perjob_lower,
				min_jobnum);

		generatePPDSSeries(runDirectory, group, dagPath, dags, budget, vmDescription, workload, timespan,
				max_scaling, taskDilatation, 0.0, 0.0, deadlineVariation, failureRate, informtimeratio,
				delay, distribution, 0, averagejobnum, job_deviation, increments, perjob_upper, perjob_lower,
				min_jobnum);

		generatePPDSSeries(runDirectory, group, dagPath, dags, budget, vmDescription, workload, timespan,
				max_scaling, taskDilatation, 0.0, delayVariation, deadlineVariation, 0.0, informtimeratio,
				delay, distribution, 0, averagejobnum, job_deviation, increments, perjob_upper, perjob_lower,
				min_jobnum);

		generatePPDSSeries(runDirectory, group, dagPath, dags, budget, vmDescription, workload, timespan,
				max_scaling, taskDilatation, runtimeVariation, 0.0, deadlineVariation, 0.0, informtimeratio,
				delay, distribution, 0, averagejobnum, job_deviation, increments, perjob_upper, perjob_lower,
				min_jobnum);

	}

	/**
	 * Generates a vary series constructing dags array by random chose the dag
	 * size numDAGs times. Sets runID to 0.
	 * 
	 * @param runDirectory
	 *            : the root directory of the input and output file
	 * @param group
	 *            : the group description of this experiment
	 * @param dagPath
	 *            : the dag file path
	 * @param dagName
	 *            : the name of the dag
	 * @param budget
	 *            : the budhet in $ for total timespan
	 * @param vmDescription
	 *            : the vm description include vm name, vm price and vm power in
	 *            bips
	 * @param workload
	 *            : the workload type of this experiment: growing, bursting,
	 *            stable, onoff
	 * @param timespan
	 *            : the total time span of experiment, 48 hours default.
	 * @param max_scaling
	 *            : max autoscaling factor for provisioner
	 * @param taskDilatation
	 *            : task runtimes from the dag are multiplied by this factor;
	 *            this parameter is useful to control the task granularity
	 * @param runtimeVariation
	 *            : defines the maximum relative difference between estimated
	 *            and actual task runtime, e.g. 0.50 means that task can run 50%
	 *            longer than a given estimate
	 * @param deadlineVariation
	 *            : defines the maximum relative difference between the deadline
	 *            and the longest job runtime, eg 0.5 means that deadline can be
	 *            50% longer than the longest estimated job runtime.
	 * @param delay
	 *            : provisioning delay in seconds
	 */
	public static void generateVaryBudgetSeries(String runDirectory, String group, String generateDagPath,
			String dagPath, String dagName, double budget, HashMap<String, VMDescription> vmDescription,
			String workload, String distribution, Double timespan, double max_scaling, double taskDilatation,
			double runtimeVariation, double delayVariation, double deadlineVariation, double failureRate,
			double informtimeratio, double delay, int averagejobnum, int job_deviation, int increments,
			int perjob_upper, int perjob_lower, int min_jobnum, int runID) {

		WorkloadGenerator workloadGenerator = new WorkloadGenerator(timespan, averagejobnum, job_deviation,
				increments, perjob_upper, perjob_lower, min_jobnum);
		String[] dagNames = null;
		ArrayList<Double> subtimes = new ArrayList<Double>();

		// according the workload type to generate different job submitted time
		// distribution.
		if (workload.equals("stable")) {
			subtimes = workloadGenerator.Distribute_stable();
		} else if (workload.equals("growing")) {
			subtimes = workloadGenerator.Distribute_growing();
		} else if (workload.equals("onoff")) {
			subtimes = workloadGenerator.Distribute_onoff();
		} else if (workload.equals("bursting")) {
			subtimes = workloadGenerator.Distribute_bursting();
		} else {
			subtimes = workloadGenerator.Distribute_stable();
		}

		if ("uniform_unsorted".equals(distribution) || "uniform_sorted".equals(distribution)) {
			dagNames = DAGListGenerator.generateDAGListUniformUnsorted(new Random(runID), dagName,
					subtimes.size());
		} else if ("pareto_unsorted".equals(distribution) || "pareto_sorted".equals(distribution)) {
			dagNames = DAGListGenerator.generateDAGListParetoUnsorted(new Random(runID), dagName,
					subtimes.size());
		} else if ("constant".equals(distribution)) {
			dagNames = DAGListGenerator.generateDAGListConstant(new Random(runID), dagName, subtimes.size());
		} else if (distribution.startsWith("fixed")) {
			int size = Integer.parseInt(distribution.substring(5));
			dagNames = DAGListGenerator.generateDAGListConstant(dagName, size, subtimes.size());
		} else {
			System.err.println("Unrecognized distribution: " + distribution);
			System.exit(1);
		}

		UniformIntegerDistribution uniformIntegerDistribution = new UniformIntegerDistribution(0, 10);
		Integer priority;
		// System.out.println(dagStats.getMaxCriticalPath());
		// dag's description
		ArrayList<DAGDescription> dags = new ArrayList<DAGDescription>();

		for (Double subtime : subtimes) {
			//System.out.println(generateDagPath + File.separator + dagNames[subtimes.indexOf(subtime)]);
			DAG dag = DAGParser.parseDAG(
					new File(generateDagPath + File.separator + dagNames[subtimes.indexOf(subtime)]),
					dagNames[subtimes.indexOf(subtime)]);
			DAGStats dagStats = new DAGStats(dag, taskDilatation, vmDescription);
			Double deadline = dagStats.getMaxCriticalPath() * (1 + deadlineVariation);

			if ("uniform_sorted".equals(distribution) || "pareto_sorted".equals(distribution)) {
				priority = 10 - Math.round(dag.getTasks().length / 100);
			} else {
				priority = uniformIntegerDistribution.sample();
			}

			DAGDescription dagDescription = new DAGDescription(dagNames[subtimes.indexOf(subtime)], dagPath,
					subtime, subtime + deadline, priority);
			dags.add(dagDescription);
			// System.out.println("dagname: "+dagName+"~"+subtime+"~"+deadline);
		}

		// for (Double i = budget; i <= 8000.0; i = i + 500) {
		// generateSeries(runDirectory, group, dagPath, dags, i, vmDescription,
		// workload, timespan, max_scaling, taskDilatation,
		// runtimeVariation, deadlineVariation, informtimeratio, delay,
		// "constant", 0, averagejobnum, job_deviation, increments,
		// perjob_upper, perjob_lower, min_jobnum);
		// }

		Double i = budget;
		while (i <= 2000) {
			System.out.println("generating budget: "+i);

			generateSeries(runDirectory, group, dagPath, dags, i, vmDescription, workload, timespan,
					max_scaling, taskDilatation, 0.0, 0.0, deadlineVariation, 0.0, informtimeratio, delay,
					distribution, 0, averagejobnum, job_deviation, increments, perjob_upper, perjob_lower,
					min_jobnum);

			generatePPDSSeries(runDirectory, group, dagPath, dags, i, vmDescription, workload, timespan,
					max_scaling, taskDilatation, 0.0, 0.0, deadlineVariation, failureRate, informtimeratio,
					delay, distribution, 0, averagejobnum, job_deviation, increments, perjob_upper,
					perjob_lower, min_jobnum);

			generatePPDSSeries(runDirectory, group, dagPath, dags, i, vmDescription, workload, timespan,
					max_scaling, taskDilatation, 0.0, delayVariation, deadlineVariation, 0.0,
					informtimeratio, delay, distribution, 0, averagejobnum, job_deviation, increments,
					perjob_upper, perjob_lower, min_jobnum);

			generatePPDSSeries(runDirectory, group, dagPath, dags, i, vmDescription, workload, timespan,
					max_scaling, taskDilatation, runtimeVariation, 0.0, deadlineVariation, 0.0,
					informtimeratio, delay, distribution, 0, averagejobnum, job_deviation, increments,
					perjob_upper, perjob_lower, min_jobnum);

			if (i < 1000) {
				i = i + 50;
			} else if (i >= 1000 && i < 3000) {
				i = i + 100;
			} else {
				i = i + 200;
			}
		}

	}

}
