package scws.core.experiment;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import org.apache.commons.io.IOUtils;

import scws.core.DAGDescription;
import scws.core.VMDescription;

/**
 * Class representing a single experiment of submitting a given workflow
 * workload with specified budget, VM types and DAG deadline constraints, by
 * using the method in this class you can generate the experiment configure
 * files.
 * 
 * @author jiyuanshi
 * @since 2013-04-12
 * @version sci-cwf-sim 1.0
 */
public class ExperimentDescription {

	/** the description of this experiment */
	private String group;
	/** name of algorithm to use */
	private String algorithmName;
	/** directory of input and output files for this experiment */
	private String runDirectory;
	/** path of the input DAG files */
	private String dagPath;
	/**
	 * array that store all the job's description include the dag file name,
	 * submitted time, deadline and priority
	 */
	private ArrayList<DAGDescription> dags;
	/** the workload type of this experiment: stable, growing, bursting, onoff */
	private String workload;
	/** the total time span of experiment */
	private double timespan;
	/** the total budget in $ of experiment */
	private double budget;
	/**
	 * VM descriptions include the hour price in $, computing power in mips and
	 * type name
	 */
	private HashMap<String, VMDescription> vmDescription;
	/** max autoscaling factor for provisioner */
	private double maxScaling;
	/** runID used to distinguish e.g. different random seeds */
	private int runID;

	/** the Unique name of this file */
	private String fileName;

	/**
	 * task runtimes from the dag are multiplied by this factor; this parameter
	 * is useful to control the task granularity
	 */
	private double taskDilatation;
	/**
	 * defines the maximum relative difference between estimated and actual task
	 * runtime, e.g. 0.50 means that task can run 50% longer than a given
	 * estimate
	 */
	private double runtimeVariation;

	/**
	 * defines the maximum relative difference between estimated and actual vm
	 * provisioning delay time , e.g. 0.50 means that task can run 50% longer
	 * than a given estimate
	 */
	private double delayVariation;

	/**
	 * defines the maximum relative difference between the deadline and the
	 * longest job runtime, eg 0.5 means that deadline can be 50% longer than
	 * the longest estimated job runtime.
	 */
	private double deadlineVariation;
	/** the failure rate of the job */
	private double failureRate;
	/** the proportion of the inform advance time. */
	private double informtimeratio;
	/** provisioning delay in seconds */
	private double delay;
	/** distribution of ensembles */
	private String distribution;
	/** the average jobs number per hour in the workload. */
	private int averagejobnum;
	/** the deviation for the number of jobs submitted every hour */
	private int job_deviation;
	/** the increments of job number for growing workload */
	private int increments;
	/** the upper bound job number for the workload */
	private int perjob_upper;
	/** the lower bound job number for the workload */
	private int perjob_lower;
	/** the min job number of the growing workload */
	private int min_jobnum;

	/**
	 * create this class by the input parameters.
	 */
	public ExperimentDescription(String group, String algorithmName, String runDirectory, String dagPath,
			ArrayList<DAGDescription> dags, String workload, Double timespan, double budget,
			HashMap<String, VMDescription> vmDescription, double maxScaling, int runID,
			double taskDilatation, double runtimeVariation, double delayVariation, double deadlineVariation,
			double failureRate, double informtimeratio, double delay, String distribution, int averagejobnum,
			int job_deviation, int increments, int perjob_upper, int perjob_lower, int min_jobnum) {
		this.group = group;
		this.algorithmName = algorithmName;
		this.runDirectory = runDirectory;
		this.dagPath = dagPath;
		this.dags = dags;
		this.workload = workload;
		this.timespan = timespan;
		this.budget = budget;
		this.vmDescription = vmDescription;
		this.maxScaling = maxScaling;
		this.runID = runID;
		this.taskDilatation = taskDilatation;
		this.runtimeVariation = runtimeVariation;
		this.delayVariation = delayVariation;
		this.deadlineVariation = deadlineVariation;
		this.failureRate = failureRate;
		this.informtimeratio = informtimeratio;
		this.delay = delay;
		this.distribution = distribution;
		this.averagejobnum = averagejobnum;
		this.job_deviation = job_deviation;
		this.increments = increments;
		this.perjob_upper = perjob_upper;
		this.perjob_lower = perjob_lower;
		this.min_jobnum = min_jobnum;
	}

	/**
	 * create this class by using configuration information from configuration
	 * file.
	 * 
	 * @param propertyFileName
	 */
	public ExperimentDescription(String propertyFileName) {
		readProperties(propertyFileName);
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getAlgorithmName() {
		return algorithmName;
	}

	public void setAlgorithmName(String algorithmName) {
		this.algorithmName = algorithmName;
	}

	public String getRunDirectory() {
		return runDirectory;
	}

	public void setRunDirectory(String runDirectory) {
		this.runDirectory = runDirectory;
	}

	public String getDagPath() {
		return dagPath;
	}

	public void setDagPath(String dagPath) {
		this.dagPath = dagPath;
	}

	public ArrayList<DAGDescription> getDags() {
		return dags;
	}

	public void setDags(ArrayList<DAGDescription> dags) {
		this.dags = dags;
	}

	public double getTimespan() {
		return timespan;
	}

	public void setTimespan(Double timespan) {
		this.timespan = timespan;
	}

	public double getBudget() {
		return budget;
	}

	public void setBudget(double budget) {
		this.budget = budget;
	}

	public HashMap<String, VMDescription> getVmDescription() {
		return vmDescription;
	}

	public void setVmDescription(HashMap<String, VMDescription> vmDescription) {
		this.vmDescription = vmDescription;
	}

	public double getMaxScaling() {
		return maxScaling;
	}

	public void setMaxScaling(double maxScaling) {
		this.maxScaling = maxScaling;
	}

	public int getRunID() {
		return runID;
	}

	public void setRunID(int runID) {
		this.runID = runID;
	}

	public double getTaskDilatation() {
		return taskDilatation;
	}

	public void setTaskDilatation(double taskDilatation) {
		this.taskDilatation = taskDilatation;
	}

	public double getRuntimeVariation() {
		return runtimeVariation;
	}

	public void setRuntimeVariation(double runtimeVariation) {
		this.runtimeVariation = runtimeVariation;
	}

	public double getDelay() {
		return delay;
	}

	public void setDelay(double delay) {
		this.delay = delay;
	}

	public String getDistribution() {
		return distribution;
	}

	public void setDistribution(String distribution) {
		this.distribution = distribution;
	}

	/**
	 * Format the dags description into a string for storing it to a property
	 * file.
	 * 
	 * @return the string value after the formating
	 */
	private String dagsToString() {
		StringBuilder s = new StringBuilder();
		for (DAGDescription dag : dags)
			s.append(dag.filename + "~" + dag.subtime + "~" + dag.deadline + "~" + dag.priority + " ");
		return s.toString();
	}

	/**
	 * Format the vms information into a string for storing it to a property
	 * file.
	 * 
	 * @return the string value after the formating
	 */
	private String vmsToString() {
		StringBuilder s = new StringBuilder();
		for (VMDescription vm : vmDescription.values())
			s.append(vm.vmname + ":" + vm.mips + ":" + vm.price + " ");
		return s.toString();
	}

	/**
	 * format the dag file name string reading from the property file to a
	 * ArrayList<DAGDescription>
	 * 
	 * @param property
	 *            : the property string
	 * @return the DAGDescription arraylist.
	 */
	private ArrayList<DAGDescription> dagsFromString(String property) {
		ArrayList<DAGDescription> returnArrayList = new ArrayList<DAGDescription>();
		String[] stringarray = property.split(" ");
		String[] dagdescriptions;
		for (String string : stringarray) {
			dagdescriptions = string.split("~");
			DAGDescription dagDescription = new DAGDescription(dagdescriptions[0], dagPath,
					Double.parseDouble(dagdescriptions[1]), Double.parseDouble(dagdescriptions[2]),
					Integer.parseInt(dagdescriptions[3]));
			returnArrayList.add(dagDescription);

		}
		return returnArrayList;
	}

	/**
	 * format the vms information string reading from the property file to a
	 * HashMap variable
	 * 
	 * @param property
	 *            : the property string
	 * @return the HashMap variable
	 */
	private HashMap<String, VMDescription> vmsFromString(String property) {
		HashMap<String, VMDescription> returnArrayList = new HashMap<String, VMDescription>();
		String[] vmstr = property.split(" ");
		String[] value;
		for (String string : vmstr) {
			value = string.split(":");
			VMDescription vmDescription = new VMDescription(value[0], Double.parseDouble(value[1]),
					Double.parseDouble(value[2]));
			returnArrayList.put(vmDescription.vmname, vmDescription);
		}

		return returnArrayList;

	}

	/**
	 * Store the configuration information into a configuration file
	 * 
	 * @param fileName
	 *            : the path and name of this file.
	 */
	public void storeProperties(String fileName) {
		Properties p = new Properties();
		p.setProperty("group", group);
		p.setProperty("algorithmName", algorithmName);
		p.setProperty("runDirectory", runDirectory);
		p.setProperty("dagPath", dagPath);
		p.setProperty("dags", dagsToString());
		p.setProperty("workload", workload);
		p.setProperty("timespan", "" + timespan);
		p.setProperty("budget", "" + budget);
		p.setProperty("vmDescription", "" + vmsToString());
		p.setProperty("maxScaling", "" + maxScaling);
		p.setProperty("runID", "" + runID);
		p.setProperty("fileName", getUniqueFileName());
		p.setProperty("taskDilatation", "" + taskDilatation);
		p.setProperty("runtimeVariation", "" + runtimeVariation);
		p.setProperty("delayVariation", "" + delayVariation);
		p.setProperty("deadlineVariation", "" + deadlineVariation);
		p.setProperty("failureRate", "" + failureRate);
		p.setProperty("informtimeratio", "" + informtimeratio);
		p.setProperty("delay", "" + delay);
		p.setProperty("distribution", getDistribution());
		p.setProperty("averagejobnum", "" + averagejobnum);
		p.setProperty("job_deviation", "" + job_deviation);
		p.setProperty("increments", "" + increments);
		p.setProperty("perjob_lower", "" + perjob_lower);
		p.setProperty("perjob_upper", "" + perjob_upper);
		p.setProperty("min_jobnum", "" + min_jobnum);
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(fileName);
			p.store(out, "");
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		} finally {
			IOUtils.closeQuietly(out);
		}
	}

	/**
	 * read the relevant configuration information from the configuration file.
	 * 
	 * @param fileName
	 *            : the configuration file path and name.
	 */
	private void readProperties(String propertyfileName) {
		Properties p = new Properties();
		FileInputStream in = null;
		try {
			in = new FileInputStream(propertyfileName);
			p.load(in);
		} catch (IOException e) {
			throw new RuntimeException(e);
		} finally {
			IOUtils.closeQuietly(in);
		}
		group = p.getProperty("group");
		algorithmName = p.getProperty("algorithmName");
		runDirectory = p.getProperty("runDirectory");
		dagPath = p.getProperty("dagPath");
		dags = dagsFromString(p.getProperty("dags"));
		workload = p.getProperty("workload");
		timespan = Double.parseDouble(p.getProperty("timespan"));
		budget = Double.parseDouble(p.getProperty("budget"));
		vmDescription = vmsFromString(p.getProperty("vmDescription"));
		maxScaling = Double.parseDouble(p.getProperty("maxScaling"));
		runID = Integer.parseInt(p.getProperty("runID"));
		fileName = p.getProperty("fileName");
		taskDilatation = Double.parseDouble(p.getProperty("taskDilatation"));
		runtimeVariation = Double.parseDouble(p.getProperty("runtimeVariation"));
		delayVariation = Double.parseDouble(p.getProperty("delayVariation"));
		deadlineVariation = Double.parseDouble(p.getProperty("deadlineVariation"));
		failureRate = Double.parseDouble(p.getProperty("failureRate"));
		informtimeratio = Double.parseDouble(p.getProperty("informtimeratio"));
		delay = Double.parseDouble(p.getProperty("delay"));
		distribution = p.getProperty("distribution");
		averagejobnum = Integer.parseInt(p.getProperty("averagejobnum"));
		job_deviation = Integer.parseInt(p.getProperty("job_deviation"));
		increments = Integer.parseInt(p.getProperty("increments"));
		perjob_lower = Integer.parseInt(p.getProperty("perjob_lower"));
		perjob_upper = Integer.parseInt(p.getProperty("perjob_upper"));
		min_jobnum = Integer.parseInt(p.getProperty("min_jobnum"));

	}

	/**
	 * Creates a unique configure file name based on the values of the
	 * properties
	 */
	public String getUniqueFileName() {
		String fileName = group + "-" + getAlgorithmName() + "-" + getDistribution() + "-" + getDags().size()
				+ "-" + getWorkload() + "-" + getBudget() + "-d" + getDelay() + "-dv" + getDelayVariation()
				+ "-rv" + getRuntimeVariation() + "-f" + getFailureRate() + runID;

		return fileName;

	}

	public double getDeadlineVariation() {
		return deadlineVariation;
	}

	public void setDeadlineVariation(double deadlineVariation) {
		this.deadlineVariation = deadlineVariation;
	}

	public String getWorkload() {
		return workload;
	}

	public void setWorkload(String workload) {
		this.workload = workload;
	}

	public int getAveragejobnum() {
		return averagejobnum;
	}

	public void setAveragejobnum(int averagejobnum) {
		this.averagejobnum = averagejobnum;
	}

	public int getJob_deviation() {
		return job_deviation;
	}

	public void setJob_deviation(int job_deviation) {
		this.job_deviation = job_deviation;
	}

	public int getIncrements() {
		return increments;
	}

	public void setIncrements(int increments) {
		this.increments = increments;
	}

	public int getPerjob_upper() {
		return perjob_upper;
	}

	public void setPerjob_upper(int perjob_upper) {
		this.perjob_upper = perjob_upper;
	}

	public int getPerjob_lower() {
		return perjob_lower;
	}

	public void setPerjob_lower(int perjob_lower) {
		this.perjob_lower = perjob_lower;
	}

	public int getMin_jobnum() {
		return min_jobnum;
	}

	public void setMin_jobnum(int min_jobnum) {
		this.min_jobnum = min_jobnum;
	}

	public double getInformtimeratio() {
		return informtimeratio;
	}

	public void setInformtimeratio(double informtimeratio) {
		this.informtimeratio = informtimeratio;
	}

	public double getFailureRate() {
		return failureRate;
	}

	public void setFailureRate(double failureRate) {
		this.failureRate = failureRate;
	}

	public double getDelayVariation() {
		return delayVariation;
	}

	public void setDelayVariation(double delayVariation) {
		this.delayVariation = delayVariation;
	}

	public String getFileName() {
		return getUniqueFileName();
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

}
