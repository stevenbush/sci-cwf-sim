package scws.core.experiment;

import static org.junit.Assert.*;

import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.distributions.ContinuousDistribution;
import org.cloudbus.cloudsim.distributions.LognormalDistr;
import org.junit.Test;

import scws.core.Cloud;
import scws.core.VMDescription;
import scws.core.VMInstance;
import scws.core.VMManger;
import scws.core.WorkflowEngine;
import scws.core.provisioner.SimpleUtilizationBasedProvisioner;
import scws.core.scheduler.DAGDynamicScheduler;

public class VMFactoryTest {

	@Test
	public void testCreateVM() {

		CloudSim.init(1, null, false);
		// To get mean m and stddev s, use:
		// sigma = sqrt(log(1+s^2/m^2))
		// mu = log(m)-0.5*log(1+s^2/m^2)
		// For mean = 60 and stddev = 10: mu = 4.080645 sigma = 0.1655264
		// For mean = 20 and stddev = 5: mu = 2.96542, sigma = 0.09975135

		ContinuousDistribution provisioningDelayDistribution = new LognormalDistr(new java.util.Random(0),
				4.080645, 0.1655264);
		ContinuousDistribution deprovisioningDelayDistribution = new LognormalDistr(new java.util.Random(0),
				2.96542, 0.09975135);

		VMFactory.setProvisioningDelayDistribution(provisioningDelayDistribution);
		VMFactory.setDeprovisioningDelayDistribution(deprovisioningDelayDistribution);

		VMDescription vmDescription = new VMDescription("testvm", 1000.0, 0.02);

		VMManger manage = new VMManger(vmDescription.vmname, vmDescription.price, vmDescription.mips, 1, 1.0,
				new SimpleUtilizationBasedProvisioner(0), new DAGDynamicScheduler(), new WorkflowEngine(
						new SimpleUtilizationBasedProvisioner(0.0), new DAGDynamicScheduler()), new Cloud());

		for (int i = 0; i < 1000; i++) {
			VMInstance vm = VMFactory.createVM(manage, vmDescription.mips, 1, 1.0, vmDescription.price);
			assertTrue(vm.getProvisioningDelay() > 0.0);
			assertEquals(vmDescription.price, vm.getPrice(), 0);
			assertEquals(vmDescription.mips, vm.getMips(), 0);
			System.out.println(vm.getProvisioningDelay());
		}
		for (int i = 0; i < 1000; i++) {
			VMInstance vm = VMFactory.createVM(manage, vmDescription.mips, 1, 1.0, vmDescription.price);
			assertTrue(vm.getDeprovisioningDelay() > 0.0);
			System.out.println(vm.getDeprovisioningDelay());
		}
	}

}
