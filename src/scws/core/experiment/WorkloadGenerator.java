package scws.core.experiment;

import java.util.ArrayList;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.distribution.UniformIntegerDistribution;
import org.apache.commons.math3.distribution.UniformRealDistribution;

/**
 * This class according different parameters to generate different workflow job
 * workload
 * 
 * @author jiyuanshi
 * @since 2013-4-14
 * @version sci-cwf-sim 1.0
 */
public class WorkloadGenerator {

	/** the total time span of this experiment */
	private double timespan = 48 * 3600;
	/** the average job number can be submitted every hour */
	private int averagejobnum = 120;
	/** the deviation of job number can be submitted every hour */
	private int job_deviation = 20;
	/** the increments of job number of growing workload */
	private int increments = 10;
	/** the upper bound of the job number */
	private int perjob_upper = 200;
	/** the lower bound of the job number */
	private int perjob_lower = 80;
	/** the minimum job number of growing workload */
	private int min_jobnum = 20;

	/**
	 * create a class instance by using the parameter
	 * 
	 * @param timespan
	 * @param averagejobnum
	 * @param job_deviation
	 * @param increments
	 * @param perjob_upper
	 * @param perjob_lower
	 * @param min_jobnum
	 */
	public WorkloadGenerator(Double timespan, int averagejobnum, int job_deviation, int increments, int perjob_upper, int perjob_lower,
			int min_jobnum) {

		this.timespan = timespan;
		this.averagejobnum = averagejobnum;
		this.job_deviation = job_deviation;
		this.increments = increments;
		this.perjob_upper = perjob_upper;
		this.perjob_lower = perjob_lower;
		this.min_jobnum = min_jobnum;
	}

	/**
	 * generate the onoff workload jobs' subtime in seconds
	 * 
	 * @return a array that store the jobs' subtime.
	 */
	public ArrayList<Double> Distribute_onoff() {

		ArrayList<Double> sub_times = new ArrayList<Double>();
		NormalDistribution normalDistribution = new NormalDistribution(averagejobnum, job_deviation);
		long job_num;
		double sub_time, upper, lower;

		for (int i = 0; i < timespan; i = i + 7200) {
			job_num = Math.round(normalDistribution.sample());
			System.out.println(i/3600 + "---" + job_num);

			lower = i;
			upper = i + 3600;

			for (int j = 0; j < job_num; j++) {

				sub_time = (new UniformRealDistribution(lower, upper).sample());
				sub_times.add(sub_time);
			}
		}

		return sub_times;
	}

	/**
	 * generate the growing workload jobs' subtime in seconds
	 * 
	 * @return a array that store the jobs' subtime.
	 */
	public ArrayList<Double> Distribute_growing() {

		ArrayList<Double> sub_times = new ArrayList<Double>();
		long job_num;
		double sub_time, upper, lower;

		for (int i = 0; i < timespan; i = i + 3600) {
			job_num = min_jobnum + (i/3600) * increments;
			System.out.println(i/3600 + "---" + job_num);

			lower = i;
			upper = i + 3600;

			for (int j = 0; j < job_num; j++) {
				sub_time = (new UniformRealDistribution(lower, upper).sample());
				sub_times.add(sub_time);
			}

		}

		return sub_times;
	}

	/**
	 * generate the bursting workload jobs' subtime in seconds
	 * 
	 * @return a array that store the jobs' subtime.
	 */
	public ArrayList<Double> Distribute_bursting() {

		ArrayList<Double> sub_times = new ArrayList<Double>();
		UniformIntegerDistribution distribution = new UniformIntegerDistribution(perjob_lower, perjob_upper);
		long job_num;
		double sub_time, upper, lower;

		for (int i = 0; i < timespan; i = i + 3600) {
			job_num = Math.round(distribution.sample());
			System.out.println(i/3600 + "---" + job_num);

			lower = i;
			upper = i + 3600;

			for (int j = 0; j < job_num; j++) {
				sub_time = (new UniformRealDistribution(lower, upper).sample());
				sub_times.add(sub_time);
			}

		}

		return sub_times;
	}

	/**
	 * generate the stable workload jobs' subtime in seconds
	 * 
	 * @return a array that store the jobs' subtime.
	 */
	public ArrayList<Double> Distribute_stable() {

		ArrayList<Double> sub_times = new ArrayList<Double>();
		NormalDistribution normalDistribution = new NormalDistribution(averagejobnum, job_deviation);
		long job_num;
		double sub_time, upper, lower;

		for (int i = 0; i < timespan; i = i + 3600) {
			job_num = Math.round(normalDistribution.sample());
			System.out.println(i/3600 + "---" + job_num);

			lower = i;
			upper = i + 3600;

			for (int j = 0; j < job_num; j++) {

				sub_time = (new UniformRealDistribution(lower, upper).sample());
				sub_times.add(sub_time);
			}

		}

		return sub_times;
	}

}
