package scws.core.experiment;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.cloudbus.cloudsim.Log;

/**
 * Class for storing the experiment results and storing them in a text format.
 * 
 * @author jiyuanshi <shijiyuan.seu@gmail.com>
 * @since 2013-4-28
 * @version sci-cwf-sim 1.0
 * 
 */
public class ExperimentResult {

	private String algorithm;
	private double cost;
	private double budget;
	private int numFreeVMs;
	private int numBusyVMs;
	private int numTotalVMs;
	private int numTotalDAGs;
	private int numFinishedDAGs;
	private double timespan;
	private List<Integer> priorities;
	private List<Double> sizes;
	private String scoreBitString;
	private double ExponentialScore;
	private double LinearScore;
	private double actualFinishTime;
	private double actualJobFinishTime;
	private long planningWallTime;
	private long simulationWallTime;
	private long initWallTime;

	public String getAlgorithm() {
		return algorithm;
	}

	public void setAlgorithm(String algorithm) {
		this.algorithm = algorithm;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public double getBudget() {
		return budget;
	}

	public void setBudget(double budget) {
		this.budget = budget;
	}

	public int getNumFreeVMs() {
		return numFreeVMs;
	}

	public void setNumFreeVMs(int numFreeVMs) {
		this.numFreeVMs = numFreeVMs;
	}

	public int getNumBusyVMs() {
		return numBusyVMs;
	}

	public void setNumBusyVMs(int numBusyVMs) {
		this.numBusyVMs = numBusyVMs;
	}

	public int getNumTotalVMs() {
		return numTotalVMs;
	}

	public void setNumTotalVMs(int numTotalVMs) {
		this.numTotalVMs = numTotalVMs;
	}

	public int getNumFinishedDAGs() {
		return numFinishedDAGs;
	}

	public void setNumFinishedDAGs(int numFinishedDAGs) {
		this.numFinishedDAGs = numFinishedDAGs;
	}

	public double getTimespan() {
		return timespan;
	}

	public void setTimespan(double timespan) {
		this.timespan = timespan;
	}

	public List<Integer> getPriorities() {
		return priorities;
	}

	public void setPriorities(List<Integer> priorities) {
		this.priorities = priorities;
	}

	public List<Double> getSizes() {
		return sizes;
	}

	public void setSizes(List<Double> sizes) {
		this.sizes = sizes;
	}

	public long getPlanningWallTime() {
		return planningWallTime;
	}

	public void setPlanningWallTime(long planningWallTime) {
		this.planningWallTime = planningWallTime;
	}

	public long getSimulationWallTime() {
		return simulationWallTime;
	}

	public void setSimulationWallTime(long simulationWallTime) {
		this.simulationWallTime = simulationWallTime;
	}

	public long getInitWallTime() {
		return initWallTime;
	}

	public void setInitWallTime(long initWallTime) {
		this.initWallTime = initWallTime;
	}

	public double getActualFinishTime() {
		return actualFinishTime;
	}

	public void setActualFinishTime(double actualFinishTime) {
		this.actualFinishTime = actualFinishTime;
	}

	public String getScoreBitString() {
		return scoreBitString;
	}

	public void setScoreBitString(String scoreBitString) {
		this.scoreBitString = scoreBitString;
	}

	public double getExponentialScore() {
		return ExponentialScore;
	}

	public void setExponentialScore(double exponentialScore) {
		ExponentialScore = exponentialScore;
	}

	public double getLinearScore() {
		return LinearScore;
	}

	public void setLinearScore(double linearScore) {
		LinearScore = linearScore;
	}

	/**
	 * Format the list of priorities of completed DAGs as a string
	 * 
	 * @return the string containing space separated priorities
	 */

	public String formatPriorities() {

		StringBuffer prioritiesBuffer = new StringBuffer();

		for (int priority : getPriorities()) {
			prioritiesBuffer.append(priority + " ");
		}

		// remove trailing space
		if (prioritiesBuffer.length() > 0)
			prioritiesBuffer.deleteCharAt(prioritiesBuffer.length() - 1);
		prioritiesBuffer.append("\n");

		return prioritiesBuffer.toString();
	}

	/**
	 * Format the list of sizes of completed DAGs as a string
	 * 
	 * @return the string containing space separated sizes
	 */

	public String formatSizes() {

		StringBuffer sizesBuffer = new StringBuffer();

		for (double size : getSizes()) {
			sizesBuffer.append(size + " ");
		}
		if (sizesBuffer.length() > 0)
			sizesBuffer.deleteCharAt(sizesBuffer.length() - 1);
		sizesBuffer.append("\n");

		return sizesBuffer.toString();
	}

	/**
	 * get the total sum of all the dag job size
	 * 
	 * @return
	 */
	public Double sumSize() {
		Double sum = 0.0;
		for (Double size : getSizes()) {
			sum = sum + size;
		}
		return sum;
	}

	public void WriteResult(String fileName) {

		try {
			BufferedWriter logwriter = new BufferedWriter(new FileWriter(fileName));

			logwriter.write("[result]\n");
			Log.print("algorithm=" + algorithm + "\n");
			logwriter.write("algorithm=" + algorithm + "\n");
			Log.print("budget=" + budget + "\n");
			logwriter.write("budget=" + budget + "\n");
			Log.print("timespan=" + timespan + "\n");
			logwriter.write("timespan=" + timespan + "\n");
			Log.print("cost=" + cost + "\n");
			logwriter.write("cost=" + cost + "\n");
			Log.print("total=" + numTotalDAGs + "\n");
			logwriter.write("total=" + numTotalDAGs + "\n");
			Log.print("finished=" + numFinishedDAGs + "\n");
			logwriter.write("finished=" + numFinishedDAGs + "\n");
			//Log.print("priorities=" + formatPriorities());
			//logwriter.write("priorities=" + formatPriorities());
			// result.append("sizes " + formatSizes());
			// Log.print("total size=" + sumSize() + "\n");
			// logwriter.write("total size=" + sumSize() + "\n");
			Log.print("actualFinishTime=" + actualFinishTime + "\n");
			logwriter.write("actualFinishTime=" + actualFinishTime + "\n");
			Log.print("actualJobFinishTime=" + actualJobFinishTime + "\n");
			logwriter.write("actualJobFinishTime=" + actualJobFinishTime + "\n");
			// result.append("scoreBitString " + scoreBitString + "\n");
			Log.print("ExponentialScore=" + ExponentialScore + "\n");
			logwriter.write("ExponentialScore=" + ExponentialScore + "\n");
			Log.print("LinearScore=" + LinearScore + "\n");
			logwriter.write("LinearScore=" + LinearScore + "\n");
			Log.print("initWallTime=" + initWallTime + "\n");
			logwriter.write("initWallTime=" + initWallTime + "\n");
			Log.print("planningWallTime=" + planningWallTime + "\n");
			logwriter.write("planningWallTime=" + planningWallTime + "\n");
			Log.print("simulationWallTime=" + simulationWallTime + "\n");
			logwriter.write("simulationWallTime=" + simulationWallTime + "\n");

			logwriter.flush();
			logwriter.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public int getNumTotalDAGs() {
		return numTotalDAGs;
	}

	public void setNumTotalDAGs(int numTotalDAGs) {
		this.numTotalDAGs = numTotalDAGs;
	}

	public double getActualJobFinishTime() {
		return actualJobFinishTime;
	}

	public void setActualJobFinishTime(double actualJobFinishTime) {
		this.actualJobFinishTime = actualJobFinishTime;
	}

}
