package scws.core;

import org.cloudbus.cloudsim.core.SimEvent;
import org.cloudbus.cloudsim.core.predicates.Predicate;

/**
 * A predicate to select events with specific job data belong to specific DAGJob.
 * 
 * @author jiyuanshi <shijiyuan.seu@gmail.com>
 * @since 20130517
 * @version sci-cwf-sim 2.5
 */
public class PredicateCancleDag extends Predicate {

	/** the data */
	private DAGJob dagJob;

	/**
	 * Constructor used to select events with the DAGJob <code>t1</code>.
	 * 
	 * @param dagJob
	 *            : the dagJob need to be seleted
	 */
	public PredicateCancleDag(DAGJob dagJob) {
		this.dagJob = dagJob;
	}

	/**
	 * The match function called by <code>Sim_system</code>, not used directly
	 * by the user.
	 * 
	 * @param ev
	 *            the ev
	 * 
	 * @return true, if match
	 */
	@Override
	public boolean match(SimEvent event) {
		if (event.getData() instanceof Job) {
			if (((Job)event.getData()).getDAGJob()==this.dagJob) {
				System.out.println("Match success!!!!");
				return true;
			}else {
				return false;
			}
		} else {
			return false;
		}
	}

}
