package scws.core.log;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.cloudbus.cloudsim.Log;

import scws.core.DAGJob;
import scws.core.DAGJobListener;
import scws.core.Job;
import scws.core.JobListener;
import scws.core.VMInstance;
import scws.core.VMInstanceListener;

/**
 * this is a utility class that can be used to output the log information
 * 
 * @author jiyuanshi <shijiyuan.seu@gmail.com>
 * @since 2013-4-20
 * @version sci-cwf-sim 1.0
 * 
 */
public class WorkflowLog implements JobListener, VMInstanceListener, DAGJobListener {

	//HashSet<Job> jobs = new HashSet<Job>();
	//HashSet<VMInstance> vms = new HashSet<VMInstance>();
	//Set<DAGJob> djs = new HashSet<DAGJob>();
	Double totalSpendTime = 0.0;

	public WorkflowLog() {
		this.totalSpendTime = 0.0;
	}

	@Override
	public void jobReleased(Job job) {
	}

	@Override
	public void jobSubmitted(Job job) {
	}

	@Override
	public void jobStarted(Job job) {
	}

	@Override
	public void jobFinished(Job job) {
		//jobs.add(job);
		totalSpendTime = totalSpendTime + job.getDuration();
	}

	@Override
	public void vminstanceLaunched(VMInstance vm) {
		//vms.add(vm);
	}

	@Override
	public void vminstanceTerminated(VMInstance vm) {
	}

	@Override
	public void dagStarted(DAGJob dagJob) {
	}

	@Override
	public void dagFinished(DAGJob dagJob) {
		//djs.add(dagJob);
	}

	public void printJobs(String fileName) {

		// try {
		// BufferedWriter logwriter = new BufferedWriter(new FileWriter(fileName
		// + "-jobs.txt"));
		//
		// String indent = "    ";
		// logwriter.newLine();
		// logwriter.write("========== OUTPUT ==========\n");
		// logwriter.write("Total job spend time: " + this.totalSpendTime +
		// "\n");
		// logwriter.write("Job ID" + indent + "STATUS" + indent + "Priority" +
		// indent + "VM ID" + indent + "Sub-Deadline" + indent
		// + "Job Size" + indent + "Time" + indent + "Start Time" + indent +
		// "Finish Time\n");
		//
		// DecimalFormat dft = new DecimalFormat("###.##");
		// for (Job job : jobs) {
		// logwriter.write(indent + job.getID() + indent + indent);
		//
		// if (job.getState() == Job.State.TERMINATED && job.getResult() ==
		// Job.Result.SUCCESS) {
		// logwriter.write("SUCCESS");
		//
		// logwriter.write(indent + indent + job.getDAGJob().getPriority() +
		// indent + indent + indent
		// + job.getVminstance().getVmID() + indent + indent +
		// job.getSubdeadline() + indent + indent + job.getSize()
		// + indent + indent + dft.format(job.getDuration()) + indent + indent +
		// dft.format(job.getStartTime()) + indent
		// + indent + dft.format(job.getFinishTime()) + "\n");
		// } else {
		// logwriter.write("FAILED");
		// logwriter.write(indent + indent + job.getDAGJob().getPriority() +
		// indent + indent + indent
		// + job.getVminstance().getVmID() + indent + indent +
		// job.getSubdeadline() + indent + indent + job.getSize()
		// + indent + indent + dft.format(job.getDuration()) + indent + indent +
		// dft.format(job.getStartTime()) + indent
		// + indent + dft.format(job.getFinishTime()) + "\n");
		// }
		// }
		// logwriter.flush();
		// logwriter.close();
		//
		// } catch (IOException e) {
		// e.printStackTrace();
		// }

	}

	public void printVmList(String fileName) {

		// try {
		// BufferedWriter logwriter = new BufferedWriter(new FileWriter(fileName
		// + "-vms.txt"));
		//
		// String indent = "    ";
		// logwriter.newLine();
		// logwriter.write("========== VMs ==========\n");
		// logwriter.write("VM ID" + indent + "Creation Time" + indent +
		// "Destroy Time" + indent + "usage record\n");
		//
		// DecimalFormat dft = new DecimalFormat("###.##");
		//
		// for (VMInstance vm : vms) {
		// logwriter.write(indent + vm.getVmID() + indent + indent);
		//
		// logwriter.write(indent + indent + dft.format(vm.getLaunchTime()) +
		// indent + indent + dft.format(vm.getTerminateTime())
		// + indent);
		//
		// for (Job job : vm.getAllJobs()) {
		// logwriter.write(job.getDAGJob().getDagname() + job.getID() + ": " +
		// job.getStartTime() + "~" + job.getFinishTime()
		// + indent);
		// }
		// logwriter.newLine();
		// }
		// logwriter.flush();
		// logwriter.close();
		//
		// } catch (IOException e) {
		// e.printStackTrace();
		// }

	}

	public void printDAGJobs(String fileName) {
		// try {
		//
		// BufferedWriter logwriter = new BufferedWriter(new
		// FileWriter(fileName+"-dags.txt"));
		//
		// String indent = "    ";
		// Log.printLine("");
		// logwriter.newLine();
		// Log.printLine("========== DAGJobs ==========");
		// logwriter.write("========== DAGJobs ==========\n");
		// Log.printLine("Priority" + indent + "Finished");
		// logwriter.write("Priority" + indent + "Finished\n");
		//
		// int finished = 0;
		// for (DAGJob dj : djs) {
		// Log.print(indent + dj.getPriority() + indent + indent);
		// logwriter.write(indent + dj.getPriority() + indent + indent);
		// Log.print(indent + dj.isFinished() + indent + indent);
		// logwriter.write(indent + dj.isFinished() + indent + indent);
		// Log.printLine("");
		// logwriter.newLine();
		// if (dj.isFinished())
		// finished++;
		// }
		// Log.printLine("Completed DAGs: " + finished);
		// logwriter.write("Completed DAGs: " + finished+"\n");
		//
		// logwriter.flush();
		// logwriter.close();
		//
		// } catch (IOException e) {
		// e.printStackTrace();
		// }

	}
	// public static void stringToFile(String s, String fileName) {
	// BufferedWriter out = null;
	// try {
	// out = new BufferedWriter(new FileWriter(fileName));
	// out.write(s);
	// } catch (IOException e) {
	// throw new RuntimeException(e);
	// } finally {
	// IOUtils.closeQuietly(out);
	// }
	// }

}
