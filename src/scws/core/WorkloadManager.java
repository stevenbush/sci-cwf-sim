package scws.core;

import java.util.ArrayList;
import java.util.LinkedList;

import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.core.SimEntity;
import org.cloudbus.cloudsim.core.SimEvent;

/**
 * This class manages a collection of DAGs workload and according to their
 * preset submit time to submits them to a WorkflowEngine for execution in
 * priority order according to a scheduling algorithm.
 * 
 * @author jiyuanshi <shijiyuan.seu@gmail.com>
 * @since 2013-4-16
 * @version sci-cwf-sim 1.0
 */
public class WorkloadManager extends SimEntity implements WorkflowEvent {
	

	/** List of all DAGs remaining to be executed */
	private LinkedList<DAGDescription> dags;

	/** DAG listeners */
	private LinkedList<DAGJobListener> listeners;

	/** Workflow engine that will receive DAGs for execution */
	private WorkflowEngine engine = null;

	/**
	 * construct the class by these specific parameters
	 * 
	 * @param dags
	 *            : all the workload jobs' DAG
	 * @param dagsinformation
	 *            : all the dag jobs' information, include the submit time,
	 *            deadline, priority.
	 * @param engine
	 *            : the workflow enging that will arrange these jobs to be
	 *            executed.
	 */
	public WorkloadManager(ArrayList<DAGDescription> dags, WorkflowEngine engine) {
		super("WorkloadManager");
		this.engine = engine;
		this.dags = new LinkedList<DAGDescription>();
		this.listeners = new LinkedList<DAGJobListener>();
		queueDAGs(dags);
		CloudSim.addEntity(this);
	}

	public WorkloadManager(WorkflowEngine engine) {
		this(null, engine);
	}

	/**
	 * Construct the DAGjob from the dags and dagsinformation
	 * @param dags: array store the dags
	 * @param dagsinformation: array store the daginformation
	 */
	private void queueDAGs(ArrayList<DAGDescription> dags) {
		if (dags == null)
			return;

		// put the input dags into the queue to be submit
		for (DAGDescription dagDescription : dags) {
			this.dags.add(dagDescription);
		}
		
	}
	
	public void addDAGJobListener(DAGJobListener l) {
        this.listeners.add(l);
    }

    public void removeDAGJobListener(DAGJobListener l) {
        this.listeners.remove(l);
    }

	@Override
	public void startEntity() {
		// according the submit time of DAGJobs to submit all the jobs.
        while (!dags.isEmpty()) {
            submitDAG(dags.pop());
        }
        Log.printLine("start "+getName()+" ID="+getId());
	}

	@Override
	public void processEvent(SimEvent ev) {
		switch (ev.getTag()) {
        case DAG_STARTED:
            dagStarted((DAGJob) ev.getData());
            break;
        case DAG_FINISHED:
            dagFinished((DAGJob) ev.getData());
            break;
        default:
            throw new RuntimeException("Unknown event: " + ev);
        }

	}

	@Override
	public void shutdownEntity() {
		// TODO Auto-generated method stub

	}
	
	/**
	 * according the submit time to submit the job to the workflow engine.
	 * @param dagJob
	 */
	public void submitDAG(DAGDescription dagJob) {
        // Submit the dag to the workflow engine
		Double timedelay = dagJob.subtime;
		send(engine.getId(), timedelay, DAG_SUBMIT, dagJob);
    }

	/**
	 * inform all the listener that this dag jobs have been started.
	 * @param dag
	 */
    private void dagStarted(DAGJob dag) {
        // Notify all listeners
    	Log.printLine("CloudSim Clock: " + CloudSim.clock());
		Log.printLine(getName() + ": a DAGJob started: " + dag.getDagname());
		Log.printLine("dagjob owner: " + dag.getOwner());
		Log.printLine(dag.getSubtime()+"-"+dag.getDeadline()+"-"+dag.getPriority());
		
        for (DAGJobListener l : listeners) {
            l.dagStarted(dag);
        }
    }

    /**
     * inform all the listener that this dag jobs have been finished.
     * @param dag
     */
    private void dagFinished(DAGJob dag) {
        // Notify all listeners
        for (DAGJobListener l : listeners) {
            l.dagFinished(dag);
        }
        // Remove the DAG
        dags.remove(dag);
    }

}
