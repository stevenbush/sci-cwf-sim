package scws.core;

import scws.core.VMInstance;

public interface VMInstanceListener {
	public void vminstanceLaunched(VMInstance vm);

    public void vminstanceTerminated(VMInstance vm);
}
