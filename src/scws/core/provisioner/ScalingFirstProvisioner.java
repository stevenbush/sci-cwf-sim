package scws.core.provisioner;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.core.CloudSim;

import scws.core.Job;
import scws.core.Provisioner;
import scws.core.VMInstance;
import scws.core.VMManger;
import scws.core.WorkflowEngine;
import scws.core.WorkflowEvent;
import scws.core.experiment.VMFactory;

/**
 * This is a Scaling First Based Provision class which implement the resource
 * provision mechanism that used in paper:
 * "Scaling and Scheduling to Maximize Application Performance within Budget Constraints in Cloud Workflows"
 * 
 * @author jiyuanshi <shijiyuan.seu@gmail.com>
 * @since 2013-4-19
 * @version sci-cwf-sim 1.0
 * 
 */
public class ScalingFirstProvisioner extends AbstractProvisioner implements Provisioner, WorkflowEvent {

	public ScalingFirstProvisioner() {
		// TODO Auto-generated constructor stub
	}

	public ScalingFirstProvisioner(double maxScaling) {
		super(maxScaling);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void provisionResources(VMManger vmManger) {
		// DO NOTHING
	}

	@Override
	public void provisionResources(VMManger vmManger, VMInstance vmInstance) {
		// finding the free vm instance that is close to the full hour of
		// operation and terminated it.
		double timespan = vmManger.getWorkflowengine().getTimespan();
		double time = CloudSim.clock();
		if (time < timespan) {
			if (vmManger.getFreeVMs().contains(vmInstance)) {
				HashSet<VMInstance> toTerminate = new HashSet<VMInstance>();
				toTerminate.add(vmInstance);
				terminateInstances(toTerminate);
			}
		} else {
			Set<VMInstance> runningVMs = new HashSet<VMInstance>(vmManger.getFreeVMs());
			runningVMs.addAll(vmManger.getBusyVMs());
			HashSet<VMInstance> toTerminate = new HashSet<VMInstance>();
			toTerminate.addAll(runningVMs);
			terminateInstances(toTerminate);
		}

	}

	@Override
	public void provisionResources(WorkflowEngine engine) {
		double timespan = engine.getTimespan();
		double time = CloudSim.clock();
		if (time < timespan) {
			Log.printLine("Start provision resource process....");
			// the VM instance need to be created
			ArrayList<VMManger> toCreate = new ArrayList<VMManger>();
			// get the remaining budget of this engine
			Double remainingBudget = engine.getBudget() - engine.getCost();
			// averagely assigning the remaining budget to the left hours
			Double budget = remainingBudget / (Math.ceil((engine.getTimespan() - time) / 3600));
			// the cost used for the still running vm instances
			Double usedbudget = 0.0;
			for (VMManger vmManger : engine.getVmMangers()) {
				usedbudget = usedbudget + vmManger.getAvailableVMs().size() * vmManger.getPrice();
			}

			Double maxPowerDouble = Double.MIN_VALUE;
			Double maxPriceDouble = Double.MIN_VALUE;
			Integer maxTypeIndex = 0;
			// Find the fast VM type information include computing power, price
			// and
			// index
			for (VMManger vmManger : engine.getVmMangers()) {
				if (vmManger.getMips() > maxPowerDouble) {
					maxPowerDouble = vmManger.getMips();
					maxPriceDouble = vmManger.getPrice();
					maxTypeIndex = engine.getVmMangers().indexOf(vmManger);
				}
			}

			Double availablebudget = budget - usedbudget;
			if (availablebudget >= 0) {
				ArrayList<Job> waitingJobs = new ArrayList<Job>();
				Double duration = 0.0;
				waitingJobs.addAll(engine.getQueuedJobs());
				Double capableConsumption = 0.0;
				if (waitingJobs.size() > 0) {
					for (Job job : waitingJobs) {
						duration = duration + job.getSize() / maxPowerDouble;
					}
					Double fastConsumption = Math.ceil(duration / 3600.0);
					Double fastCost = fastConsumption * maxPriceDouble;
					capableConsumption = Math.floor(fastConsumption * (availablebudget / fastCost));
				} else {
					capableConsumption = Math.floor(availablebudget / maxPriceDouble);
				}

				// add the capable consumption into the create array
				for (int i = 0; i < capableConsumption; i++) {
					toCreate.add(engine.getVmMangers().get(maxTypeIndex));
				}

				// Consolidate budget, use the left budget to request other type
				// of
				// vm
				Double leftbudget = availablebudget - capableConsumption * maxPriceDouble;
				while (leftbudget > 0) {
					for (VMManger vmManger : engine.getVmMangers()) {
						leftbudget = leftbudget - vmManger.getPrice();
						if (leftbudget <= 0) {
							break;
						}
						toCreate.add(vmManger);
					}
				}

				// create the new VM instance for the next
				for (VMManger vmManger : toCreate) {
					// VMFactory.setDeprovisioningDelayDistribution(vmManger
					// .getDeprovisioningDelayDistribution());
					// VMFactory.setFailureModel(vmManger.getFailureModel());
					// VMFactory.setProvisioningDelayDistribution(vmManger.getProvisioningDelayDistribution());
					// VMFactory.setRuntimeDistribution(vmManger.getRuntimeDistribution());
					VMInstance vm = VMFactory.createVM(vmManger, vmManger.getMips(), vmManger.getCores(),
							vmManger.getBandwidth(), vmManger.getPrice());
					Log.printLine(CloudSim.clock() + " Starting VM: " + vm.getVmID());
					CloudSim.send(vmManger.getId(), cloud.getId(), 0.0, VM_LAUNCH, vm);
				}

			} else {
				// select Vms to terminate
				HashSet<VMInstance> toTerminate = new HashSet<VMInstance>();
				Double costDouble = usedbudget - budget;

				for (VMManger vmManger : engine.getVmMangers()) {
					if (costDouble <= 0) {
						break;
					} else {
						for (VMInstance vmInstance : vmManger.getAvailableVMs()) {
							costDouble = costDouble - vmManger.getPrice();
							toTerminate.add(vmInstance);
							if (costDouble <= 0) {
								break;
							}
						}
					}
				}
				terminateInstances(toTerminate);

			}

			// send event to initiate next provisioning cycle
			CloudSim.send(engine.getId(), engine.getId(), PROVISIONER_INTERVAL, PROVISIONING_REQUEST, null);
		} else {
			HashSet<VMInstance> toTerminate = new HashSet<VMInstance>();
			toTerminate.addAll(cloud.getVms());
			terminateInstances(toTerminate);
		}

	}

	/**
	 * This method terminates instances no matter whether it is close to the
	 * full hour of operation. The method modifies the given vmSet by removing
	 * the terminated Vms.
	 * 
	 * @param vmSet
	 */
	private void terminateInstances(Set<VMInstance> vmSet) {

		Iterator<VMInstance> vmIt = vmSet.iterator();

		while (vmIt.hasNext()) {
			VMInstance vm = vmIt.next();
			vmIt.remove();
			Log.printLine(CloudSim.clock() + " Terminating VM: " + vm.getVmID());
			if (vm.getManager().getFreeVMs().contains(vm)) {
				vm.getManager().getFreeVMs().remove(vm);
			}
			if (vm.getManager().getBusyVMs().contains(vm)) {
				vm.getManager().getBusyVMs().remove(vm);
			}
			CloudSim.send(vm.getManager().getId(), cloud.getId(), 0.0, VM_TERMINATE, vm);
		}

	}
	
}
