package scws.core.provisioner;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.core.CloudSim;

import scws.core.Provisioner;
import scws.core.VMInstance;
import scws.core.VMManger;
import scws.core.WorkflowEngine;
import scws.core.WorkflowEvent;
import scws.core.experiment.VMFactory;

/**
 * This is a specific Simple Utilization Based Provision class which implement
 * the resource provision mechanism that used in the DPDS and WA-DPDS algorithm.
 * 
 * @author jiyuanshi <shijiyuan.seu@gmail.com>
 * @since 2013-4-19
 * @version sci-cwf-sim 1.0
 * 
 */
public class SimpleUtilizationBasedProvisioner extends AbstractProvisioner implements Provisioner,
		WorkflowEvent {
	// above this utilization threshold we start provisioning additional VMs
	private static final double UPPER_THRESHOLD = 0.90;
	// below this utilization threshold we start deprovisioning vms
	private static final double LOWER_THRESHOLD = 0.70;

	// conservative estimate of deprovisioning delay
	// private static final double DEPROVISIONING_DELAY_ESTIMATE = 25.0;
	// optimistic estimate of deprovisioning delay
	private static final double DEPROVISIONING_DELAY_ESTIMATE = 10;
	/** The default provisioning delay */
	private static final double DEFAULT_PROVISIONING_DELAY = 60.0;

	// number of initially provisioned VMs to be used for setting limits for
	// autoscaling
	private int initialNumVMs = 0;

	public SimpleUtilizationBasedProvisioner(double maxScaling) {
		super(maxScaling);
	}

	@Override
	public void provisionResources(VMManger vmManger, VMInstance vmInstance) {
		// Log.printLine("Last PO time:" + vmManger.getLastPOTime() +
		// "~current time:" + CloudSim.clock());
		if (vmManger.getLastPOTime() != CloudSim.clock()) {
			vmManger.setLastPOTime(CloudSim.clock());
			vmManger.getWorkflowengine().setRemainingBudget(
					vmManger.getWorkflowengine().getBudget() - vmManger.getWorkflowengine().getCost());
			vmManger.setVmNumber(vmManger.getFreeVMs().size() + vmManger.getBusyVMs().size());
		}
		// Log.printLine("SimpleUtilization provision decision~remaining budget:"
		// + vmManger.getWorkflowengine().getRemainingBudget() + "~budget"
		// + (vmManger.getWorkflowengine().getBudget() -
		// vmManger.getWorkflowengine().getCost()));

		Double remainingBudget = vmManger.getWorkflowengine().getRemainingBudget();

		// double budget = vmManger.getWorkflowengine().getBudget();
		// double cost = vmManger.getWorkflowengine().getCost();
		// Log.printLine(CloudSim.clock() + " Provisioner: Budget consumed " +
		// cost);
		// Log.printLine(CloudSim.clock() + " Provisioner: Remaining Budget " +
		// (budget - cost));

		if (remainingBudget < vmManger.getPrice()) {
			HashSet<VMInstance> toTerminate = new HashSet<VMInstance>();
			toTerminate.add(vmInstance);
			terminateInstances(vmManger, toTerminate);
			Integer currentVMNum = vmManger.getVmNumber();
			vmManger.setVmNumber(currentVMNum - toTerminate.size());
		} else {
			this.provisionResources(vmManger);
		}
	}

	@Override
	public void provisionResources(WorkflowEngine workflowEngine) {
		// DO nothing
	}

	@Override
	public void provisionResources(VMManger vmManger) {
		// this process is design to handle the several provision operation in
		// one simulation clock
		// Log.printLine("Last PO time:" + vmManger.getLastPOTime() +
		// "~current time:" + CloudSim.clock());
		if (vmManger.getLastPOTime() != CloudSim.clock()) {
			vmManger.setLastPOTime(CloudSim.clock());
			vmManger.getWorkflowengine().setRemainingBudget(
					vmManger.getWorkflowengine().getBudget() - vmManger.getWorkflowengine().getCost());
			vmManger.setVmNumber(vmManger.getFreeVMs().size() + vmManger.getBusyVMs().size());
		}
		// Log.printLine("SimpleUtilization provision decision~remaining budget:"
		// + vmManger.getWorkflowengine().getRemainingBudget() + "~budget"
		// + (vmManger.getWorkflowengine().getBudget() -
		// vmManger.getWorkflowengine().getCost()));
		// Log.printLine("Budget:" + vmManger.getWorkflowengine().getBudget() +
		// "~cost:"
		// + vmManger.getWorkflowengine().getCost());

		// before the provision we should schedule the waiting job into the
		// available vm instance first, in case that these vm instance are
		// terminated.
		vmManger.getScheduler().scheduleJobToVMInstance(vmManger);

		// when called for the first time it should obtain the initial number of
		// VMs
		if (initialNumVMs == 0)
			initialNumVMs = vmManger.getAvailableVMs().size();

		// check the deadline and budget constraints
		// double budget = vmManger.getWorkflowengine().getBudget();
		double timespan = vmManger.getWorkflowengine().getTimespan();
		double time = CloudSim.clock();
		// double cost = vmManger.getWorkflowengine().getCost();
		Double remainingBudget = vmManger.getWorkflowengine().getRemainingBudget();

		// Log.printLine(CloudSim.clock() + " Provisioner: Budget consumed " +
		// cost);
		// Log.printLine(CloudSim.clock() + " Provisioner: Remaining Budget " +
		// (budget - cost));

		// running vms are free + busy
		Set<VMInstance> runningVMs = new HashSet<VMInstance>(vmManger.getFreeVMs());
		runningVMs.addAll(vmManger.getBusyVMs());

		int numVMsRunning = runningVMs.size();

		// find VMs that will complete their billing hour
		// during the next provisioning cycle
		Set<VMInstance> completingVMs = new HashSet<VMInstance>();

		for (VMInstance vm : runningVMs) {
			double vmRuntime = vm.getRuntime();

			// full hours (rounded up)
			double vmHours = Math.ceil(vmRuntime / 3600.0);

			// seconds till next full hour
			double secondsRemaining = vmHours * 3600.0 - vmRuntime;

			// we add delay estimate to include also the deprovisioning time
			if (secondsRemaining <= (DEFAULT_PROVISIONING_DELAY + DEPROVISIONING_DELAY_ESTIMATE)) {
				completingVMs.add(vm);
			}
		}

		int numVMsCompleting = completingVMs.size();

		// Log.printLine(CloudSim.clock() +
		// " Provisioner: number of instances : " + numVMsRunning);
		// Log.printLine(CloudSim.clock() +
		// " Provisioner: number of instances completing: " + numVMsCompleting);

		// get the price of this kind of vm
		double vmPrice = vmManger.getPrice();

		// if we are close to the budget
		if (remainingBudget < vmPrice * numVMsCompleting || time > timespan) {
			// compute number of vms to terminate
			// it is the number that would overrun the budget if not terminated
			int numToTerminate = numVMsRunning - (int) Math.floor(((remainingBudget) / vmPrice));

			// get the VM Instance that will be terminated
			// set of vms scheduled for termination
			Set<VMInstance> toTerminate;

			if (time > timespan) {
				toTerminate = runningVMs;
			} else {
				toTerminate = new HashSet<VMInstance>();
				// select VMs to terminate
				if (numToTerminate < numVMsCompleting) {
					// select only from completing vms
					Iterator<VMInstance> completingIt = completingVMs.iterator();
					for (int i = 0; i < numToTerminate; i++) {
						VMInstance vm;
						vm = completingIt.next();
						toTerminate.add(vm);
					}
				} else {
					// terminate all completing and add more from free and busy
					// ones
					toTerminate.addAll(completingVMs);
				}
			}

			// Log.printLine(CloudSim.clock() +
			// " Provisioner: number of instances to terminate: "
			// + toTerminate.size());

			// start terminating vms
			terminateInstances(vmManger, toTerminate);
			Integer currentVMNum = vmManger.getVmNumber();
			vmManger.setVmNumber(currentVMNum - toTerminate.size());
			vmManger.getWorkflowengine().setRemainingBudget(
					remainingBudget - (numVMsCompleting - toTerminate.size()) * vmManger.getPrice());

		} else {
			// compute utilization
			double numFreeVMS = vmManger.getFreeVMs().size();
			double numBusyVMs = vmManger.getBusyVMs().size();
			if ((numFreeVMS + numBusyVMs) != 0) {

				double utilization = numBusyVMs / (numFreeVMS + numBusyVMs);

				// Log.printLine("numFreeVMS: " + numFreeVMS + "~numBusyVMs: " +
				// numBusyVMs);
				// Log.printLine(CloudSim.clock() +
				// " Provisioner: utilization: " + utilization);

				// if we are close to constraints we should not provision new
				// vms
				boolean finishing_phase = remainingBudget <= vmPrice * numVMsRunning || time > timespan;

				// if:
				// we are not in finishing phase,
				// and utilization is high
				// and we are below max limit
				// and we have money left for one instance more
				// then: deploy new instance

				if (!finishing_phase && utilization > UPPER_THRESHOLD
						&& numBusyVMs + numFreeVMS < getMaxScaling() * initialNumVMs
						&& remainingBudget >= vmPrice) {

					// VMFactory.setDeprovisioningDelayDistribution(vmManger
					// .getDeprovisioningDelayDistribution());
					// VMFactory.setFailureModel(vmManger.getFailureModel());
					// VMFactory.setProvisioningDelayDistribution(vmManger.getProvisioningDelayDistribution());
					// VMFactory.setRuntimeDistribution(vmManger.getRuntimeDistribution());
					VMInstance vm = VMFactory.createVM(vmManger, vmManger.getMips(), vmManger.getCores(),
							vmManger.getBandwidth(), vmManger.getPrice());
					// Log.printLine(CloudSim.clock() + " Starting VM: " +
					// vm.getVmID());
					CloudSim.send(vmManger.getId(), cloud.getId(), 0.0, VM_LAUNCH, vm);

					remainingBudget = remainingBudget - vmManger.getPrice();
					vmManger.getWorkflowengine().setRemainingBudget(remainingBudget);
					Integer currentVMs = vmManger.getVmNumber() + 1;
					vmManger.setVmNumber(currentVMs);

				} else if (!finishing_phase && utilization < LOWER_THRESHOLD) {

					// select Vms to terminate
					HashSet<VMInstance> toTerminate = new HashSet<VMInstance>();

					// terminate half of the instances
					// make sure that if there is only one instance it should be
					// terminated
					int numToTerminate = (int) Math.ceil(vmManger.getFreeVMs().size() / 2.0);
					Iterator<VMInstance> vmIt = vmManger.getFreeVMs().iterator();
					for (int i = 0; i < numToTerminate && vmIt.hasNext(); i++) {
						toTerminate.add(vmIt.next());
					}

					terminateInstances(vmManger, toTerminate);
					Integer currentVMNum = vmManger.getVmNumber();
					vmManger.setVmNumber(currentVMNum - toTerminate.size());

				}

			}

		}

	}

	/**
	 * This method terminates instances but only the ones that are close to the
	 * full hour of operation.. The method modifies the given vmSet by removing
	 * the terminated Vms.
	 * 
	 * @param vmManger
	 * @param vmSet
	 */
	private void terminateInstances(VMManger vmManger, Set<VMInstance> vmSet) {

		Iterator<VMInstance> vmIt = vmSet.iterator();

		while (vmIt.hasNext()) {
			VMInstance vm = vmIt.next();
			double vmRuntime = vm.getRuntime();

			// full hours (rounded up)
			double vmHours = Math.ceil(vmRuntime / 3600.0);

			// seconds till next full hour
			double secondsRemaining = vmHours * 3600.0 - vmRuntime;

			// terminate only vms that have less seconds remaining than a
			// defined threshold
			if (secondsRemaining < DEFAULT_PROVISIONING_DELAY + DEPROVISIONING_DELAY_ESTIMATE) {
				vmIt.remove();
				// Log.printLine(CloudSim.clock() + " Terminating VM: " +
				// vm.getVmID());
				if (vmManger.getFreeVMs().contains(vm)) {
					vmManger.getFreeVMs().remove(vm);
				}
				if (vmManger.getBusyVMs().contains(vm)) {
					vmManger.getBusyVMs().remove(vm);
				}
				CloudSim.send(vmManger.getId(), cloud.getId(), 0.0, VM_TERMINATE, vm);
			}
		}

	}

}
