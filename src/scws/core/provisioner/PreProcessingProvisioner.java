package scws.core.provisioner;

import java.util.ArrayList;

import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.core.CloudSim;

import scws.core.Job;
import scws.core.Provisioner;
import scws.core.VMInstance;
import scws.core.VMManger;
import scws.core.WorkflowEngine;
import scws.core.WorkflowEvent;
import scws.core.experiment.VMFactory;

/**
 * this class implement the PreProcessing resource provision method which can
 * according the occupancy of vm instance to decide if request new vm instance
 * 
 * @author jiyuanshi <shijiyuan.seu@gmail.com>
 * @since 2013-05-03
 * @version sci-cwf-sim 1.0
 * 
 */
public class PreProcessingProvisioner extends AbstractProvisioner implements Provisioner, WorkflowEvent {

	/**
	 * according the current VM instance usage status to do the resource
	 * provision operation. In this operation we need to consider the VM
	 * instance provision delay lag
	 */
	@Override
	public void provisionResources(VMManger vmManger) {
		if (!vmManger.getJobPriorityQueue().isEmpty()) {
			// this process is design to handle the several provision operation
			// in
			// one simulation clock
			// Log.printLine("Last PO time:" + vmManger.getLastPOTime() +
			// "~current time:" + CloudSim.clock());
			if (vmManger.getLastPOTime() != CloudSim.clock()) {
				vmManger.setLastPOTime(CloudSim.clock());
				vmManger.getWorkflowengine().setRemainingBudget(
						vmManger.getWorkflowengine().getBudget() - vmManger.getWorkflowengine().getCost());
				vmManger.setVmNumber(vmManger.getFreeVMs().size() + vmManger.getBusyVMs().size());
			}
			// Log.printLine("decide if request new VM instance~remaining budget:"
			// + vmManger.getWorkflowengine().getRemainingBudget() + "~budget"
			// + (vmManger.getWorkflowengine().getBudget() -
			// vmManger.getWorkflowengine().getCost()));

			// get the remaining budget of this engine
			Double remainingBudget = vmManger.getWorkflowengine().getRemainingBudget();
			//Double delayLag = vmManger.getProvisioningDelayDistribution().sample();
			Double currentTime = CloudSim.clock();
			Double earlyestDeadline = Double.MAX_VALUE;
			for (Job job : vmManger.getJobPriorityQueue()) {
				if (job.getSubdeadline() < earlyestDeadline && job.getSubdeadline() > currentTime) {
					earlyestDeadline = job.getSubdeadline();
				}
			}

			if (remainingBudget >= vmManger.getPrice()) {
				// calculate and record the vm occupy of every vm instance
				ArrayList<Double> VMOs = new ArrayList<Double>();
				// get the vmo of all the waiting jobs
				for (Job job : vmManger.getJobPriorityQueue()) {
					Double vmo = 1.0;
					if (job.getSubdeadline() <= currentTime) {
						vmo = 1.0;
					} else {
						vmo = (job.getSize() / vmManger.getMips())
								/ (job.getSubdeadline() - currentTime);
						if (vmo > 1.0) {
							vmo = 1.0;
						}
					}
					VMOs.add(vmo);
				}
				// get the vmo of all the running jobs
				for (VMInstance vmInstance : vmManger.getBusyVMs()) {
					for (Job job : vmInstance.getRunningJobs()) {
						Double vmo = 1.0;
						Double expectCompleteTime = job.getStartTime() + job.getSize() / vmManger.getMips();
						// if we found some exception situation we just assume
						// that
						// this
						// job will occupy this VM instance for a long time
						if (expectCompleteTime <= currentTime || expectCompleteTime >= earlyestDeadline) {
							vmo = 1.0;
						} else {
							vmo = (expectCompleteTime - currentTime) / (earlyestDeadline - currentTime);
						}
						VMOs.add(vmo);
					}
				}

				// calculate the estimated VM instance number
				Double instanceNum = 0.0;
				for (Double vmo : VMOs) {
					instanceNum = instanceNum + vmo;
				}
				instanceNum = Math.ceil(instanceNum);
				// get current VM instance numbers
				Integer currentVMNum = vmManger.getVmNumber();

				// Log.printLine("currentVMNum:" + currentVMNum +
				// "~neededVMNum:" + instanceNum);
				// if the current VM instance number is not enough we need to
				if (instanceNum > currentVMNum) {
					for (int i = 0; i < instanceNum - currentVMNum; i++) {
						if (remainingBudget > vmManger.getPrice()) {
							// VMFactory.setDeprovisioningDelayDistribution(vmManger
							// .getDeprovisioningDelayDistribution());
							// VMFactory.setFailureModel(vmManger.getFailureModel());
							// VMFactory.setProvisioningDelayDistribution(vmManger
							// .getProvisioningDelayDistribution());
							// VMFactory.setRuntimeDistribution(vmManger.getRuntimeDistribution());
							VMInstance vm = VMFactory.createVM(vmManger, vmManger.getMips(),
									vmManger.getCores(), vmManger.getBandwidth(), vmManger.getPrice());
							// Log.printLine(CloudSim.clock() + " Starting VM: "
							// + vmManger.getName()
							// + vm.getVmID());
							CloudSim.send(vmManger.getId(), cloud.getId(), 0.0, VM_LAUNCH, vm);
							remainingBudget = remainingBudget - vmManger.getPrice();
							vmManger.getWorkflowengine().setRemainingBudget(remainingBudget);
							Integer currentVMs = vmManger.getVmNumber() + 1;
							vmManger.setVmNumber(currentVMs);
						}
					}
				}
			}

		}
	}

	/**
	 * according the current VM instance usage status to decide whether or not
	 * to terminate this VM instance. In this operation we do not need to
	 * consider the VM instance provision delay lag
	 */
	@Override
	public void provisionResources(VMManger vmManger, VMInstance vm) {
		// this process is design to handle the several provision
		// operation
		// in
		// one simulation clock
		//Log.printLine("Last PO time:" + vmManger.getLastPOTime() + "~current time:" + CloudSim.clock());
		if (vmManger.getLastPOTime() != CloudSim.clock()) {
			vmManger.setLastPOTime(CloudSim.clock());
			vmManger.getWorkflowengine().setRemainingBudget(
					vmManger.getWorkflowengine().getBudget() - vmManger.getWorkflowengine().getCost());
			vmManger.setVmNumber(vmManger.getFreeVMs().size() + vmManger.getBusyVMs().size());
		}
		// Log.printLine("decide if terminate VM instance" + vm.getVmID() +
		// "~remaining budget:"
		// + vmManger.getWorkflowengine().getRemainingBudget() + "~budget"
		// + (vmManger.getWorkflowengine().getBudget() -
		// vmManger.getWorkflowengine().getCost()));

		// get the remaining budget of this engine
		Double remainingBudget = vmManger.getWorkflowengine().getRemainingBudget();

		if (remainingBudget >= vmManger.getPrice()) {

			// if this vm instance is in free status and there are jobs waitiing
			// for scheduling
			if (!vmManger.getBusyVMs().contains(vm)) {

				// if no waiting jobs just terminate this vm instance
				if (!vmManger.getJobPriorityQueue().isEmpty()) {
					Double currentTime = CloudSim.clock();
					Double earlyestDeadline = Double.MAX_VALUE;
					for (Job job : vmManger.getJobPriorityQueue()) {
						if (job.getSubdeadline() < earlyestDeadline && job.getSubdeadline() > currentTime) {
							earlyestDeadline = job.getSubdeadline();
						}
					}

					// calculate and record the vm occupy of every vm instance
					ArrayList<Double> VMOs = new ArrayList<Double>();
					// get the vmo of all the waiting jobs
					for (Job job : vmManger.getJobPriorityQueue()) {
						Double vmo = 1.0;
						if (job.getSubdeadline() <= currentTime) {
							vmo = 1.0;
						} else {
							vmo = (job.getSize() / vmManger.getMips()) / (job.getSubdeadline() - currentTime);
							if (vmo > 1.0) {
								vmo = 1.0;
							}
						}
						VMOs.add(vmo);
					}
					// get the vmo of all the running jobs
					for (VMInstance vmInstance : vmManger.getBusyVMs()) {
						for (Job job : vmInstance.getRunningJobs()) {
							Double vmo = 1.0;
							Double expectCompleteTime = job.getStartTime() + job.getSize()
									/ vmManger.getMips();
							// if we found some exception situation we just
							// assume
							// that
							// this
							// job will occupy this VM instance for a long time
							if (expectCompleteTime <= currentTime || expectCompleteTime >= earlyestDeadline) {
								vmo = 1.0;
							} else {
								vmo = (expectCompleteTime - currentTime) / (earlyestDeadline - currentTime);
							}
							VMOs.add(vmo);
						}
					}

					// calculate the estimated VM instance number
					Double instanceNum = 0.0;
					for (Double vmo : VMOs) {
						instanceNum = instanceNum + vmo;
					}
					instanceNum = Math.ceil(instanceNum);
					// get current VM instance numbers
					Integer currentVMNum = vmManger.getVmNumber();

					// if the current VM instance number is enough we have to
					// terminate
					// this
					// VM instance
					if (instanceNum < currentVMNum) {
						terminateInstances(vmManger, vm);
						vmManger.setVmNumber(currentVMNum - 1);
					} else {
						vmManger.getWorkflowengine()
								.setRemainingBudget(remainingBudget - vmManger.getPrice());
					}
				} else {
					Integer currentVMNum = vmManger.getVmNumber();
					terminateInstances(vmManger, vm);
					vmManger.setVmNumber(currentVMNum - 1);
				}

			} else {
				vmManger.getWorkflowengine().setRemainingBudget(remainingBudget - vmManger.getPrice());
			}

		} else {
			Integer currentVMNum = vmManger.getVmNumber();
			terminateInstances(vmManger, vm);
			vmManger.setVmNumber(currentVMNum - 1);
		}
	}

	@Override
	public void provisionResources(WorkflowEngine workflowEngine) {
		// DO nothing

	}

	/**
	 * this method will terminate the specific VM instance
	 * 
	 * @param vmManger
	 * @param vmInstance
	 */
	private void terminateInstances(VMManger vmManger, VMInstance vmInstance) {
		// Log.printLine(CloudSim.clock() + " Terminating VM: " +
		// vmInstance.getVmID());
		if (vmManger.getFreeVMs().contains(vmInstance)) {
			vmManger.getFreeVMs().remove(vmInstance);
		}
		if (vmManger.getBusyVMs().contains(vmInstance)) {
			vmManger.getBusyVMs().remove(vmInstance);
		}
		CloudSim.send(vmManger.getId(), cloud.getId(), 0.0, VM_TERMINATE, vmInstance);
	}

}
