package scws.core;

/**
 * An interface for job schedulers used by the WorkflowEngine.
 * 
 * @author Gideon Juve <juve@usc.edu>
 */
public interface Scheduler {

	/**
	 * use the corresponding mechanism to pre-process the DAG to assign the
	 * sub-deadline.
	 * 
	 * @param dagJob
	 */
	public void preprocessDagJob(DAGJob dagJob);

	/**
	 * when a job execute failed, we have to do some operation on these task
	 * behind them in the DAG
	 * 
	 * @param job
	 */
	public void jobFailerProcess(Job job);

	/**
	 * schedule the job in the engine's queue to a specific VMManger
	 * 
	 * @param engine
	 */
	public void scheduleJobs(WorkflowEngine engine);
	
	/**
	 * schedule the job in the VMManger' queue to a specific VM instance
	 * @param vmManger
	 */
	public void scheduleJobToVMInstance(VMManger vmManger);

	public void setWorkflowEngine(WorkflowEngine engine);
}