package scws.core;

import scws.core.dag.Task;

public interface JobFactory {
	public Job createJob(DAGJob dagJob, Task task, int owner);
	
	public double getJobSize(Task task);
}
