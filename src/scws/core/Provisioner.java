package scws.core;

/**
 * An interface for resource provisioners used by the WorkflowEngine.
 * 
 * @author jiyuanshi <shijiyuan.seu@gmail.com>
 * @since 2013-4-19
 * @version sci-cwf-sim 1.0
 */
public interface Provisioner {
	/**
	 * used for VMManger resource provision
	 * @param vmManger
	 */
    public void provisionResources(VMManger vmManger);
    
    /**
     * finding the free vm instance that is close to the full hour of operation and terminated it.
     * @param vmManger
     * @param vmInstance
     */
    public void provisionResources(VMManger vmManger,VMInstance vmInstance);
    
    /**
     * used for workflow engine resource provision
     * @param workflowEngine
     */
    public void provisionResources(WorkflowEngine workflowEngine);

    public void setCloud(Cloud cloud);
}
