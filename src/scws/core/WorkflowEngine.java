package scws.core;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;

import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.core.SimEntity;
import org.cloudbus.cloudsim.core.SimEvent;
import org.cloudbus.cloudsim.core.predicates.Predicate;
import org.cloudbus.cloudsim.core.predicates.PredicateType;

import scws.core.Provisioner;

import scws.core.dag.DAGParser;
import scws.core.dag.Task;
import scws.core.dag.DAG;

/**
 * The workflow engine is an entity that executes the submitted workflows by
 * scheduling their tasks on different type VMManger, then the VMManger will
 * scheduling them to specific VM instances.
 * 
 * @author jiyuanshi <shijiyuan.seu@gmail.com>
 * @since 2013-4-17
 * @version sci-cwf-sim 1.0
 */
public class WorkflowEngine extends SimEntity implements WorkflowEvent {

	public static int next_id = 0;

	/** the dag jobs that need to be scheduled */
	private LinkedList<DAGJob> dags = new LinkedList<DAGJob>();

	/** all the dag jobs that WorkflowEngine has scheduled */
	// private LinkedList<DAGJob> allDAGJobs = new LinkedList<DAGJob>();

	/** all the Listeners that will be informed the job related event */
	private HashSet<JobListener> jobListeners = new HashSet<JobListener>();

	/** The provisioner that allocates resources for this workflow engine */
	private Provisioner provisioner;

	/** The scheduler that matches jobs to resources for this workflow engine */
	private Scheduler scheduler;

	/** The current different type of VMManger */
	private ArrayList<VMManger> vmMangers = new ArrayList<VMManger>();

	/** The list of unmatched ready jobs */
	private LinkedList<Job> queue = new LinkedList<Job>();

	/** A factory for creating Job objects from Task objects */
	private JobFactory jobFactory = null;

	/** The value that is used by provisioner to estimate system load */
	private int queueLength = 0;

	/** The total time span of this simulation */
	private double timespan = 48 * 3600.0;

	/** the total budget of this simulation */
	private double budget = Double.MAX_VALUE;

	/** the remaining budget during this provision operation */
	private double remainingBudget = 0.0;

	/**
	 * the constructor of this class
	 * 
	 * @param jobFactory
	 *            : a job factory that convert a dag task to a job
	 * @param provisioner
	 *            : the provisoner is a class that implement the resource
	 *            provision method.
	 * @param scheduler
	 *            : the scheduler is a class that implement that job scheduler
	 *            method.
	 */
	public WorkflowEngine(JobFactory jobFactory, Provisioner provisioner, Scheduler scheduler,
			Double timespan, Double budget) {
		super("WorkflowEngine" + (next_id++));
		this.jobFactory = jobFactory;
		this.provisioner = provisioner;
		this.scheduler = scheduler;
		this.budget = budget;
		this.timespan = timespan;
		this.remainingBudget = budget;
		CloudSim.addEntity(this);
	}

	public WorkflowEngine(Provisioner provisioner, Scheduler scheduler) {
		this(new SimpleJobFactory(), provisioner, scheduler, 48 * 3600.0, Double.MAX_VALUE);
	}

	@Override
	public void startEntity() {
		// Do nothing
		Log.printLine("start " + getName() + " ID=" + getId());
		sendNow(getId(), PROVISIONING_REQUEST);
	}

	@Override
	public void processEvent(SimEvent ev) {
		switch (ev.getTag()) {
		case DAG_SUBMIT:
			dagSubmit((DAGDescription) ev.getData(), ev.getSource());
			break;
		case DAG_EXPIRED:
			dagExpired((DAGJob) ev.getData(), ev.getSource());
			break;
		case JOB_STARTED:
			jobStarted((Job) ev.getData());
			break;
		case JOB_FINISHED:
			jobFinished((Job) ev.getData());
			break;
		case VM_LAUNCHED:
			// schedule the ready jobs stored in the queue.
			scheduler.scheduleJobs(this);
			break;
		case PROVISIONING_REQUEST:
			if (provisioner != null) {
				Log.printLine(CloudSim.clock() + "~PROVISIONING_REQUEST");
				provisioner.provisionResources(this);
			}
			break;
		default:
			throw new RuntimeException("Unrecognized event: " + ev);
		}

	}

	@Override
	public void shutdownEntity() {
		// Do nothing
		Log.printLine("shutdown " + getName() + " ID=" + getId());
	}

	/**
	 * get the cost of all the vm instances from different vm type
	 * 
	 * @return the cost
	 */
	public double getCost() {
		double cost = 0;

		for (VMManger vms : vmMangers) {
			cost += vms.getCost();
		}
		return cost;
	}

	/**
	 * process the DAG_SUBMIT event
	 * 
	 * @param dj
	 *            : the submitted DAG job
	 */
	private void dagSubmit(DAGDescription dj, int owner) {

		double remainingBudget = this.getBudget() - this.getCost();
		Log.printLine("remaiting budget:" + remainingBudget);
		if (remainingBudget > 0) {
			DAG dag = DAGParser.parseDAG(new File(dj.dagpath + "/" + dj.filename), dj.filename);
			dag.setPriority(dj.priority);

			// if the deadline of this dag over the total time span of this
			// simulation, we set the total time span to the deadline of this
			// dag.
			if (dj.deadline > timespan) {
				dj.deadline = timespan;
			}
			DAGJob dagJob = new DAGJob(dag, owner);
			dagJob.setWorkflowengine(this);
			dagJob.setPriority(dj.priority);
			dagJob.setDeadline(dj.deadline);
			dagJob.setSubtime(dj.subtime);

			// use the corresponding scheduler to pre-process the DAG to assign
			// the
			// sub-deadline.
			scheduler.preprocessDagJob(dagJob);

			dags.add(dagJob);
			// allDAGJobs.add(dagJob);

			// The DAG starts immediately
			sendNow(dagJob.getOwner(), DAG_STARTED, dagJob);
			// Send the DAGJob expired event
			send(getId(), dagJob.getDeadline() - CloudSim.clock() + 2.0, DAG_EXPIRED, dagJob);

			// Queue any ready jobs for this DAG
			queueReadyJobs(dagJob);

			// debug output

			Log.printLine("CloudSim Clock: " + CloudSim.clock() + " " + getName() + ": receive a DAGjob: "
					+ dagJob.getDagname() + " dagjob owner: " + dagJob.getOwner() + " subtime: "
					+ dagJob.getSubtime() + "-" + "Deadline: " + dagJob.getDeadline() + "-" + "proority: "
					+ dagJob.getPriority() + " the number of dag's task: " + dagJob.getDAG().numTasks());

			// schedule the ready jobs stored in the queue
			scheduler.scheduleJobs(this);
		} else {
			Log.printLine("no remaining budget:" + remainingBudget + " cannot accept new dag job");
		}

	}

	/**
	 * handle the DAG_EXPIRED event to delete the DAGJob and the relate jobs
	 * 
	 * @param dj
	 * @param owner
	 */
	private void dagExpired(DAGJob dj, int owner) {

		if (getDags().contains(dj)) {
			cancleDAGjob(dj);
		}

	}

	/**
	 * Get the ready tasks and convert them into jobs
	 * 
	 * @param dj
	 *            : the dag job
	 */
	private void queueReadyJobs(DAGJob dj) {
		while (true) {
			Task t = dj.nextReadyTask();
			if (t == null)
				break;
			Job j = jobFactory.createJob(dj, t, getId());
			j.setDAGJob(dj);
			j.setTask(t);
			j.setOwner(getId());
			j.setSubdeadline(t.subdealine);
			jobReleased(j);
		}
	}

	/**
	 * Notify listeners that job was released and add this job into the queue to
	 * be scheduled to the corresponding VM type manger.
	 * 
	 * @param j
	 *            : the jobs
	 */
	private void jobReleased(Job j) {
		queue.add(j);

		for (JobListener jl : jobListeners) {
			jl.jobReleased(j);
		}
	}

	/**
	 * Notify the listeners
	 * 
	 * @param j
	 */
	private void jobStarted(Job j) {
		for (JobListener jl : jobListeners) {
			jl.jobStarted(j);
		}
	}

	/**
	 * Notify the listeners, and complete this job then try to schedule the left
	 * ready job.
	 * 
	 * @param j
	 */
	private void jobFinished(Job j) {
		// Notify the listeners
		// IT IS IMPORTANT THAT THIS HAPPENS FIRST
		for (JobListener jl : jobListeners) {
			jl.jobFinished(j);
		}

		DAGJob dj = j.getDAGJob();
		Task t = j.getTask();

		// if DAGjob this job belong to is expired just cancle this DAGjob and
		// the relate jobs in related entity
		if (dj.getDeadline() < CloudSim.clock()) {
			this.cancleDAGjob(dj);
		} else {
			// If the job succeeded
			if (j.getResult() == Job.Result.SUCCESS && CloudSim.clock() <= timespan) {

				// Mark the task as complete in the DAG
				dj.completeTask(t);

				// Queue any jobs that are now ready
				queueReadyJobs(dj);

				// Log.printLine(CloudSim.clock() + " Job " + j.getID() +
				// " finished on VMManger " + j.getVmManger().getId() +
				// " on VM Instance "
				// + j.getVminstance().getVmID());

				// If the workflow is complete, send it back
				if (dj.isFinished()) {
					// Log.printLine(CloudSim.clock() + " DAGJob " +
					// dj.getDagname()
					// + " finished on VMManger "
					// + j.getVmManger().getId() + " on VM Instance " +
					// j.getVminstance().getVmID());
					dags.remove(dj);
					sendNow(dj.getOwner(), DAG_FINISHED, dj);
				}

			}

			// If the job failed
			if (j.getResult() == Job.Result.FAILURE) {
				// do some failed operation on this jobs after this job in DAG
				scheduler.jobFailerProcess(j);
				// Retry the job
				// Log.printLine(CloudSim.clock() + " Job " + j.getID() +
				// " failed on VMManger "
				// + j.getVmManger().getId() + " on VM Instance " +
				// j.getVminstance().getVmID()
				// + " resubmitting...");
				Job retry = jobFactory.createJob(dj, t, getId());
				retry.setDAGJob(dj);
				retry.setTask(t);
				retry.setOwner(getId());
				retry.setSubdeadline(t.subdealine);
				jobReleased(retry);
			}
		}

		// schedule the ready jobs stored in the queue.
		scheduler.scheduleJobs(this);
		// remove this job in queue
		getQueuedJobs().remove(j);

	}

	/**
	 * cancle the expired DAGjob in the dags and also delete the jobs belong to
	 * the DAG job in related entity
	 * 
	 * @param dagJob
	 */
	public void cancleDAGjob(DAGJob dagJob) {

		// Predicate used for cancle events
		Predicate p = new PredicateCancleDag(dagJob);

		// cancle job in every VMManger's queue and running vm instance
		for (VMManger vmManger : this.getVmMangers()) {

			Job[] tmpjobs = new Job[vmManger.getJobPriorityQueue().size()];
			vmManger.getJobPriorityQueue().toArray(tmpjobs);

			for (Job job : tmpjobs) {
				if (job.getDAGJob() == dagJob) {
					vmManger.getJobPriorityQueue().remove(job);
				}
			}

			VMInstance[] tmpvms = new VMInstance[vmManger.getBusyVMs().size()];
			vmManger.getBusyVMs().toArray(tmpvms);

			for (VMInstance vmInstance : tmpvms) {
				for (Job job : vmInstance.getRunningJobs()) {
					if (job.getDAGJob() == dagJob) {
						vmManger.jobFinished(job);
					}
				}
			}

			// Cancle future events send from all the VMMangers
			CloudSim.cancelAll(vmManger.getId(), p);
		}

		// cancle job in the engine's queue
		Job[] tmpjobs = new Job[this.getQueuedJobs().size()];
		this.getQueuedJobs().toArray(tmpjobs);
		for (Job job : tmpjobs) {
			if (job.getDAGJob() == dagJob) {
				this.getQueuedJobs().remove(job);
			}
		}

		// cancel future events send from WorkflowEngine
		CloudSim.cancelAll(getId(), p);

		// cancle the DAG job in the engine's dag queue
		this.getDags().remove(dagJob);

		Log.printLine(CloudSim.clock() + ":DAGJob:" + dagJob.getDagname() + " expired~dealine: "
				+ dagJob.getDeadline());
	}

	public LinkedList<DAGJob> getDags() {
		return dags;
	}

	public void setDags(LinkedList<DAGJob> dags) {
		this.dags = dags;
	}

	// public LinkedList<DAGJob> getAllDAGJobs() {
	// return allDAGJobs;
	// }

	// public void setAllDAGJobs(LinkedList<DAGJob> allDAGJobs) {
	// this.allDAGJobs = allDAGJobs;
	// }

	public void addJobListener(JobListener l) {
		jobListeners.add(l);
	}

	public void removeJobListener(JobListener l) {
		jobListeners.remove(l);
	}

	public Scheduler getScheduler() {
		return scheduler;
	}

	public void setScheduler(Scheduler scheduler) {
		this.scheduler = scheduler;
	}

	public ArrayList<VMManger> getVmMangers() {
		return vmMangers;
	}

	public void setVmMangers(ArrayList<VMManger> vmMangers) {
		this.vmMangers = vmMangers;
	}

	public LinkedList<Job> getQueuedJobs() {
		return queue;
	}

	public void setQueuedJobs(LinkedList<Job> queue) {
		this.queue = queue;
	}

	public JobFactory getJobFactory() {
		return jobFactory;
	}

	public void setJobFactory(JobFactory jobFactory) {
		this.jobFactory = jobFactory;
	}

	public int getQueueLength() {
		return queueLength;
	}

	public void setQueueLength(int queueLength) {
		this.queueLength = queueLength;
	}

	public Double getTimespan() {
		return timespan;
	}

	public void setTimespan(Double timespan) {
		this.timespan = timespan;
	}

	public double getBudget() {
		return budget;
	}

	public void setBudget(double budget) {
		this.budget = budget;
	}

	public double getRemainingBudget() {
		return remainingBudget;
	}

	public void setRemainingBudget(double remainingBudget) {
		this.remainingBudget = remainingBudget;
	}

}
