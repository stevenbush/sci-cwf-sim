package scws.core;

/**
 * a class that describe the relevant information of a DAG
 * 
 * @author jiyuanshi <shijiyuan.seu@gmail.com>
 * @since 2013-4-17
 * @version sci-cwf-sim 1.0
 */
public class DAGDescription {
	public DAGDescription(String filename, String dagpath, Double subtime, Double deadline, Integer priority) {
		super();
		this.filename = filename;
		this.dagpath = dagpath;
		this.subtime = subtime;
		this.deadline = deadline;
		this.priority = priority;
	}

	/** the file path of this DAG */
	public String dagpath;
	/** the file name of this DAG */
	public String filename;
	/** the submit time of this DAG */
	public Double subtime;
	/** the deadline of this DAG */
	public Double deadline;
	/** the priority of this DAG */
	public Integer priority;
}
