package scws.core;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.core.CloudSim;

import scws.core.transfer.Port;

/**
 * A VMInstance class is a virtual machine instance that executes Jobs' tasks.
 * 
 * It has a number of cores, and each core has a certain power measured in MIPS
 * (millions of instructions per second).
 * 
 * It has an input Port that is used to transfer data to the VM instance, and an
 * output Port that is used to transfer data from the VM instance. Both ports
 * have the same bandwidth.
 * 
 * Jobs' tasks can be queued and are executed in FIFO order. The scheduling is
 * space shared.
 * 
 * Every VM instance belong to a VM type, it has a specific price per hour. The
 * cost of a VM instance is computed by multiplying the runtime in hours by the
 * hourly price. The runtime is rounded up to the nearest hour for this
 * calculation.
 * 
 * Each VM instance has a provisioning delay between when it is launched and
 * when it is ready, and a deprovisioning delay between when it is terminated
 * and when the provider stops charging for it.
 * 
 * @author jiyuanshi <shijiyuan.seu@gmail.com>
 * @since 2013-4-19
 * @version sci-cwf-sim 1.0
 */
public class VMInstance implements WorkflowEvent {

	/** It's a unique identification number for every class */
	private static int next_id = 0;

	/** The default provisioning delay */
	public static final double DEFAULT_PROVISIONING_DELAY = 60.0;

	/** The default deprovisioning delay */
	public static final double DEFAULT_DEPROVISIONING_DELAY = 10.0;

	/** How many seconds there are in one hour */
	public static final double SECONDS_PER_HOUR = 60 * 60;

	/** the VMInstance class ID number */
	private int vmID;

	/** The VMManger SimEntity ID that owns this VM instance */
	private int owner;

	/** the VMManger object that manage this vm instance */
	private VMManger manager;

	/** The Cloud that runs this VM instance */
	private int cloud;

	/** The processing power of this VM instance */
	private Double mips;

	/** The number of cores of this VM instance */
	private int cores;

	/** Network port for input data */
	private Port inputPort;

	/** Network port for output data */
	private Port outputPort;

	/** Current idle cores */
	private int idleCores;

	/** Queue of jobs submitted to this VM instance */
	private LinkedList<Job> jobs;

	/** Set of jobs currently running */
	private Set<Job> runningJobs;

	/** Time that the VM instance was launched */
	private double launchTime;

	/** Time that the VM instance was terminated */
	private double terminateTime;

	/** The usage record of this VM instance */
	// private ArrayList<Job> allJobs;

	/** Price per hour of usage */
	private double price;

	/** Is this VM running? */
	private boolean running;

	/** Number of CPU seconds consumed by jobs on this VM instance */
	private double cpuSecondsConsumed;

	/** Delay from when the VM instance is launched until it is ready */
	private double provisioningDelay = DEFAULT_PROVISIONING_DELAY;

	/**
	 * Delay from when the VM instance is terminated until it is no longer
	 * charged
	 */
	private double deprovisioningDelay = DEFAULT_DEPROVISIONING_DELAY;

	/**
	 * Varies the actual runtime of tasks according to the specified
	 * distribution
	 */
	private RuntimeDistribution runtimeDistribution = new IdentityRuntimeDistribution();

	/** Varies the failure rate of tasks according to a specified distribution */
	private FailureModel failureModel = new FailureModel(0, 0.0);

	public VMInstance(VMManger manager, Double mips, int cores, double bandwidth, double price) {
		this.vmID = next_id++;
		this.manager = manager;
		this.mips = mips;
		this.cores = cores;
		this.inputPort = new Port(bandwidth);
		this.outputPort = new Port(bandwidth);
		this.jobs = new LinkedList<Job>();
		// this.allJobs = new ArrayList<Job>();
		this.runningJobs = new HashSet<Job>();
		this.idleCores = cores;
		this.launchTime = -1.0;
		this.terminateTime = -1.0;
		this.price = price;
		this.running = false;
		this.cpuSecondsConsumed = 0.0;
	}

	/**
	 * Runtime of the VM in seconds. If the VM has not been launched, then the
	 * result is 0. If the VM is not terminated, then we use the current
	 * simulation time as the termination time. After the VM is terminated the
	 * runtime does not change.
	 * 
	 * @return the actual run time of this VM instance
	 */
	public double getRuntime() {
		if (launchTime < 0)
			return 0.0;
		else if (terminateTime < 0)
			return CloudSim.clock() - launchTime;
		else
			return terminateTime - launchTime;
	}

	/**
	 * Compute the total cost of this VM instance. This is computed by taking
	 * the runtime, rounding it up to the nearest whole hour, and multiplying by
	 * the hourly price.
	 * 
	 * @return the cost value of this VM instance
	 */
	public double getCost() {
		double hours = getRuntime() / SECONDS_PER_HOUR;
		hours = Math.ceil(hours);
		// Log.printLine(CloudSim.clock() + " VM " + getId() + " cost " + hours
		// * price);
		return hours * price;
	}

	/**
	 * Get the utilization of this VM instance for the judgment criteria of
	 * deciding whether terminate this VM instance or ask for new VM instances
	 * 
	 * @return the utilization of this VM instance
	 */
	public double getUtilization() {
		double totalCPUSeconds = getRuntime() * cores;
		return cpuSecondsConsumed / totalCPUSeconds;
	}

	/**
	 * process the VM_LAUNCH operation
	 */
	public void launchVM() {
		// Reset dynamic state
		jobs.clear();
		idleCores = cores;
		cpuSecondsConsumed = 0.0;

		// VM can now accept jobs
		running = true;

		if (running) {
			CloudSim.send(getOwner(), getOwner(), 3600 - getDeprovisioningDelay() - getProvisioningDelay(),
					VM_APPROACHING, this);
			CloudSim.send(getOwner(), getOwner(), 3600, VM_CHECK_APPROACHING, this);
		}
	}

	/**
	 * process the VM_TERMINATE operation
	 */
	public void terminateVM() {
		// Can no longer accept jobs
		running = false;

		// Move running jobs back to the queue...
		jobs.addAll(runningJobs);
		runningJobs.clear();

		// ... and fail all queued jobs
		for (Job job : jobs) {
			job.setFinishTime(CloudSim.clock());
			job.setResult(Job.Result.FAILURE);
			CloudSim.send(getOwner(), job.getOwner(), 0.0, JOB_FINISHED, job);
			Log.printLine(CloudSim.clock() + " Terminating job " + job.getID() + " on VM "
					+ job.getVminstance().getVmID());
		}
		jobs.clear();
		// allJobs.clear();

	}

	/**
	 * process the Check approaching event, when this VM instance are
	 * approaching the billing cycle, send the approaching event to ask the
	 * resource provision process
	 */
	public void vmCheckApproaching() {
		if (running) {
			CloudSim.send(getOwner(), getOwner(), 3600 - getDeprovisioningDelay() - getProvisioningDelay(),
					VM_APPROACHING, this);
			CloudSim.send(getOwner(), getOwner(), 3600, VM_CHECK_APPROACHING, this);
		}
	}

	/**
	 * process JOB_SUBMIT operation
	 * 
	 * @param job
	 */
	public void jobSubmit(Job job) {
		// Sanity check
		if (!running) {
			throw new RuntimeException("Cannot execute jobs: VM not running");
		}

		job.setVminstance(this);

		// Queue the job
		jobs.add(job);

		// Start to schedule the job in the queues for excutionThis shouldn't do
		// anything if the VM is busy
		startJobs();
	}

	/**
	 * check the queue and idle cores and try to execute the remaining job in
	 * the queue.
	 */
	private void startJobs() {
		// While there are still idle jobs and cores
		while (jobs.size() > 0 && idleCores > 0) {
			// Start the next job in the queue
			jobStart(jobs.poll());
		}
	}

	/**
	 * start a job for execution
	 * 
	 * @param job
	 */
	private void jobStart(Job job) {
		// record this job into the allJobs array
		// allJobs.add(job);
		// The job is now running
		job.setStartTime(CloudSim.clock());
		job.setState(Job.State.RUNNING);
		// add it to the running set
		runningJobs.add(job);

		// Tell the owner
		CloudSim.send(getOwner(), job.getOwner(), 0.0, JOB_STARTED, job);

		// Compute the duration of the job on this VM
		double size = job.getSize();
		double predictedRuntime = size / mips;

		// Compute actual runtime
		double actualRuntime = this.runtimeDistribution.getActualRuntime(predictedRuntime);

		// Decide whether the job succeeded or failed
		if (failureModel.failureOccurred()) {
			job.setResult(Job.Result.FAILURE);

			// How long did it take to fail?
			actualRuntime = failureModel.runtimeBeforeFailure(actualRuntime);
		} else {
			job.setResult(Job.Result.SUCCESS);
		}

		// send this JOB_FINISHED event to the corresponding VMManger and make
		// it wait actualRuntime to be scheduled
		CloudSim.send(getOwner(), getOwner(), actualRuntime, JOB_FINISHED, job);
		// Log.printLine(CloudSim.clock() + " Starting job " + job.getID() +
		// " on VM " + job.getVminstance().getVmID()
		// + " duration " + actualRuntime);

		// One core is now busy running the job
		idleCores--;
	}

	/**
	 * Finish a job run on this vm and change the related states of this job,
	 * then call startjob to execute the other job in the queue.
	 * 
	 * @param job
	 */
	public void jobFinish(Job job) {

		// Sanity check
		if (!running) {
			throw new RuntimeException("Cannot finish job~" + job.getID() + " : VM " + getVmID()
					+ " not running");
		}

		if (runningJobs.contains(job)) {
			// remove from the running set
			runningJobs.remove(job);

			// Complete the job
			job.setFinishTime(CloudSim.clock());
			job.setState(Job.State.TERMINATED);

			// Increment the usage
			cpuSecondsConsumed += job.getDuration();

			// Tell the job's owner
			CloudSim.send(getOwner(), job.getOwner(), 0.0, JOB_FINISHED, job);

			// The core that was running the job is now free
			idleCores++;

			// We may be able to start more jobs now
			startJobs();
		}

	}

	public int getOwner() {
		return owner;
	}

	public void setOwner(int owner) {
		this.owner = owner;
	}

	public int getCloud() {
		return cloud;
	}

	public void setCloud(int cloud) {
		this.cloud = cloud;
	}

	public Double getMips() {
		return mips;
	}

	public void setMips(Double mips) {
		this.mips = mips;
	}

	public int getCores() {
		return cores;
	}

	public void setCores(int cores) {
		this.cores = cores;
	}

	public Port getInputPort() {
		return inputPort;
	}

	public void setInputPort(Port inputPort) {
		this.inputPort = inputPort;
	}

	public Port getOutputPort() {
		return outputPort;
	}

	public void setOutputPort(Port outputPort) {
		this.outputPort = outputPort;
	}

	public int getIdleCores() {
		return idleCores;
	}

	public void setIdleCores(int idleCores) {
		this.idleCores = idleCores;
	}

	public LinkedList<Job> getJobs() {
		return jobs;
	}

	public void setJobs(LinkedList<Job> jobs) {
		this.jobs = jobs;
	}

	public Set<Job> getRunningJobs() {
		return runningJobs;
	}

	public void setRunningJobs(Set<Job> runningJobs) {
		this.runningJobs = runningJobs;
	}

	public double getLaunchTime() {
		return launchTime;
	}

	public void setLaunchTime(double launchTime) {
		this.launchTime = launchTime;
	}

	public double getTerminateTime() {
		return terminateTime;
	}

	public void setTerminateTime(double terminateTime) {
		this.terminateTime = terminateTime;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public boolean isRunning() {
		return running;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}

	public double getCpuSecondsConsumed() {
		return cpuSecondsConsumed;
	}

	public void setCpuSecondsConsumed(double cpuSecondsConsumed) {
		this.cpuSecondsConsumed = cpuSecondsConsumed;
	}

	public double getProvisioningDelay() {
		return provisioningDelay;
	}

	public void setProvisioningDelay(double provisioningDelay) {
		this.provisioningDelay = provisioningDelay;
	}

	public double getDeprovisioningDelay() {
		return deprovisioningDelay;
	}

	public void setDeprovisioningDelay(double deprovisioningDelay) {
		this.deprovisioningDelay = deprovisioningDelay;
	}

	public RuntimeDistribution getRuntimeDistribution() {
		return runtimeDistribution;
	}

	public void setRuntimeDistribution(RuntimeDistribution runtimeDistribution) {
		this.runtimeDistribution = runtimeDistribution;
	}

	public FailureModel getFailureModel() {
		return failureModel;
	}

	public void setFailureModel(FailureModel failureModel) {
		this.failureModel = failureModel;
	}

	public int getVmID() {
		return vmID;
	}

	public void setVmID(int vmID) {
		this.vmID = vmID;
	}

	public VMManger getManager() {
		return manager;
	}

	public void setManager(VMManger manager) {
		this.manager = manager;
	}

	// public ArrayList<Job> getAllJobs() {
	// return allJobs;
	// }
	//
	// public void setAllJobs(ArrayList<Job> allJobs) {
	// this.allJobs = allJobs;
	// }

}
