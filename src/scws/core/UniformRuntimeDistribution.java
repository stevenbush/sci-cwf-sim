package scws.core;

import java.util.Random;

import org.apache.commons.math3.distribution.UniformRealDistribution;

/**
 * Returns 'runtime' +/- 'variance' percent of 'runtime', where the actual
 * variance is drawn from a uniform distribution. e.g. if 'variance' is .10,
 * then it will return a random uniform value that is +/- 10% of 'runtime'.
 * 
 * @author jiyuanshi <shijiyuan.seu@gmail.com>
 * @since 2013-05-12
 * @version sci-cwf-sim 1.0
 */
public class UniformRuntimeDistribution implements RuntimeDistribution {
	private double variance;

	public UniformRuntimeDistribution(double variance) {
		this.variance = variance;
	}

	@Override
	public double getActualRuntime(double runtime) {
		// Get a random number in the range [-1,+1]
		UniformRealDistribution distribution = new UniformRealDistribution(-1, 1);
		double plusorminus = distribution.sample();
		return runtime + (plusorminus * variance * runtime);
	}

	public static void main(String[] args) {
		RuntimeDistribution d = new UniformRuntimeDistribution(0.2);

		for (int i = 0; i < 1000; i++) {
			System.out.println(d.getActualRuntime(100));
		}
	}
}
