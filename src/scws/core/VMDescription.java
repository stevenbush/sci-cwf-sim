package scws.core;

/**
 * a class that describe the relevant information of a VM
 * @author jiyuanshi <shijiyuan.seu@gmail.com>
 * @since 2013-4-17
 * @version sci-cwf-sim 1.0
 */
public class VMDescription {
	
	public VMDescription(String vmname, Double mips, Double price) {
		super();
		this.vmname = vmname;
		this.mips = mips;
		this.price = price;
	}
	/** the name of this VM type */
	public String vmname;
	/** the computing power of this VM type */
	public Double mips;
	/** the price of this VM type */
	public Double price;
}
