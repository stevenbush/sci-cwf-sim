package scws.core;

import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Set;

import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.core.SimEntity;
import org.cloudbus.cloudsim.core.SimEvent;
import org.cloudbus.cloudsim.distributions.ContinuousDistribution;

import scws.core.experiment.VMFactory.ZeroDistribution;

/**
 * This is a class that manage a set of VM instances of one type, include
 * requesting and releasing VM instances.
 * 
 * @author jiyuanshi
 * @since 2013-4-19
 * @version sci-cwf-sim 1.0
 * 
 */
public class VMManger extends SimEntity implements WorkflowEvent {

	/** It's a unique identification number for every class */
	public static int next_id = 0;

	/** the cloud this VMManger belong to */
	private Cloud cloud;

	/** the last provision operation time */
	private double lastPOTime = 0.0;

	/** the VM instance number during this provision operation */
	private int vmNumber = 0;

	/** the total cost of all the VM type instances */
	private double terminatedCost = 0.0;

	/** the price per hour of this VM type */
	private double price = 0.0;

	/** The processing power of this VM instance */
	private Double mips;

	/** the cores of this kind of VM instance */
	private int cores;

	/** the bandwidth of this kind of VM instance */
	private double bandwidth;

	/** the name of this kind of VM */
	private String name;

	/** The provisioner that allocates resources for this workflow engine */
	private Provisioner provisioner;

	/** The scheduler that matches jobs to resources for this workflow engine */
	private Scheduler scheduler;

	/** The current VM instances of this type */
	private LinkedList<VMInstance> vms = new LinkedList<VMInstance>();

	/** the proportion of the inform advance time. */
	private double informtimeratio;

	/** the profision delay base of this kind of VM type */
	private double provisioningDelay = 0.0;

	/** the provision delay distribution of this kind of VM type */
	private ContinuousDistribution provisioningDelayDistribution = new ZeroDistribution();

	/** the de-provision delay distribution of this kind of VM type */
	private ContinuousDistribution deprovisioningDelayDistribution = new ZeroDistribution();

	/** the Run time distribution of this kind of VM type */
	private RuntimeDistribution runtimeDistribution = new IdentityRuntimeDistribution();

	/** the failure model of this kind of VM type */
	private FailureModel failureModel = new FailureModel(0, 0.0);

	/**
	 * The set of free VM instances, i.e. the ones which are not executing any
	 * jobs (idle)
	 */
	protected Set<VMInstance> freeVMs = new HashSet<VMInstance>();

	/** The set of busy VM instances, i.e. the ones which execute jobs */
	protected Set<VMInstance> busyVMs = new HashSet<VMInstance>();

	/**
	 * The PriorityQueue list of unmatched ready jobs, in this PriorityQueue the
	 * job with small priority number and small sub-deadline will be scheduled
	 * first; schedule the job with earlier deadline first, if some job with the
	 * same deadline, schedule the job with high priority first.
	 */
	PriorityQueue<Job> jobPriorityQueue = new PriorityQueue<Job>(1, new Comparator<Job>() {
		public int compare(Job a, Job b) {
			
			if (a.getSubdeadline() < b.getSubdeadline()) {
				return -1;
			} else if (a.getSubdeadline() > b.getSubdeadline()) {
				return 1;
			} else {
				if (a.getDAGJob().getPriority() < b.getDAGJob().getPriority()) {
					return -1;
				} else if (a.getDAGJob().getPriority() > b.getDAGJob().getPriority()) {
					return 1;
				} else {
					return a.getID() - b.getID();
				}
			}
			
		}
	});

	/**
	 * The length of this priority queue.
	 */
	private int priorityQueueLength = 0;

	/** the workflow engine that submit job to this VM manger */
	private WorkflowEngine workflowengine;

	public VMManger(String name, Double inputprice, Double inputmips, Integer inputcores,
			Double inputbandwidth, Provisioner provisioner, Scheduler scheduler,
			WorkflowEngine workflowEngine, Cloud cloud) {
		super(name + (next_id++));
		this.name = name;
		this.price = inputprice;
		this.mips = inputmips;
		this.cores = inputcores;
		this.bandwidth = inputbandwidth;
		this.provisioner = provisioner;
		this.scheduler = scheduler;
		this.workflowengine = workflowEngine;
		this.cloud = cloud;
		CloudSim.addEntity(this);
	}

	@Override
	public void startEntity() {
		Log.printLine(getName() + getId() + " VMManger started...");
		setLastPOTime(CloudSim.clock());
		setVmNumber(getBusyVMs().size() + getFreeVMs().size());
		send(getId(), getWorkflowengine().getTimespan(), TERMINATE_SIMULATION);
	}

	@Override
	public void processEvent(SimEvent ev) {
		switch (ev.getTag()) {
		case TERMINATE_SIMULATION:
			terminate_simulation();
			break;
		case VM_LAUNCH:
			vmLaunch((VMInstance) ev.getData());
			break;
		case VM_LAUNCHED:
			vmLaunched((VMInstance) ev.getData());
			break;
		case VM_TERMINATE:
			((VMInstance) ev.getData()).terminateVM();
			break;
		case VM_TERMINATED:
			vmTerminated((VMInstance) ev.getData());
			break;
		case VM_APPROACHING:
			vmApproaching((VMInstance) ev.getData());
			break;
		case VM_CHECK_APPROACHING:
			vmCheckApproaching((VMInstance) ev.getData());
			break;
		case JOB_SUBMIT:
			jobSubmit((Job) ev.getData());
			break;
		case JOB_APPROACH_LST:
			jobApproachLST((Job) ev.getData());
			break;
		case JOB_SUBMIT_TO_INSTANCE:
			jobSubmitToInstance((Job) ev.getData());
			break;
		case JOB_FINISHED:
			jobFinished((Job) ev.getData());
			break;
		default:
			throw new RuntimeException("Unrecognized event: " + ev);
		}

	}

	@Override
	public void shutdownEntity() {
		Log.printLine(getName() + "VMManger end...");

	}

	/**
	 * return the total cost consumed by all the VM instance that belong to this
	 * VM type.
	 * 
	 * @return total cost consumed by all VMs.
	 */
	public double getCost() {
		double cost = terminatedCost;

		for (VMInstance vm : vms) {
			cost += vm.getCost();
		}

		return cost;

	}

	/** reaching the timespan we need to terminate this simulation */
	private void terminate_simulation() {
		getJobPriorityQueue().clear();
		for (VMInstance vmInstance : getAvailableVMs()) {
			sendNow(getCloud().getId(), TERMINATE_SIMULATION, vmInstance);
			vmInstance.terminateVM();
		}
	}

	/**
	 * Do some operation when a new VM instance is launching
	 * 
	 * @param vm
	 */
	private void vmLaunch(VMInstance vm) {
		vm.launchVM();
		vms.add(vm);
		freeVMs.add(vm);
	}

	/**
	 * Do some operation when a new VM instance launched
	 * 
	 * @param vm
	 */
	private void vmLaunched(VMInstance vm) {
		Log.printLine("VM instance: " + getName() + vm.getVmID() + " started");
		sendNow(getWorkflowengine().getId(), VM_LAUNCHED);
		scheduler.scheduleJobToVMInstance(this);
	}

	private void vmTerminated(VMInstance vm) {
		// remove the vm instance from the related queue and add the cost of
		// this vm instance into the total cost
		if (vms.contains(vm)) {
			terminatedCost = terminatedCost + vm.getCost();
			vms.remove(vm);
		}
		// this operation has already been done in Provisioner class.
		/*
		 * if (freeVMs.contains(vm)) { freeVMs.remove(vm); } if
		 * (busyVMs.contains(vm)) { busyVMs.remove(vm); }
		 */
	}

	/**
	 * processing the vm Approaching event
	 */
	private void vmApproaching(VMInstance vmInstance) {
		if (vms.contains(vmInstance) && vmInstance.isRunning()) {
			provisioner.provisionResources(this, vmInstance);
		}
	}

	/**
	 * processing the vm check approaching event
	 */
	private void vmCheckApproaching(VMInstance vmInstance) {
		if (vms.contains(vmInstance) && vmInstance.isRunning()) {
			vmInstance.vmCheckApproaching();
		}
	}

	/**
	 * processing the job submit event
	 * 
	 * @param dj
	 */
	private void jobSubmit(Job job) {

		// Log.printLine(CloudSim.clock() + ": " + getName() +
		// " receive a job: " + job.getDAGJob().getDagname()
		// + "~" + job.getID());
		// Log.printLine("Job sub-time:" + job.getSubmitTime());

		// sending the approaching job latest start time event
		send(getId(), job.getSubdeadline() - (job.getSize() / getMips()) * (1 + getInformtimeratio())
				- CloudSim.clock(), JOB_APPROACH_LST, job);
		// send(getId(),
		// job.getSubdeadline() - (job.getSize() / getMips()) -
		// getProvisioningDelay()
		// - CloudSim.clock(), JOB_APPROACH_LST, job);

		jobPriorityQueue.add(job);
		provisioner.provisionResources(this);
		scheduler.scheduleJobToVMInstance(this);
	}

	/**
	 * processing the job approaching latest start time event
	 * 
	 * @param job
	 */
	private void jobApproachLST(Job job) {
		if (jobPriorityQueue.contains(job)) {
			provisioner.provisionResources(this);
			scheduler.scheduleJobToVMInstance(this);
		}

	}

	/**
	 * processing the job submit to instance event
	 * 
	 * @param job
	 */
	private void jobSubmitToInstance(Job job) {
		job.getVminstance().jobSubmit(job);
	}

	/**
	 * processing the job finished event
	 * 
	 * @param j
	 */
	public void jobFinished(Job j) {
		VMInstance vm = j.getVminstance();
		if (vm.isRunning()) {
			vm.jobFinish(j);
			// add to free if contained in busy set
			if (busyVMs.remove(vm))
				freeVMs.add(vm);
		}

		scheduler.scheduleJobToVMInstance(this);
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Provisioner getProvisioner() {
		return provisioner;
	}

	public void setProvisioner(Provisioner provisioner) {
		this.provisioner = provisioner;
	}

	public Scheduler getScheduler() {
		return scheduler;
	}

	public void setScheduler(Scheduler scheduler) {
		this.scheduler = scheduler;
	}

	public LinkedList<VMInstance> getAvailableVMs() {
		return vms;
	}

	public Set<VMInstance> getFreeVMs() {
		return freeVMs;
	}

	public void setFreeVMs(Set<VMInstance> freeVMs) {
		this.freeVMs = freeVMs;
	}

	public Set<VMInstance> getBusyVMs() {
		return busyVMs;
	}

	public void setBusyVMs(Set<VMInstance> busyVMs) {
		this.busyVMs = busyVMs;
	}

	public int getPriorityQueueLength() {
		return priorityQueueLength;
	}

	public void setPriorityQueueLength(int priorityQueueLength) {
		this.priorityQueueLength = priorityQueueLength;
	}

	public WorkflowEngine getWorkflowengine() {
		return workflowengine;
	}

	public void setWorkflowengine(WorkflowEngine workflowengine) {
		this.workflowengine = workflowengine;
	}

	public PriorityQueue<Job> getJobPriorityQueue() {
		return jobPriorityQueue;
	}

	public void setJobPriorityQueue(PriorityQueue<Job> jobPriorityQueue) {
		this.jobPriorityQueue = jobPriorityQueue;
	}

	public Double getMips() {
		return mips;
	}

	public void setMips(Double mips) {
		this.mips = mips;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCores() {
		return cores;
	}

	public void setCores(int cores) {
		this.cores = cores;
	}

	public double getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(double bandwidth) {
		this.bandwidth = bandwidth;
	}

	public ContinuousDistribution getProvisioningDelayDistribution() {
		return provisioningDelayDistribution;
	}

	public void setProvisioningDelayDistribution(ContinuousDistribution provisioningDelayDistribution) {
		this.provisioningDelayDistribution = provisioningDelayDistribution;
	}

	public ContinuousDistribution getDeprovisioningDelayDistribution() {
		return deprovisioningDelayDistribution;
	}

	public void setDeprovisioningDelayDistribution(ContinuousDistribution deprovisioningDelayDistribution) {
		this.deprovisioningDelayDistribution = deprovisioningDelayDistribution;
	}

	public RuntimeDistribution getRuntimeDistribution() {
		return runtimeDistribution;
	}

	public void setRuntimeDistribution(RuntimeDistribution runtimeDistribution) {
		this.runtimeDistribution = runtimeDistribution;
	}

	public FailureModel getFailureModel() {
		return failureModel;
	}

	public void setFailureModel(FailureModel failureModel) {
		this.failureModel = failureModel;
	}

	public double getInformtimeratio() {
		return informtimeratio;
	}

	public void setInformtimeratio(double informtimeratio) {
		this.informtimeratio = informtimeratio;
	}

	public double getLastPOTime() {
		return lastPOTime;
	}

	public void setLastPOTime(double lastPOTime) {
		this.lastPOTime = lastPOTime;
	}

	public int getVmNumber() {
		return vmNumber;
	}

	public void setVmNumber(int vmNumber) {
		this.vmNumber = vmNumber;
	}

	public Cloud getCloud() {
		return cloud;
	}

	public void setCloud(Cloud cloud) {
		this.cloud = cloud;
	}

	public double getProvisioningDelay() {
		return provisioningDelay;
	}

	public void setProvisioningDelay(double provisioningDelay) {
		this.provisioningDelay = provisioningDelay;
	}

}
