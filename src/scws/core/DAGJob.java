package scws.core;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import scws.core.dag.DAG;
import scws.core.dag.Task;

/**
 * This class records information about the execution of a DAG, including the
 * state of all tasks.
 * 
 * @author jiyuanshi <shijiyuan.seu@gmail.com>
 * @since 2013-4-16
 * @version sci-cwf-sim 1.0
 */
public class DAGJob {

	/** The entity that owns the DAG */
	private int owner;

	/** The DAG being executed */
	private DAG dag;

	/** if DAG job is started */
	private Boolean started;

	/** the name of the dag file */
	private String dagname;

	/** Set of tasks that have been released */
	private Set<Task> releasedTasks;

	/** Set of tasks that are finished */
	private Set<Task> completedTasks;

	/** List of all tasks that are ready but have not been claimed */
	private LinkedList<Task> queue;

	/** Workflow priority */
	private double priority;

	/** the submit time of this job */
	private double subtime;

	/** the overall deadline of this job */
	private double deadline;

	/** the workflow engine that schedule this DAG job */
	private WorkflowEngine workflowengine;

	/**
	 * represent if this dag is evaluated: 0 means unevaluated, 1 means
	 * admitted, 2 means rejected
	 */
	private int admitted = 0;

	public DAGJob(DAG dag, int owner) {
		this.dag = dag;
		this.started = false;
		this.dagname = dag.getDagname();
		this.owner = owner;
		this.queue = new LinkedList<Task>();
		this.releasedTasks = new HashSet<Task>();
		this.completedTasks = new HashSet<Task>();
		this.admitted = 0;

		// Release all root tasks
		for (String tid : dag.getTasks()) {
			Task t = dag.getTask(tid);
			if (t.parents.size() == 0) {
				releaseTask(t);
			}
		}
	}

	public int getOwner() {
		return owner;
	}

	public void setOwner(int owner) {
		this.owner = owner;
	}

	public DAG getDAG() {
		return dag;
	}

	public void setDAG(DAG dag) {
		this.dag = dag;
	}

	/** Check to see if a task has been released */
	public boolean isReleased(Task t) {
		return releasedTasks.contains(t);
	}

	/** Check to see if a task has been completed */
	public boolean isComplete(Task t) {
		return completedTasks.contains(t);
	}

	/** Return true if the workflow is finished */
	public boolean isFinished() {
		// The workflow must be finished if all the tasks that
		// have been released have been completed
		return releasedTasks.size() == completedTasks.size();
	}

	private void releaseTask(Task t) {
		releasedTasks.add(t);
		queue.add(t);
	}

	/** Mark a task as completed */
	public void completeTask(Task t) {
		// Sanity check
		if (!releasedTasks.contains(t)) {
			throw new RuntimeException("Task has not been released: " + t);
		}

		// Add it to the list of completed tasks
		completedTasks.add(t);

		// Release all ready children
		for (Task c : t.children) {
			if (!releasedTasks.contains(c)) {
				if (completedTasks.containsAll(c.parents)) {
					releaseTask(c);
				}
			}
		}
	}

	/** Return the next ready task */
	public Task nextReadyTask() {
		if (queue.size() <= 0)
			return null;
		return queue.pop();
	}

	/** Return the number of ready tasks */
	public int readyTasks() {
		return queue.size();
	}

	public double getSubtime() {
		return subtime;
	}

	public void setSubtime(double subtime) {
		this.subtime = subtime;
	}

	public double getDeadline() {
		return deadline;
	}

	public void setDeadline(double deadline) {
		this.deadline = deadline;
	}

	public double getPriority() {
		return priority;
	}

	public void setPriority(double priority) {
		this.priority = priority;
	}

	public String getDagname() {
		return dagname;
	}

	public void setDagname(String dagname) {
		this.dagname = dagname;
	}

	public WorkflowEngine getWorkflowengine() {
		return workflowengine;
	}

	public void setWorkflowengine(WorkflowEngine workflowengine) {
		this.workflowengine = workflowengine;
	}

	public Boolean getStarted() {
		return started;
	}

	public void setStarted(Boolean started) {
		this.started = started;
	}

	public int getAdmitted() {
		return admitted;
	}

	public void setAdmitted(int admitted) {
		this.admitted = admitted;
	}

}
