package scws.core.dag;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * represent a DAG graph and implement some methods
 * @author Jiyuanshi <shijiyuan.seu@gmail.com>
 * @since 2013-05-03
 * @version sci-cwf-sim 1.0
 */
public class DAG {
	
	/** the dag name */
	private String dagname;
	/** the priority of this DAG */
	private Integer priority;
    private HashMap<String, Double> files = new HashMap<String, Double>();
    private HashMap<String, Task> tasks = new HashMap<String, Task>();

    public DAG(String dagname) {
    	this.dagname = dagname;
    }

    public void addTask(Task t) {
        if (tasks.containsKey(t.id)) {
            throw new RuntimeException("Task already exists: " + t.id);
        }
        tasks.put(t.id, t);
    }

    public void addFile(String name, double size) {
        if (size < 0) {
            throw new RuntimeException("Invalid size for file '" + name + "': " + size);
        }
        files.put(name, size);
    }

    public void addEdge(String parent, String child) {
        Task p = tasks.get(parent);
        if (p == null) {
            throw new RuntimeException("Invalid edge: Parent not found: " + parent);
        }
        Task c = tasks.get(child);
        if (c == null) {
            throw new RuntimeException("Invalid edge: Child not found: " + child);
        }
        p.children.add(c);
        c.parents.add(p);
    }

    public void setInputs(String task, List<String> inputs) {
        Task t = getTask(task);
        t.inputs = inputs;
    }

    public void setOutputs(String task, List<String> outputs) {
        Task t = getTask(task);
        t.outputs = outputs;
    }

    public int numTasks() {
        return tasks.size();
    }

    public int numFiles() {
        return files.size();
    }

    public Task getTask(String id) {
        if (!tasks.containsKey(id)) {
            throw new RuntimeException("Task not found: " + id);
        }
        return tasks.get(id);
    }

    public double getFileSize(String name) {
        if (!files.containsKey(name)) {
            throw new RuntimeException("File not found: " + name);
        }
        return files.get(name);
    }

    public String[] getFiles() {
        return files.keySet().toArray(new String[0]);
    }

    public String[] getTasks() {
        return tasks.keySet().toArray(new String[0]);
    }

	public String getDagname() {
		return dagname;
	}

	public void setDagname(String dagname) {
		this.dagname = dagname;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}
	
	/**
	 * get all the start tasks which have no parents in this DAG
	 * @return an array contain these tasks
	 */
	public ArrayList<Task> getStartTasks() {
		ArrayList<Task> starttasks = new ArrayList<Task>();
		for (Task task : tasks.values()) {
			if (task.parents.size()==0) {
				starttasks.add(task);
			}
		}
		return starttasks;
	}
	
	/**
	 * get all the end tasks which have no childrens in this DAG
	 * @return an array contain these tasks
	 */
	public ArrayList<Task> getEndTasks() {
		ArrayList<Task> endtasks = new ArrayList<Task>();
		for (Task task : tasks.values()) {
			if (task.children.size()==0) {
				endtasks.add(task);
			}
		}
		return endtasks;
	}
	
	/**
	 * Get a virtual start task that connect all the task without parents in the DAG as children
	 * @return this virtual start task
	 */
	public Task getVirtualStartTask() {
		Task virtualStart = new Task("virtual_start", "virtual_start", 0);
		for (Task task : getStartTasks()) {
			virtualStart.children.add(task);
		}
		return virtualStart;
	}
	
	/**
	 * Get a virtual end task that connect all the task without children in the DAG as parent
	 * @return this virtual end task
	 */
	public Task getVirtualEndTask() {
		Task virtualEnd = new Task("virtual_end", "virtual_end", 0);
		for (Task task : getEndTasks()) {
			virtualEnd.parents.add(task);
		}
		return virtualEnd;

	}
}
