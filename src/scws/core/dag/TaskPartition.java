package scws.core.dag;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * this class represents a set of task after parting the original DAG, it
 * represents a node in the Partition DAG
 * 
 * @author jiyuanshi
 * @since 2013-05-03
 * @version sci-cwf-sim 1.0
 * 
 */
public class TaskPartition {

	/** the id of this task set */
	public String id = null;

	public List<TaskPartition> parents = new ArrayList<TaskPartition>(2);
	public List<TaskPartition> children = new ArrayList<TaskPartition>(5);

	/** the type if this task set Synchronization task or Branch of Simple task */
	public String type;

	/** the tasks in this set */
	public ArrayList<Task> tasks = new ArrayList<Task>();

	/** the lever of this set */
	public Integer lever = 0;

	/** sub-deadline of this tasksset */
	public double subdealine = 0.0;

	public TaskPartition(String id, String type) {
		this.id = id;
		this.type = type;
		this.lever = 0;
	}

	/**
	 * get the total size of thi task set
	 * 
	 * @return the size
	 */
	public Double getSize() {
		Double sum = 0.0;
		for (Task task : tasks) {
			sum = sum + task.size;
		}
		return sum;
	}

	@Override
	public String toString() {
		return id;
	}

}
