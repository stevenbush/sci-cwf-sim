package scws.core.dag;

import java.util.ArrayList;
import java.util.HashMap;

import scws.core.VMDescription;
import scws.core.dag.Task;

import scws.core.dag.algorithms.CriticalPath;
import scws.core.dag.algorithms.TopologicalOrder;

/**
 * Get the related DAG status
 * 
 * @author jiyuanshi
 * @since 2013-4-15
 * @versions sci-cwf-sim 1.0
 */
public class DAGStats {

	/**
	 * the minimum critical path length (execution time) when use the most
	 * powerful vm
	 */
	private double minCriticalPath;
	/**
	 * the maximum critical path length (execution time) when use the lowest
	 * powerful vm
	 */
	private double maxCriticalPath;
	/** the minimum total execution time when use the most powerful vm */
	private double minTotalRuntime;
	/** the maximum total execution time when use the lowest powerful vm */
	private double maxTotalRuntime;
	/** the topological order of a dag */
	private TopologicalOrder order;
	/** the dilatation of task */
	private Double total_size;
	
	public DAGStats(DAG dag, Double taskDilatation){
		order = new TopologicalOrder(dag);

		Double sum = 0.0;
		for (Task t : order) {
			sum = sum + t.size * taskDilatation;
		}

		total_size = sum;
	}

	/**
	 * initialize this class by specific parameters
	 * 
	 * @param dag
	 *            : a dag class represent this dag
	 * @param taskDilatation
	 *            : task runtimes from the dag are multiplied by this factor;
	 *            this parameter is useful to control the task granularity
	 * @param vmDescription
	 *            : the specific vm description
	 */
	public DAGStats(DAG dag, Double taskDilatation, HashMap<String, VMDescription> vmDescription) {

		order = new TopologicalOrder(dag);

		minCriticalPath = 0.0;
		maxCriticalPath = 0.0;
		minTotalRuntime = 0.0;
		maxTotalRuntime = 0.0;

		Double minPowerDouble = Double.MAX_VALUE;
		Double maxPowerDouble = Double.MIN_VALUE;
		for (VMDescription vm : vmDescription.values()) {
			if (vm.mips < minPowerDouble) {
				minPowerDouble = vm.mips;
			}
			if (vm.mips > maxPowerDouble) {
				maxPowerDouble = vm.mips;
			}
		}

		HashMap<Task, Double> minruntimes = new HashMap<Task, Double>();
		HashMap<Task, Double> maxruntimes = new HashMap<Task, Double>();

		Double sum = 0.0;
		for (Task t : order) {

			// The runtime is just the size of the task (MI) divided by the
			// MIPS of the VM
			double minruntime = (t.size * taskDilatation) / maxPowerDouble;
			double maxruntime = (t.size * taskDilatation) / minPowerDouble;
			minruntimes.put(t, minruntime);
			maxruntimes.put(t, maxruntime);

			minTotalRuntime += minruntime;
			maxTotalRuntime += maxruntime;

			sum = sum + t.size * taskDilatation;
		}

		total_size = sum;

		// Make sure a plan is feasible given the deadline and available VMs
		CriticalPath maxpath = new CriticalPath(order, maxruntimes);
		CriticalPath minPath = new CriticalPath(order, minruntimes);
		maxCriticalPath = maxpath.getCriticalPathLength();
		minCriticalPath = minPath.getCriticalPathLength();

	}

	/**
	 * get the size of every task.
	 * 
	 * @return a array that store every task's size.
	 */
	public ArrayList<Double> gettasksize() {
		ArrayList<Double> runtimes = new ArrayList<Double>();
		for (Task t : order) {
			runtimes.add(t.size);
		}
		return runtimes;
	}

	public double getMinCriticalPath() {
		return minCriticalPath;
	}

	public double getMaxCriticalPath() {
		return maxCriticalPath;
	}

	public double getMinTotalRuntime() {
		return minTotalRuntime;
	}

	public double getMaxTotalRuntime() {
		return maxTotalRuntime;
	}

	public Double getTotal_size() {
		return total_size;
	}

}
