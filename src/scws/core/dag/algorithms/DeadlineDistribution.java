package scws.core.dag.algorithms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import scws.core.dag.DAG;
import scws.core.dag.DAGPartition;
import scws.core.dag.Task;
import scws.core.dag.TaskPartition;

/**
 * this class if responsible for dividing the overall deadline and distributing
 * them to every task.
 * 
 * @author jiyuanshi
 * @since 2013-05-04
 * @version sci-cwf-sim 1.0
 * 
 */
public class DeadlineDistribution {

	public static Set<Task> marked = new HashSet<Task>();
	public static Set<TaskPartition> Partitionmarked = new HashSet<TaskPartition>();
	public static ArrayList<Task> Postorder = new ArrayList<Task>();
	public static ArrayList<Task> Preorder = new ArrayList<Task>();
	public static ArrayList<TaskPartition> PartitionPostorder = new ArrayList<TaskPartition>();
	public static ArrayList<TaskPartition> PartitionPreorder = new ArrayList<TaskPartition>();
	public static HashMap<Integer, ArrayList<TaskPartition>> PartitionLever = new HashMap<Integer, ArrayList<TaskPartition>>();
	public static LinkedList<TaskPartition> Critacalpath = new LinkedList<TaskPartition>();
	public static Double CriticalPathSize = 0.0;

	/**
	 * distribute the deadline to the tasks
	 * 
	 * @param dag
	 * @param deadline
	 */
	public static void DistributeDeadline(DAG dag, Double subtime, Double deadline) {
		// doing the initial process
		marked.clear();
		Partitionmarked.clear();
		Postorder.clear();
		Preorder.clear();
		PartitionPostorder.clear();
		PartitionPreorder.clear();
		PartitionLever.clear();
		Critacalpath.clear();
		CriticalPathSize = 0.0;
		// Depth first traverse the dag and get the post and pre order of node
		dftraveral(dag);
		// divide the dag into paitition and depth first traverse it
		dftraveral(DagPartition());
		// according Bottom Depth algorithm to allocate the node in partition
		// graph to different lever.
		LeverAllocattion();
		// Find the critical path in the partition graph
		CriticalPath();
		// distribute the deadline to every node partition set
		DisDeadlineToPartition(subtime, deadline);
		// distribute the deadline to the tasks in every partition
		DisDeadlineInPartition(subtime, deadline);
		marked.clear();
		Partitionmarked.clear();
		Postorder.clear();
		Preorder.clear();
		PartitionPostorder.clear();
		PartitionPreorder.clear();
		PartitionLever.clear();
		Critacalpath.clear();
		CriticalPathSize = 0.0;
	}

	/**
	 * distribute the deadline for these tasks in every task partition
	 */
	public static void DisDeadlineInPartition(Double subtime, Double deadline) {
		for (TaskPartition taskPartition : PartitionPreorder) {
			if (taskPartition.type.equals("Synchronization")) {
				// if this partition is a Synchronization partition then just
				// sent the partition deadline to the task's deadline
				for (Task task : taskPartition.tasks) {
					task.subdealine = taskPartition.subdealine;
				}
			} else {
				// if this partition is a Branch partition then set the task's
				// deadline according to the proportion of the size
				// the timespan of this task partition set
				Double timespan = 0.0;
				Double startTime = 0.0;
				Double endTime = 0.0;
				if (taskPartition.parents.size() == 0) {
					startTime = subtime;
				} else {
					startTime = taskPartition.parents.get(0).subdealine;
				}
				endTime = taskPartition.subdealine;
				timespan = endTime - startTime;
				Double totalSize = taskPartition.getSize();
				for (Task task : taskPartition.tasks) {
					if (task.parents.size() == 0) {
						startTime = subtime;
					} else {
						startTime = task.parents.get(0).subdealine;
					}
					task.subdealine = startTime + timespan * (task.size / totalSize);
				}
			}

		}
	}

	/**
	 * distribute the deadline to every partition
	 */
	public static void DisDeadlineToPartition(Double subtime, Double deadline) {
		HashSet<Integer> levermarked = new HashSet<Integer>();
		// First distribute deadline for the critical path
		for (int i = 0; i < Critacalpath.size(); i++) {
			TaskPartition currentPartition = Critacalpath.get(i);
			if (i == 0) {
				currentPartition.subdealine = subtime + (deadline - subtime)
						* (currentPartition.getSize() / CriticalPathSize);
			} else {
				currentPartition.subdealine = Critacalpath.get(i - 1).subdealine + (deadline - subtime)
						* (currentPartition.getSize() / CriticalPathSize);
			}
		}

		// according the sub-deadline of task partition on critical path to set
		// the deadline of the task partition on the same lever
		for (TaskPartition taskPartition : Critacalpath) {
			levermarked.add(taskPartition.lever);
			for (TaskPartition leverenement : PartitionLever.get(taskPartition.lever)) {
				leverenement.subdealine = taskPartition.subdealine;
			}
		}

		// distribute the sub-deadline for these unprocessed lever
		HashMap<Integer, ArrayList<Integer>> unProcessLeverSet = new HashMap<Integer, ArrayList<Integer>>();
		Integer processedLever = 0;
		// System.out.println("PartitionLever: " + PartitionLever);
		// System.out.println("levermarked: " + levermarked);
		for (int i = 0; i < PartitionLever.size(); i++) {
			if (!levermarked.contains(i)) {
				if (unProcessLeverSet.containsKey(processedLever)) {
					unProcessLeverSet.get(processedLever).add(i);
				} else {
					unProcessLeverSet.put(processedLever, new ArrayList<Integer>());
					unProcessLeverSet.get(processedLever).add(i);
				}
			} else {
				processedLever = i;
			}
		}
		// System.out.println("unProcessLeverSet: " + unProcessLeverSet);

		for (Integer key : unProcessLeverSet.keySet()) {

			// the unprocessed lever's number
			ArrayList<Integer> unprocessedLevers = unProcessLeverSet.get(key);

			// set the lever size to the largest task partition size in every
			// lever
			HashMap<Integer, Double> unprocessedleverSize = new HashMap<Integer, Double>();

			for (Integer levernum : unprocessedLevers) {
				Double maxsize = 0.0;
				for (TaskPartition taskPartition : PartitionLever.get(levernum)) {
					Double taskPartitionsize = taskPartition.getSize();
					if (maxsize < taskPartitionsize) {
						maxsize = taskPartitionsize;
					}
				}
				unprocessedleverSize.put(levernum, maxsize);
			}

			// get the next lever size of the last lever in these unprocessed
			// lever set
			Double nextLeverSize = 0.0;
			for (TaskPartition taskPartition : PartitionLever.get(unprocessedLevers.get(0))) {
				if (taskPartition.children.size() > 0) {
					for (TaskPartition child : taskPartition.children) {
						Double childsize = child.getSize();
						if (nextLeverSize < childsize) {
							nextLeverSize = childsize;
						}
					}
				}
			}

			// get the total size of these unprocessed lever including the
			// processed lever after the last unprocessed lever
			Double totalsize = nextLeverSize;
			for (Double size : unprocessedleverSize.values()) {
				totalsize = totalsize + size;
			}

			// get the total timespan can be use for these unprocessed lever
			Double totalTimeSpan = 0.0;
			Double startTime = 0.0;
			Double endTime = 0.0;
			if (PartitionLever.get(unprocessedLevers.get(0)).get(0).children.size() != 0
					&& PartitionLever.get(unprocessedLevers.get(unprocessedLevers.size() - 1)).get(0).parents
							.size() != 0) {
				endTime = PartitionLever.get(unprocessedLevers.get(0)).get(0).children.get(0).subdealine;
				startTime = PartitionLever.get(unprocessedLevers.get(unprocessedLevers.size() - 1)).get(0).parents
						.get(0).subdealine;
			}
			if (PartitionLever.get(unprocessedLevers.get(0)).get(0).children.size() == 0) {
				endTime = deadline;
				startTime = PartitionLever.get(unprocessedLevers.get(unprocessedLevers.size() - 1)).get(0).parents
						.get(0).subdealine;
			}
			if (PartitionLever.get(unprocessedLevers.get(unprocessedLevers.size() - 1)).get(0).parents.size() == 0) {
				startTime = subtime;
				endTime = PartitionLever.get(unprocessedLevers.get(0)).get(0).children.get(0).subdealine;
			}
			if (PartitionLever.get(unprocessedLevers.get(0)).get(0).children.size() == 0
					&& PartitionLever.get(unprocessedLevers.get(unprocessedLevers.size() - 1)).get(0).parents
							.size() == 0) {
				startTime = subtime;
				endTime = deadline;
			}

			totalTimeSpan = endTime - startTime;

			// distribute the sub-deadline to the unprocessed lever
			for (int j = unprocessedLevers.size() - 1; j >= 0; j--) {
				Integer levernum = unprocessedLevers.get(j);
				if (PartitionLever.get(levernum).get(0).parents.size() == 0) {
					startTime = subtime;
				} else {
					startTime = PartitionLever.get(levernum).get(0).parents.get(0).subdealine;
				}
				Double subdeadline = startTime + totalTimeSpan
						* (unprocessedleverSize.get(levernum) / totalsize);

				// set sub-deadline for every task partition in this lever
				for (TaskPartition taskPartition : PartitionLever.get(levernum)) {
					taskPartition.subdealine = subdeadline;
				}
			}

		}

	}

	/**
	 * get the Critical Path of the Partition DAG graph
	 */
	public static void CriticalPath() {
		HashMap<TaskPartition, Double> partitionSize = new HashMap<TaskPartition, Double>();
		HashMap<TaskPartition, Double> eft = new HashMap<TaskPartition, Double>();

		// Initially all the task partition's size and finish time no matter
		// whatever the runtime is
		for (TaskPartition taskPartition : PartitionPostorder) {
			Double size = taskPartition.getSize();
			partitionSize.put(taskPartition, size);
			eft.put(taskPartition, size);
		}

		// Now we adjust the values in the topological order and record the node
		// with the largest eft
		TaskPartition LargestPartition = PartitionPostorder.get(PartitionPostorder.size() - 1);
		for (int i = PartitionPostorder.size() - 1; i >= 0; i--) {
			TaskPartition taskPartition = PartitionPostorder.get(i);
			if (eft.get(taskPartition) > eft.get(LargestPartition)) {
				LargestPartition = taskPartition;
			}
			for (TaskPartition child : taskPartition.children) {
				eft.put(child, Math.max(eft.get(child), eft.get(taskPartition) + partitionSize.get(child)));
			}
		}
		// record the critical path size
		CriticalPathSize = eft.get(LargestPartition);

		// Now we find the critical path according the topological order and
		// eft, then save it into the arraylist
		Critacalpath.push(LargestPartition);
		while (!LargestPartition.parents.isEmpty()) {
			TaskPartition tmpPartition = LargestPartition.parents.get(0);
			for (TaskPartition taskPartition : LargestPartition.parents) {
				if (eft.get(tmpPartition) < eft.get(taskPartition)) {
					tmpPartition = taskPartition;
				}
			}
			LargestPartition = tmpPartition;
			Critacalpath.push(LargestPartition);
		}
	}

	/**
	 * assign the task to different partition
	 */
	public static DAGPartition DagPartition() {
		DAGPartition dagPartition = new DAGPartition();
		Integer partitionId = 0;
		for (Task task : Preorder) {
			if (task.children.size() > 1 || task.parents.size() > 1) {
				task.partitionId = partitionId;
				TaskPartition taskPartition = new TaskPartition(String.valueOf(partitionId),
						"Synchronization");
				taskPartition.tasks.add(task);
				dagPartition.addTaskPartition(taskPartition);
				partitionId++;
			} else {
				task.partitionId = partitionId;
				if (dagPartition.containsTaskPartiton(String.valueOf(partitionId))) {
					dagPartition.getTask(String.valueOf(partitionId)).tasks.add(task);
				} else {
					TaskPartition taskPartition = new TaskPartition(String.valueOf(partitionId), "Branch");
					taskPartition.tasks.add(task);
					dagPartition.addTaskPartition(taskPartition);
				}
				if (task.children.size() <= 0 || task.children.get(0).children.size() > 1
						|| task.children.get(0).parents.size() > 1) {
					partitionId++;
				}
			}
		}
		for (String partitionID : dagPartition.getTasks()) {
			if (dagPartition.getTask(partitionID).tasks.get(0).parents.size() > 0) {
				for (Task task : dagPartition.getTask(partitionID).tasks.get(0).parents) {
					dagPartition.addEdge(String.valueOf(task.partitionId), String.valueOf(partitionID));
				}
			}
		}
		return dagPartition;
	}

	/**
	 * distribute the task into different lever using the DBL algorithm
	 */
	public static void LeverAllocattion() {
		for (TaskPartition taskPartition : PartitionPostorder) {
			if (taskPartition.children.size() == 0) {
				taskPartition.lever = 0;
			} else {
				for (TaskPartition children : taskPartition.children) {
					if (taskPartition.lever < children.lever + 1) {
						taskPartition.lever = children.lever + 1;
					}
				}
			}
		}
		for (TaskPartition taskPartition : PartitionPostorder) {
			// System.out.println("Partition ID: " + taskPartition + "~lever: "
			// + taskPartition.lever);
			if (PartitionLever.containsKey(taskPartition.lever)) {
				PartitionLever.get(taskPartition.lever).add(taskPartition);
			} else {
				PartitionLever.put(taskPartition.lever, new ArrayList<TaskPartition>());
				PartitionLever.get(taskPartition.lever).add(taskPartition);
			}
		}
	}

	/**
	 * depth first traverse for DAG
	 */
	public static void dftraveral(DAG dag) {
		for (Task task : dag.getStartTasks()) {
			if (!marked.contains(task))
				dfs(task);
		}
		// System.out.println("Preorder~~~~~~~~~~");
		// for (Task task : Preorder) {
		// System.out.println(task);
		// }
		// System.out.println("Postorder~~~~~~~~~~");
		// for (Task task : Postorder) {
		// System.out.println(task);
		// }
		marked.clear();
	}

	/**
	 * depth first search DAG
	 * 
	 * @param task
	 */
	private static void dfs(Task task) {
		marked.add(task);
		Preorder.add(task);
		for (Task child : task.children) {
			if (!marked.contains(child))
				dfs(child);
		}
		Postorder.add(task);
	}

	/**
	 * depth first traverse for DAGPartition
	 */
	public static void dftraveral(DAGPartition dagPartition) {
		for (TaskPartition taskPartition : dagPartition.getStartTasks()) {
			if (!Partitionmarked.contains(taskPartition))
				dfs(taskPartition);
		}
		// for (Task task : Preorder) {
		// System.out.println(task);
		// }
		// System.out.println("~~~~~~~~~~");
		// for (Task task : Postorder) {
		// System.out.println(task);
		// }
		Partitionmarked.clear();
	}

	/**
	 * depth first search DAGPartition
	 * 
	 * @param task
	 */
	private static void dfs(TaskPartition taskPartition) {
		Partitionmarked.add(taskPartition);
		PartitionPreorder.add(taskPartition);
		for (TaskPartition child : taskPartition.children) {
			if (!Partitionmarked.contains(child))
				dfs(child);
		}
		PartitionPostorder.add(taskPartition);
	}

}
