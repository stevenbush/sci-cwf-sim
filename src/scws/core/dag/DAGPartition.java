package scws.core.dag;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * this class represent a divided graph, every node in this graph is a task
 * partition of the original graph
 * 
 * @author jiyuanshi
 * @since 2013-05-03
 * @version sci-cwf-sim 1.0
 * 
 */
public class DAGPartition {

	private HashMap<String, TaskPartition> taskPartitions = new HashMap<String, TaskPartition>();

	/**
	 * add a new taskpartition node into this graph
	 * 
	 * @param t
	 */
	public void addTaskPartition(TaskPartition t) {
		if (taskPartitions.containsKey(t.id)) {
			throw new RuntimeException("TaskPartition already exists: " + t.id);
		}
		taskPartitions.put(t.id, t);
	}

	/**
	 * add a new edge into this graph
	 * 
	 * @param parent
	 * @param child
	 */
	public void addEdge(String parent, String child) {
		TaskPartition p = taskPartitions.get(parent);
		if (p == null) {
			throw new RuntimeException("Invalid edge: Parent not found: " + parent);
		}
		TaskPartition c = taskPartitions.get(child);
		if (c == null) {
			throw new RuntimeException("Invalid edge: Child not found: " + child);
		}
		p.children.add(c);
		c.parents.add(p);
	}

	/**
	 * get the task partition of the specific id
	 * 
	 * @param id
	 * @return
	 */
	public TaskPartition getTask(String id) {
		if (!taskPartitions.containsKey(id)) {
			throw new RuntimeException("TaskPartiton not found: " + id);
		}
		return taskPartitions.get(id);
	}
	
	/**
	 * judge if this DAG contain is task partition
	 * @param id
	 * @return
	 */
	public Boolean containsTaskPartiton(String id) {
		return taskPartitions.containsKey(id);
	}

	/**
	 * get all the task partiton's id
	 * 
	 * @return
	 */
	public String[] getTasks() {
		return taskPartitions.keySet().toArray(new String[0]);
	}

	/**
	 * get all the start tasks which have no parents in this DAG
	 * 
	 * @return an array contain these tasks
	 */
	public ArrayList<TaskPartition> getStartTasks() {
		ArrayList<TaskPartition> starttasks = new ArrayList<TaskPartition>();
		for (TaskPartition task : taskPartitions.values()) {
			if (task.parents.size() == 0) {
				starttasks.add(task);
			}
		}
		return starttasks;
	}

	/**
	 * get all the end tasks which have no childrens in this DAG
	 * 
	 * @return an array contain these tasks
	 */
	public ArrayList<TaskPartition> getEndTasks() {
		ArrayList<TaskPartition> endtasks = new ArrayList<TaskPartition>();
		for (TaskPartition task : taskPartitions.values()) {
			if (task.children.size() == 0) {
				endtasks.add(task);
			}
		}
		return endtasks;
	}

	/**
	 * Get a virtual start task that connect all the task without parents in the
	 * DAG as children
	 * 
	 * @return this virtual start task
	 */
	public TaskPartition getVirtualStartTask() {
		TaskPartition virtualStart = new TaskPartition("virtual_start", "Synchronization");
		for (TaskPartition task : getStartTasks()) {
			virtualStart.children.add(task);
		}
		return virtualStart;
	}

	/**
	 * Get a virtual end task that connect all the task without children in the
	 * DAG as parent
	 * 
	 * @return this virtual end task
	 */
	public TaskPartition getVirtualEndTask() {
		TaskPartition virtualEnd = new TaskPartition("virtual_end", "Synchronization");
		for (TaskPartition task : getEndTasks()) {
			virtualEnd.parents.add(task);
		}
		return virtualEnd;

	}
}
