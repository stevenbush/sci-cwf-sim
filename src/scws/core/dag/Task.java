package scws.core.dag;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a task.
 * 
 * @author Gideon Juve <juve@usc.edu>
 */
public class Task {
	public String id = null;
	public String transformation = null;
	public double size = 0.0;
	/** the lever of this task */
	public Integer lever = 0;
	/** the set id of this task belong to */
	public Integer partitionId = 0;
	/** sub-deadline of this tasks */
	public double subdealine = 0.0;
	public List<Task> parents = new ArrayList<Task>(2);
	public List<Task> children = new ArrayList<Task>(5);
	public List<String> inputs = null;
	public List<String> outputs = null;

	public Task(String id, String transformation, double size) {
		this.id = id;
		this.transformation = transformation;
		this.size = size;
		this.lever = 0;
		// SIPHT workflows have tasks with 0.0 size so we commented out this
		// condition
		// if (size <= 0) {
		// throw new RuntimeException(
		// "Invalid size for task "+id+" ("+transformation+"): "+size);
		// }
	}

	/**
	 * IT IS IMPORTANT THAT THESE ARE NOT IMPLEMENTED
	 * 
	 * Using the default implementation allows us to put Tasks from different
	 * DAGs that have the same task ID into a single HashMap or HashSet. That
	 * way we don't have to maintain a reference from the Task to the DAG that
	 * owns it--we can mix tasks from different DAGs in the same data structure.
	 * 
	 * <pre>
	 * public int hashCode() {
	 * 	return id.hashCode();
	 * }
	 * 
	 * public boolean equals(Object o) {
	 * 	if (!(o instanceof Task)) {
	 * 		return false;
	 * 	}
	 * 	Task t = (Task) o;
	 * 	return this.id.equals(t.id);
	 * }
	 * </pre>
	 */

	@Override
	public String toString() {
		return id;
	}
}
