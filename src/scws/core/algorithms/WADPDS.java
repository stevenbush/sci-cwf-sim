package scws.core.algorithms;

import java.util.ArrayList;

import scws.core.DAGDescription;
import scws.core.VMDescription;
import scws.core.provisioner.SimpleUtilizationBasedProvisioner;
import scws.core.scheduler.WorkflowAwareEnsembleScheduler;

public class WADPDS extends DynamicAlgorithm {
	public WADPDS(double budget, double timespan, double taskDilatation, double informtimeratio,
			ArrayList<DAGDescription> dags, ArrayList<VMDescription> vms, double maxScaling) {
		super(budget, timespan, taskDilatation, dags, vms, informtimeratio,
				new WorkflowAwareEnsembleScheduler(), new SimpleUtilizationBasedProvisioner(maxScaling));
	}
}
