package scws.core.algorithms;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import scws.core.DAGDescription;
import scws.core.dag.DAG;

/**
 * this a abstract class represent a kind of algorithm that can be extended by
 * specific algorithm.
 * 
 * @author jiyuanshi <shijiyuan.seu@gmail.com>
 * @since 2013-4-20
 * @version sci-cwf-sim 1.0
 * 
 */
public abstract class Algorithm {
	private double budget;
	private double timespan;
	private double taskDilatation;
	private double informtimeratio;
	private ArrayList<DAGDescription> dags;
	private ArrayList<Integer> DAGPriorities;
	private boolean generateLog = false;

	public Algorithm(double budget, double timespan, double taskDilatation, double informtimeratio,
			ArrayList<DAGDescription> dags) {
		this.budget = budget;
		this.timespan = timespan;
		this.taskDilatation = taskDilatation;
		this.informtimeratio = informtimeratio;
		this.dags = dags;
		this.DAGPriorities = new ArrayList<Integer>();
	}

	public double getBudget() {
		return budget;
	}

	public double getTimespan() {
		return timespan;
	}

	public ArrayList<DAGDescription> getDags() {
		return dags;
	}

	public String getName() {
		return this.getClass().getSimpleName();
	}

	public boolean shouldGenerateLog() {
		return this.generateLog;
	}

	public void setGenerateLog(boolean generateLog) {
		this.generateLog = generateLog;
	}

	abstract public void simulate(String logname);

	abstract public double getActualCost();

	/**
	 * @return Finish time of the last completed dag
	 */
	abstract public double getActualDagFinishTime();

	/**
	 * @return Finish time of the last successfully completed job
	 */
	abstract public double getActualJobFinishTime();

	/**
	 * @return Termination time of the last VM
	 */
	abstract public double getActualVMFinishTime();

	abstract public long getSimulationWallTime();

	abstract public long getPlanningnWallTime();

	//abstract public List<DAG> getCompletedDAGs();

	public int numCompletedDAGs() {
		return DAGPriorities.size();
	}

	public ArrayList<Integer> completedDAGPriorities() {
		return DAGPriorities;
	}

	public String completedDAGPriorityString() {
		StringBuilder b = new StringBuilder("[");
		boolean first = true;
		for (int priority : completedDAGPriorities()) {
			if (!first) {
				b.append(", ");
			}
			b.append(priority);
			first = false;
		}
		b.append("]");
		return b.toString();
	}

	/** score = sum[ 1 / 2^priority ] */
	public double getExponentialScore() {
		BigDecimal one = BigDecimal.ONE;
		BigDecimal two = new BigDecimal(2.0);

		BigDecimal score = new BigDecimal(0.0);
		for (int priority : completedDAGPriorities()) {
			BigDecimal divisor = two.pow(priority);
			BigDecimal increment = one.divide(divisor);
			score = score.add(increment);
		}
		return score.doubleValue();
	}

	/** score = sum[ 1 / priority ] */
	public double getLinearScore() {
		double score = 0.0;
		for (int priority : completedDAGPriorities()) {
			score += 1.0 / (priority + 1);
		}
		return score;
	}

	public String getScoreBitString() {
		HashSet<Integer> priorities = new HashSet<Integer>(completedDAGPriorities());

		int ensembleSize = getDags().size();

		StringBuilder b = new StringBuilder();

		for (int p = 0; p < ensembleSize; p++) {
			if (priorities.contains(p)) {
				b.append("1");
			} else {
				b.append("0");
			}
		}

		return b.toString();
	}

	public double getTaskDilatation() {
		return taskDilatation;
	}

	public void setTaskDilatation(double taskDilatation) {
		this.taskDilatation = taskDilatation;
	}

	public double getInformtimeratio() {
		return informtimeratio;
	}

	public void setInformtimeratio(double informtimeratio) {
		this.informtimeratio = informtimeratio;
	}

	public ArrayList<Integer> getDAGPriorities() {
		return DAGPriorities;
	}

	public void setDAGPriorities(ArrayList<Integer> dAGPriorities) {
		DAGPriorities = dAGPriorities;
	}

}
