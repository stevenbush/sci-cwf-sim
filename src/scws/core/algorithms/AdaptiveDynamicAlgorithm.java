package scws.core.algorithms;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.core.CloudSim;

import scws.core.Cloud;
import scws.core.DAGDescription;
import scws.core.DAGJob;
import scws.core.DAGJobListener;
import scws.core.Job;
import scws.core.JobListener;
import scws.core.Provisioner;
import scws.core.Scheduler;
import scws.core.SimpleJobFactory;
import scws.core.VMDescription;
import scws.core.VMInstance;
import scws.core.VMInstanceListener;
import scws.core.VMManger;
import scws.core.WorkflowEngine;
import scws.core.WorkloadManager;
import scws.core.Job.Result;
import scws.core.dag.DAG;
import scws.core.experiment.VMFactory;
import scws.core.log.WorkflowLog;

/**
 * this class is used to be extended for other Adaptive resource provision
 * algorithm.
 * 
 * @author jiyuanshi
 * @since 2013-4-27
 * @version sci-cwf-sim 1.0
 * 
 */
public class AdaptiveDynamicAlgorithm extends Algorithm implements DAGJobListener, VMInstanceListener,
		JobListener {

	private ArrayList<VMDescription> vms;

	private Scheduler scheduler;

	private Provisioner provisioner;

	// private List<DAG> completedDAGs = new LinkedList<DAG>();

	private double actualCost = 0.0;

	private double actualDagFinishTime = 0.0;
	private double actualVMFinishTime = 0.0;
	private double actualJobFinishTime = 0.0;

	protected long simulationStartWallTime;
	protected long simulationFinishWallTime;

	public AdaptiveDynamicAlgorithm(double budget, double timespan, double taskDilatation,
			ArrayList<DAGDescription> dags, ArrayList<VMDescription> vms, double informtimeratio,
			Scheduler scheduler, Provisioner provisioner) {
		super(budget, timespan, taskDilatation, informtimeratio, dags);
		this.vms = vms;
		this.provisioner = provisioner;
		this.scheduler = scheduler;
	}

	@Override
	public double getActualCost() {
		return actualCost;
	}

	@Override
	public void dagStarted(DAGJob dagJob) {
		/* Do nothing */
	}

	@Override
	public void dagFinished(DAGJob dagJob) {
		actualDagFinishTime = Math.max(actualDagFinishTime, CloudSim.clock());
		if (dagJob.isFinished()) {
			getDAGPriorities().add((int) dagJob.getPriority());
		}
	}

	@Override
	public double getActualDagFinishTime() {
		return actualDagFinishTime;
	}

	// @Override
	// public List<DAG> getCompletedDAGs() {
	// return completedDAGs;
	// }

	@Override
	public void simulate(String logname) {

		CloudSim.init(1, null, false);

		Cloud cloud = new Cloud();
		cloud.addVMListener(this);
		provisioner.setCloud(cloud);

		WorkflowEngine engine = new WorkflowEngine(new SimpleJobFactory(getTaskDilatation()), provisioner,
				scheduler, getTimespan(), getBudget());

		engine.addJobListener(this);

		scheduler.setWorkflowEngine(engine);

		WorkloadManager wm = new WorkloadManager(getDags(), engine);
		wm.addDAGJobListener(this);

		ArrayList<VMManger> vmMangers = new ArrayList<VMManger>();
		for (VMDescription vmDescription : vms) {
			VMManger vmManger = new VMManger(vmDescription.vmname, vmDescription.price, vmDescription.mips,
					1, 1.0, provisioner, scheduler, engine, cloud);
			vmMangers.add(vmManger);
			vmManger.setInformtimeratio(getInformtimeratio());
			//vmManger.setProvisioningDelay(VMFactory.getProvisioningDelay());
			// vmManger.setProvisioningDelayDistribution(VMFactory.getProvisioningDelayDistribution());
			// vmManger.setDeprovisioningDelayDistribution(VMFactory.getDeprovisioningDelayDistribution());
			vmManger.setRuntimeDistribution(VMFactory.getRuntimeDistribution());
			vmManger.setFailureModel(VMFactory.getFailureModel());
		}

		engine.setVmMangers(vmMangers);

		WorkflowLog log = null;
		if (shouldGenerateLog()) {
			log = new WorkflowLog();
			engine.addJobListener(log);
			cloud.addVMListener(log);
			wm.addDAGJobListener(log);
		}

		simulationStartWallTime = System.nanoTime();

		CloudSim.startSimulation();

		simulationFinishWallTime = System.nanoTime();

		if (shouldGenerateLog()) {
			log.printJobs(logname);
			log.printVmList(logname);
			log.printDAGJobs(logname);
		}

		Log.printLine(CloudSim.clock() + " Total budget " + getBudget());
		Log.printLine(CloudSim.clock() + " Total cost " + engine.getCost());

		// Set results
		actualCost = engine.getCost();

		// for (DAGJob dj : engine.getAllDAGJobs()) {
		// if (dj.isFinished()) {
		// completedDAGs.add(dj.getDAG());
		// }
		// }

		if (actualDagFinishTime > getTimespan()) {
			System.err.println("WARNING: Exceeded timespan: " + actualDagFinishTime + ">" + getTimespan()
					+ " budget: " + getBudget());
		}

		if (getActualCost() > getBudget()) {
			System.err.println("WARNING: Cost exceeded budget: " + getActualCost() + ">" + getBudget()
					+ " deadline: " + getTimespan());
		}

	}

	@Override
	public long getSimulationWallTime() {
		return simulationFinishWallTime - simulationStartWallTime;
	}

	@Override
	public long getPlanningnWallTime() {
		// planning is always 0 for dynamic algorithms
		return 0;
	}

	@Override
	public double getActualJobFinishTime() {
		return actualJobFinishTime;
	}

	@Override
	public double getActualVMFinishTime() {
		return actualVMFinishTime;
	}

	@Override
	public void vminstanceLaunched(VMInstance vm) {
	}

	@Override
	public void vminstanceTerminated(VMInstance vm) {
		actualVMFinishTime = Math.max(actualVMFinishTime, vm.getTerminateTime());
	}

	@Override
	public void jobReleased(Job job) {
	}

	@Override
	public void jobSubmitted(Job job) {
	}

	@Override
	public void jobStarted(Job job) {
	}

	@Override
	public void jobFinished(Job job) {
		if (job.getResult() == Result.SUCCESS) {
			actualJobFinishTime = Math.max(actualJobFinishTime, job.getFinishTime());
		}
	}

}
