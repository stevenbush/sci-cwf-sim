package scws.core.algorithms;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.core.CloudSim;

import scws.core.WorkflowEvent;
import scws.core.log.WorkflowLog;
import scws.core.DAGDescription;
import scws.core.Job;
import scws.core.Job.Result;
import scws.core.SimpleJobFactory;
import scws.core.VMInstance;
import scws.core.VMManger;
import scws.core.WorkflowEngine;
import scws.core.Cloud;
import scws.core.DAGJob;
import scws.core.Provisioner;
import scws.core.Scheduler;
import scws.core.WorkloadManager;
import scws.core.dag.DAG;
import scws.core.experiment.VMFactory;
import scws.core.DAGJobListener;
import scws.core.JobListener;
import scws.core.VMDescription;
import scws.core.VMInstanceListener;

/**
 * this class is used to be extended for algorithms DPDS and WADPDS
 * 
 * @author jiyuanshi <shijiyuan.seu@gmail.com>
 * @since 2013-4-20
 * @version sci-cwf-sim 1.0
 * 
 */
public class DynamicAlgorithm extends Algorithm implements DAGJobListener, VMInstanceListener, JobListener {

	private ArrayList<VMDescription> vms;

	private Scheduler scheduler;

	private Provisioner provisioner;

	// private List<DAG> completedDAGs = new LinkedList<DAG>();

	private double actualCost = 0.0;

	private double actualDagFinishTime = 0.0;
	private double actualVMFinishTime = 0.0;
	private double actualJobFinishTime = 0.0;

	protected long simulationStartWallTime;
	protected long simulationFinishWallTime;

	public DynamicAlgorithm(double budget, double timespan, double taskDilatation,
			ArrayList<DAGDescription> dags, ArrayList<VMDescription> vms, double informtimeratio,
			Scheduler scheduler, Provisioner provisioner) {
		super(budget, timespan, taskDilatation, informtimeratio, dags);
		this.vms = vms;
		this.provisioner = provisioner;
		this.scheduler = scheduler;
	}

	@Override
	public double getActualCost() {
		return actualCost;
	}

	@Override
	public void dagStarted(DAGJob dagJob) {
		/* Do nothing */
	}

	@Override
	public void dagFinished(DAGJob dagJob) {
		actualDagFinishTime = Math.max(actualDagFinishTime, CloudSim.clock());
		if (dagJob.isFinished()) {
			getDAGPriorities().add((int) dagJob.getPriority());
		}
	}

	@Override
	public double getActualDagFinishTime() {
		return actualDagFinishTime;
	}

	// @Override
	// public List<DAG> getCompletedDAGs() {
	// return completedDAGs;
	// }

	@Override
	public void simulate(String logname) {

		CloudSim.init(1, null, false);

		Cloud cloud = new Cloud();
		cloud.addVMListener(this);
		provisioner.setCloud(cloud);

		WorkflowEngine engine = new WorkflowEngine(new SimpleJobFactory(getTaskDilatation()), provisioner,
				scheduler, getTimespan(), getBudget());

		engine.addJobListener(this);

		scheduler.setWorkflowEngine(engine);

		WorkloadManager wm = new WorkloadManager(getDags(), engine);
		wm.addDAGJobListener(this);

		ArrayList<VMManger> vmMangers = new ArrayList<VMManger>();
		for (VMDescription vmDescription : vms) {
			VMManger vmManger = new VMManger(vmDescription.vmname, vmDescription.price, vmDescription.mips,
					1, 1.0, provisioner, scheduler, engine, cloud);
			vmMangers.add(vmManger);
			vmManger.setInformtimeratio(getInformtimeratio());
			//vmManger.setProvisioningDelay(VMFactory.getProvisioningDelay());
			// vmManger.setProvisioningDelayDistribution(VMFactory.getProvisioningDelayDistribution());
			// vmManger.setDeprovisioningDelayDistribution(VMFactory.getDeprovisioningDelayDistribution());
			vmManger.setRuntimeDistribution(VMFactory.getRuntimeDistribution());
			vmManger.setFailureModel(VMFactory.getFailureModel());
		}

		engine.setVmMangers(vmMangers);

		WorkflowLog log = null;
		if (shouldGenerateLog()) {
			log = new WorkflowLog();
			engine.addJobListener(log);
			cloud.addVMListener(log);
			wm.addDAGJobListener(log);
		}

		// Calculate estimated number of VMs to consume budget evenly before the
		// end of timespan
		// ceiling is used to start more vms so that the budget is consumed just
		// before the end of the timespan
		// first according the number of vm type to divide the budget equally
		ArrayList<Double> subbudgets = new ArrayList<Double>();
		for (int i = 0; i < vmMangers.size(); i++) {
			Double subbudget = getBudget() / vmMangers.size();
			subbudgets.add(subbudget);
		}
		// calculate the estimate number of VMs for every VMmanger
		ArrayList<Integer> vm_numbers = new ArrayList<Integer>();
		for (int i = 0; i < vmMangers.size(); i++) {
			Integer numVMs = (int) Math.ceil(Math.floor(subbudgets.get(i))
					/ Math.ceil((getTimespan() / (60 * 60))) / vmMangers.get(i).getPrice());
			// Check if we can afford at least one VM of this type
			if (subbudgets.get(i) < vmMangers.get(i).getPrice())
				numVMs = 0;
			vm_numbers.add(numVMs);

			Log.printLine(CloudSim.clock() + " assigned budget for VM type " + vmMangers.get(i).getName()
					+ " is " + subbudgets.get(i));
			Log.printLine(CloudSim.clock() + " Estimated num of VMs for VM type "
					+ vmMangers.get(i).getName() + " is " + numVMs);
		}

		// Launch the initial VMs for every VM type
		for (int i = 0; i < vmMangers.size(); i++) {
			for (int j = 0; j < vm_numbers.get(i); j++) {
				VMInstance vmInstance = VMFactory.createVM(vmMangers.get(i), vmMangers.get(i).getMips(), 1,
						1.0, vmMangers.get(i).getPrice());
				CloudSim.send(vmMangers.get(i).getId(), cloud.getId(), 0.0, WorkflowEvent.VM_LAUNCH,
						vmInstance);
			}
		}

		simulationStartWallTime = System.nanoTime();

		CloudSim.startSimulation();

		simulationFinishWallTime = System.nanoTime();

		if (shouldGenerateLog()) {
			log.printJobs(logname);
			log.printVmList(logname);
			log.printDAGJobs(logname);
		}

		Log.printLine(CloudSim.clock() + " Total budget " + getBudget());
		Log.printLine(CloudSim.clock() + " Total cost " + engine.getCost());

		// Set results
		actualCost = engine.getCost();

		// for (DAGJob dj : engine.getAllDAGJobs()) {
		// if (dj.isFinished()) {
		// completedDAGs.add(dj.getDAG());
		// }
		// }

		if (actualDagFinishTime > getTimespan()) {
			System.err.println("WARNING: Exceeded timespan: " + actualDagFinishTime + ">" + getTimespan()
					+ " budget: " + getBudget());
		}

		if (getActualCost() > getBudget()) {
			System.err.println("WARNING: Cost exceeded budget: " + getActualCost() + ">" + getBudget()
					+ " deadline: " + getTimespan());
		}

	}

	@Override
	public long getSimulationWallTime() {
		return simulationFinishWallTime - simulationStartWallTime;
	}

	@Override
	public long getPlanningnWallTime() {
		// planning is always 0 for dynamic algorithms
		return 0;
	}

	@Override
	public double getActualJobFinishTime() {
		return actualJobFinishTime;
	}

	@Override
	public double getActualVMFinishTime() {
		return actualVMFinishTime;
	}

	@Override
	public void vminstanceLaunched(VMInstance vm) {
	}

	@Override
	public void vminstanceTerminated(VMInstance vm) {
		actualVMFinishTime = Math.max(actualVMFinishTime, vm.getTerminateTime());
	}

	@Override
	public void jobReleased(Job job) {
	}

	@Override
	public void jobSubmitted(Job job) {
	}

	@Override
	public void jobStarted(Job job) {
	}

	@Override
	public void jobFinished(Job job) {
		if (job.getResult() == Result.SUCCESS) {
			actualJobFinishTime = Math.max(actualJobFinishTime, job.getFinishTime());
		}
	}

}
