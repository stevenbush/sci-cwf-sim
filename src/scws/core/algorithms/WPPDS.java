package scws.core.algorithms;

import java.util.ArrayList;

import scws.core.DAGDescription;
import scws.core.Provisioner;
import scws.core.Scheduler;
import scws.core.VMDescription;
import scws.core.provisioner.PreProcessingProvisioner;
import scws.core.scheduler.WorkflowAwarePreProcessingScheduler;

public class WPPDS extends AdaptiveDynamicAlgorithm {

	public WPPDS(double budget, double timespan, double taskDilatation, double informtimeratio,
			ArrayList<DAGDescription> dags, ArrayList<VMDescription> vms, String logfilepath) {
		super(budget, timespan, taskDilatation, dags, vms, informtimeratio,
				new WorkflowAwarePreProcessingScheduler(logfilepath), new PreProcessingProvisioner());
		// TODO Auto-generated constructor stub
	}

}
