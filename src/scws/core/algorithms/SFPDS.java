package scws.core.algorithms;

import java.util.ArrayList;

import scws.core.DAGDescription;
import scws.core.VMDescription;
import scws.core.provisioner.ScalingFirstProvisioner;
import scws.core.scheduler.ScalingFirstScheduler;

public class SFPDS extends AdaptiveDynamicAlgorithm {

	public SFPDS(double budget, double timespan, double taskDilatation, double informtimeratio,
			ArrayList<DAGDescription> dags, ArrayList<VMDescription> vms) {
		super(budget, timespan, taskDilatation, dags, vms, informtimeratio, new ScalingFirstScheduler(),
				new ScalingFirstProvisioner());
		// TODO Auto-generated constructor stub
	}
}
