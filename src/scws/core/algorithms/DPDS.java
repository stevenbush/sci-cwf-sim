package scws.core.algorithms;

import java.util.ArrayList;

import scws.core.DAGDescription;
import scws.core.VMDescription;
import scws.core.provisioner.SimpleUtilizationBasedProvisioner;
import scws.core.scheduler.EnsembleDynamicScheduler;

public class DPDS extends DynamicAlgorithm {
	public DPDS(double budget, double timespan, double taskDilatation, double informtimeratio,
			ArrayList<DAGDescription> dags, ArrayList<VMDescription> vms, double maxScaling) {
		super(budget, timespan, taskDilatation, dags, vms, informtimeratio, new EnsembleDynamicScheduler(),
				new SimpleUtilizationBasedProvisioner(maxScaling));
	}
}
